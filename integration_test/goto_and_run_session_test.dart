import 'package:exercise/main.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/io/file_io_functions.dart';
import 'package:exercise/model/run_session_timer.dart';
import 'package:exercise/model/speak.dart';
import 'package:exercise/screens/run_session_screen.dart';
import 'package:exercise/screens/session_data_screen.dart';
import 'package:exercise/widgets/run_session_controls.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

//WidgetsFlutterBinding.ensureInitialized();
  FileIOFunctions fileIOFunctions = FileIOFunctions();
  FileHandlingFunctions fileHandlingFunctions =
      FileHandlingFunctions(fileIOFunctions: fileIOFunctions);
  Speak speech = Speak();
  AppData appData = AppData(fileHandlingFunctions: fileHandlingFunctions);
  RunSessionTimer runSessionTimer = RunSessionTimer(
    speachFunction: speech.speak,
    storeSessionFunction: appData.addSessionData,
    //  fileSystem: MemoryFileSystem(),
  );
  setUp(() {
    fileIOFunctions = FileIOFunctions();
    fileHandlingFunctions =
        FileHandlingFunctions(fileIOFunctions: fileIOFunctions);
    speech = Speak();
    appData = AppData(fileHandlingFunctions: fileHandlingFunctions);
    runSessionTimer = RunSessionTimer(
      speachFunction: speech.speak,
      storeSessionFunction: appData.addSessionData,
      //  fileSystem: MemoryFileSystem(),
    );
  });
  testWidgets(
      "Can a user navigate to the MiniCircuits session, run it and view the data?",
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp(
      fileIOFunctions: fileIOFunctions,
      fileHandlingFunctions: fileHandlingFunctions,
      speech: speech,
      appData: appData,
      runSessionTimer: runSessionTimer,
    ));
    await tester.pumpAndSettle(Duration(seconds: 4));
    await mainScreenToSession(tester, "Mini Circuits (Test)");
    await runSessionAndHome(tester, 105, "Squats");
    await gotoSessionData(tester);

    await gotoNSessionData(tester, 0);
    checkCircuits();
  });
  testWidgets(
      "Can a user navigate to the MiniCircuits session, run it, view the data then repeat?",
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp(
      fileIOFunctions: fileIOFunctions,
      fileHandlingFunctions: fileHandlingFunctions,
      speech: speech,
      appData: appData,
      runSessionTimer: runSessionTimer,
    ));
    await tester.pumpAndSettle(Duration(seconds: 4));
    await mainScreenToSession(tester, "Mini Circuits (Test)");
    await runSessionAndHome(tester, 105, "Squats");
    await gotoSessionData(tester);

    await gotoNSessionData(tester, 0);
    checkCircuits();
    await pressBackButton(tester);
    await pressBackButton(tester);
    await mainScreenToSession(tester, "Mini Circuits (Test)");
    await runSessionAndHome(tester, 105, "Squats");
    await gotoSessionData(tester);
    await gotoNSessionData(tester, 1);
    checkCircuits();
    await pressBackButton(tester);
    await gotoNSessionData(tester, 0);
    checkCircuits();
  });
  testWidgets("""Can a user navigate to the MiniCircuits session,run it, 
      run the longer circuit, then check all the data?""",
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp(
      fileIOFunctions: fileIOFunctions,
      fileHandlingFunctions: fileHandlingFunctions,
      speech: speech,
      appData: appData,
      runSessionTimer: runSessionTimer,
    ));
    await tester.pumpAndSettle(Duration(seconds: 4));
    await mainScreenToSession(tester, "Mini Circuits (Test)");
    await runSessionAndHome(tester, 105, "Squats");
    await mainScreenToSession(tester, "Circuit Training 1");
    await runSessionAndHome(tester, 605, "Squats");
    await gotoSessionData(tester);

    await gotoNSessionData(tester, 1);
    checkCircuits();
    await pressBackButton(tester);
    await gotoNSessionData(tester, 0);
    checkCircuits();
  });

  testWidgets("""Can a user navigate to the MiniCircuits session, run it, 
  pause after 9 seconds for 10 seconds and then view the data?""",
      (WidgetTester tester) async {
    await tester.pumpWidget(MyApp(
      fileIOFunctions: fileIOFunctions,
      fileHandlingFunctions: fileHandlingFunctions,
      speech: speech,
      appData: appData,
      runSessionTimer: runSessionTimer,
    ));
    await tester.pumpAndSettle(Duration(seconds: 4));
    await mainScreenToSession(tester, "Mini Circuits (Test)");
    await runSessionWithPauseAndHome(tester, 105, 9, 10, "Squats");
    await gotoSessionData(tester);

    await gotoNSessionData(tester, 0);
    checkCircuits();
  });
}

Future<void> pressBackButton(WidgetTester tester) async {
  final backButtonFinder = find.byKey(Key('backButton'));
  await tester.tap(backButtonFinder);
  await tester.pumpAndSettle(Duration(seconds: 4));
}

void checkCircuits() {
  final subtitleTextFinder = find.text("Squats");
  expect(subtitleTextFinder, findsOneWidget);
}

Future<void> mainScreenToSession(
    WidgetTester tester, String sessionString) async {
  final chooseSessionFinder = find.byKey(Key('mainScreenChooseSession'));
  await tester.tap(chooseSessionFinder);
  await tester.pumpAndSettle(Duration(seconds: 4));
  final sessionNameTextFinder = find.text(sessionString);
  expect(sessionNameTextFinder, findsOneWidget);
  await tester.tap(sessionNameTextFinder);
  await tester.pumpAndSettle(Duration(seconds: 4));
  final firstActivityTextFinder = find.text("Squats");
  expect(firstActivityTextFinder, findsOneWidget);
}

Future<void> runSessionAndHome(
    WidgetTester tester, int seconds, String firstActivityText) async {
  final backButtonFinder = find.byKey(Key('backButton'));
  expect(backButtonFinder, findsOneWidget);
  final playButtonFinder = find.byKey(Key('playButton'));
  expect(playButtonFinder, findsOneWidget);
  await tester.tap(playButtonFinder);
  await tester.pumpAndSettle(Duration(seconds: 1));
  final activityFinder = find.byKey(Key("SessionActivityTile_0"));
  expect(activityFinder, findsOneWidget);
  final subtitleTextFinder = find.descendant(
      of: activityFinder, matching: find.text(firstActivityText));
  expect(subtitleTextFinder, findsOneWidget);
  for (int i = 0; i < seconds; ++i) {
    await tester.pumpAndSettle(Duration(seconds: 1));
  }
  final summaryFinder = find.byType(SessionSummary);
  expect(summaryFinder, findsOneWidget);

  final homeButtonFinder = find.byKey(Key("homeButton"));
  expect(homeButtonFinder, findsOneWidget);
  await tester.tap(homeButtonFinder);
  await tester.pumpAndSettle(Duration(seconds: 4));
}

Future<void> runSessionWithPauseAndHome(WidgetTester tester, int seconds,
    int pauseTime, int pauseLength, String firstActivityText) async {
  final backButtonFinder = find.byKey(Key('backButton'));
  expect(backButtonFinder, findsOneWidget);
  final playButtonFinder = find.byKey(Key('playButton'));
  expect(playButtonFinder, findsOneWidget);
  await tester.tap(playButtonFinder);
  await tester.pumpAndSettle(Duration(seconds: 1));
  final activityFinder = find.byKey(Key("SessionActivityTile_0"));
  expect(activityFinder, findsOneWidget);
  final subtitleTextFinder = find.descendant(
      of: activityFinder, matching: find.text(firstActivityText));
  expect(subtitleTextFinder, findsOneWidget);
  for (int i = 0; i < seconds; ++i) {
    if (i == pauseTime) {
      final pauseButtonFinder = find.byKey(Key('pauseButton'));
      expect(pauseButtonFinder, findsOneWidget);
      await tester.tap(pauseButtonFinder);
      await tester.pumpAndSettle(Duration(seconds: 1));
      // int _hours = (seconds / 3600).floor();
      // int _minutes = ((seconds - (3600 * _hours)) / 60).floor();
      // int _seconds = (seconds - (60 * _minutes) - (3600 * _hours));
      // String controlPanelString = _hours.toString().padLeft(2, '0') +
      //     ':' +
      //     _minutes.toString().padLeft(2, '0') +
      //     ':' +
      //     _seconds.toString().padLeft(2, '0');
      final controlPanelFinder_1 = find.byType(RunSessionControls);
      final controlPanelText_1 = find.descendant(
          of: controlPanelFinder_1, matching: find.byType(Text));
      expect(controlPanelText_1, findsOneWidget);
      final controlPanelTextWidget_1 =
          tester.firstWidget(controlPanelText_1) as Text;
      for (int j = 0; j < pauseLength; ++j) {
        await tester.pumpAndSettle(Duration(seconds: 1));
      }
      final controlPanelFinder_2 = find.byType(RunSessionControls);
      final controlPanelText_2 = find.descendant(
          of: controlPanelFinder_2, matching: find.byType(Text));
      expect(controlPanelText_2, findsOneWidget);
      final controlPanelTextWidget_2 =
          tester.firstWidget(controlPanelText_2) as Text;
      expect(controlPanelTextWidget_1.toString(),
          controlPanelTextWidget_2.toString());
      await tester.tap(pauseButtonFinder);
      await tester.pumpAndSettle(Duration(seconds: 1));
    }
    await tester.pumpAndSettle(Duration(seconds: 1));
  }
  final summaryFinder = find.byType(SessionSummary);
  expect(summaryFinder, findsOneWidget);

  final homeButtonFinder = find.byKey(Key("homeButton"));
  expect(homeButtonFinder, findsOneWidget);
  await tester.tap(homeButtonFinder);
  await tester.pumpAndSettle(Duration(seconds: 4));
}

Future<void> gotoSessionData(WidgetTester tester) async {
  final sessionDataFinder = find.byKey(Key('mainScreenSessionData'));
  await tester.tap(sessionDataFinder);
  await tester.pumpAndSettle(Duration(seconds: 4));
  expect(find.byType(SessionDataScreen), findsOneWidget);
}

Future<void> gotoNSessionData(WidgetTester tester, int n) async {
  await tester.pumpAndSettle(Duration(seconds: 4));
  final firstItemFinder = find.byKey(Key('sessionDataList_' + n.toString()));
  expect(firstItemFinder, findsOneWidget);
  await tester.tap(firstItemFinder);
  await tester.pumpAndSettle(Duration(seconds: 3));
}
