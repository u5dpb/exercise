#!/bin/bash

gcloud auth activate-service-account --key-file=/home/danb/exercise-3dbe1-539824972d49.json

gcloud --quiet config set project exercise-3dbe1

gcloud firebase test android run --type instrumentation \
  --app build/app/outputs/apk/debug/app-debug.apk \
  --test build/app/outputs/apk/androidTest/debug/app-debug-androidTest.apk \
  --timeout 30m

