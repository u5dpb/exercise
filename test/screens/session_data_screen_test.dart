import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/screens/choose_session_screen.dart';
import 'package:exercise/screens/main_screen.dart';
import 'package:exercise/screens/session_data_screen.dart';
import 'package:exercise/screens/show_session_data_screen.dart';
import 'package:exercise/widgets/common_bottom_app_bar.dart';
import 'package:exercise/widgets/list_view/session_data_summary_list_view.dart';
import 'package:exercise/widgets/title_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../basic_data_classes/session_data_test_data.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

class MockFileHandlingFunctions extends Mock implements FileHandlingFunctions {}

main() {
  List<SessionData> sessionDataList = SessionDataTestData.sessionDataList;
  int numberOfSessionDataItems = sessionDataList.length;
  Widget testWidget;
  MockNavigatorObserver mockNavigationObserver;
  MockFileHandlingFunctions mockFileHandlingFunctions;

  setUp(() async {
    mockNavigationObserver = MockNavigatorObserver();
    mockFileHandlingFunctions = MockFileHandlingFunctions();
    when(mockFileHandlingFunctions.loadData())
        .thenAnswer((_) async => sessionDataList);
    when(mockFileHandlingFunctions.partialSessionExists())
        .thenAnswer((_) async => false);
    testWidget = ChangeNotifierProvider(
      create: (context) => AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      ),
      child: MaterialApp(
        initialRoute: SessionDataScreen.id,
        routes: {
          MainScreen.id: (context) => MainScreen(),
          ChooseSessionScreen.id: (context) => ChooseSessionScreen(),
          SessionDataScreen.id: (context) => SessionDataScreen(),
        },
        navigatorObservers: [mockNavigationObserver],
      ),
    );
  });
  testWidgets('IS the Title Visible?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    final titleBarFinder = find.byType(TitleAppBar);
    expect(titleBarFinder, findsOneWidget);
    final titleTextFinder = find.descendant(
        of: titleBarFinder, matching: find.text('Session Data'));
    expect(titleTextFinder, findsOneWidget);
  });
  group('ListView Tests', () {
    testWidgets('Is the First Session Data Visible?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final firstItemFinder = find.byKey(Key('sessionDataList_0'));
      expect(firstItemFinder, findsOneWidget);
    });
    testWidgets('Is the Last Session Data Not Visible?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      int lastIndex = numberOfSessionDataItems - 1;
      final lastItemFinder =
          find.byKey(Key('sessionDataList_' + lastIndex.toString()));
      expect(lastItemFinder, findsNothing);
    });
    testWidgets('Can we Scroll to Last Session Data?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final listViewFinder = find.byType(SessionDataSummaryListView);
      int lastIndex = numberOfSessionDataItems - 1;
      final lastItemFinder =
          find.byKey(Key('sessionDataList_' + lastIndex.toString()));
      expect(lastItemFinder, findsNothing);
      await tester.dragUntilVisible(
        lastItemFinder,
        listViewFinder,
        const Offset(0, -250),
      );
      expect(lastItemFinder, findsOneWidget);
    });
    testWidgets('After Scrolling is the First Session Data Not Visible?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final listViewFinder = find.byType(SessionDataSummaryListView);
      final firstItemFinder = find.byKey(Key('sessionDataList_0'));
      expect(firstItemFinder, findsOneWidget);
      int lastIndex = numberOfSessionDataItems - 1;
      final lastItemFinder =
          find.byKey(Key('sessionDataList_' + lastIndex.toString()));
      expect(lastItemFinder, findsNothing);
      await tester.dragUntilVisible(
        lastItemFinder,
        listViewFinder,
        const Offset(0, -250),
      );
      expect(lastItemFinder, findsOneWidget);
      expect(firstItemFinder, findsNothing);
    });
  });
  group('Bottom Bar Tests', () {
    testWidgets('Is the Bottom Bar Visible?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final bottomBarFinder = find.byType(BottomAppBar);
      expect(bottomBarFinder, findsOneWidget);
    });
    testWidgets('Is the Back Button Visible?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final bottomBarFinder = find.byType(CommonBottomAppBar);
      expect(bottomBarFinder, findsOneWidget);
      final backButtonFinder = find.descendant(
          of: bottomBarFinder, matching: find.byKey(Key('backButton')));
      expect(backButtonFinder, findsOneWidget);
    });
    testWidgets('Does the Back Button Go Back?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final bottomBarFinder = find.byType(CommonBottomAppBar);
      expect(bottomBarFinder, findsOneWidget);
      final backButtonFinder = find.descendant(
          of: bottomBarFinder, matching: find.byKey(Key('backButton')));
      expect(backButtonFinder, findsOneWidget);
      await tester.tap(backButtonFinder);
      await tester.pumpAndSettle();
      verify(mockNavigationObserver.didPop(any, any));
    });
  });
  group('View Data Tests', () {
    testWidgets('Can we Select the First Session Data?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final firstItemFinder = find.byKey(Key('sessionDataList_0'));
      expect(firstItemFinder, findsOneWidget);
      await tester.tap(firstItemFinder);
      await tester.pumpAndSettle();
      verify(mockNavigationObserver.didPush(any, any));
      expect(find.byType(SessionDataScreen), findsNothing);
      expect(find.byType(ShowSessionDataScreen), findsOneWidget);
    });
    testWidgets('Can we Select the Second Session Data?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final secondItemFinder = find.byKey(Key('sessionDataList_1'));
      expect(secondItemFinder, findsOneWidget);
      await tester.tap(secondItemFinder);
      await tester.pumpAndSettle();
      verify(mockNavigationObserver.didPush(any, any));
      expect(find.byType(SessionDataScreen), findsNothing);
      expect(find.byType(ShowSessionDataScreen), findsOneWidget);
    });
    testWidgets('Can we Scroll and Select the Last Session Data?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final listViewFinder = find.byType(SessionDataSummaryListView);
      int lastIndex = numberOfSessionDataItems - 1;
      final lastItemFinder =
          find.byKey(Key('sessionDataList_' + lastIndex.toString()));
      expect(lastItemFinder, findsNothing);
      await tester.dragUntilVisible(
        lastItemFinder,
        listViewFinder,
        const Offset(0, -250),
      );
      expect(lastItemFinder, findsOneWidget);
      await tester.tap(lastItemFinder);
      await tester.pumpAndSettle();
      verify(mockNavigationObserver.didPush(any, any));
      expect(find.byType(SessionDataScreen), findsNothing);
      expect(find.byType(ShowSessionDataScreen), findsOneWidget);
    });
  });
}
