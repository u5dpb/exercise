import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/model/run_session_timer.dart';
import 'package:exercise/screens/choose_session_screen.dart';
import 'package:exercise/screens/run_session_screen.dart';
import 'package:exercise/widgets/bottom_icon_button.dart';
import 'package:file/memory.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../basic_data_classes/session_data_test_data.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

class MockFileHandlingFunctions extends Mock implements FileHandlingFunctions {}

class MockRunSessionTimer extends Mock implements RunSessionTimer {}

main() {
  MockFileHandlingFunctions mockFileHandlingFunctions;
  MockNavigatorObserver mockObserver;
  RunSessionTimer runSessionTimer;
  MemoryFileSystem fileSystem;

  AppData appData;
  Widget testWidget;

  setUp(() {
    fileSystem = MemoryFileSystem();

    mockFileHandlingFunctions = MockFileHandlingFunctions();
    when(mockFileHandlingFunctions.loadData())
        .thenAnswer((_) async => SessionDataTestData.sessionDataList);
    when(mockFileHandlingFunctions.partialSessionExists())
        .thenAnswer((_) async => false);

    mockObserver = MockNavigatorObserver();

    appData = AppData(
      fileHandlingFunctions: mockFileHandlingFunctions,
    );
    runSessionTimer = RunSessionTimer(
      fileSystem: fileSystem,
      speachFunction: (val) {},
      storeSessionFunction: appData.addSessionData,
    );
    testWidget = MultiProvider(
      providers: [
        ChangeNotifierProvider<RunSessionTimer>(create: (_) => runSessionTimer),
        ChangeNotifierProvider<AppData>(create: (_) => appData),
      ],
      child: MaterialApp(
        home: ChooseSessionScreen(),
        navigatorObservers: [mockObserver],
      ),
    );
  });
  group('Appearance Tests:', () {
    testWidgets('First Session Visible', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final titleTextFinder = find.text('Short Run');
      expect(titleTextFinder, findsOneWidget);
    });
    testWidgets('Second Session Visible', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final titleTextFinder = find.text('Circuit Training 1');
      expect(titleTextFinder, findsOneWidget);
    });
    testWidgets('Back Button Visible', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final bottomIconButtonFinder = find.byType(BottomIconButton);
      expect(bottomIconButtonFinder, findsOneWidget);
      final bottomIconButtonWidget =
          tester.firstWidget(bottomIconButtonFinder) as BottomIconButton;
      expect(bottomIconButtonWidget.buttonIcon, Icons.arrow_back);
    });
  });
  group('Function Tests:', () {
    // can select a session
    testWidgets('Can we select the first session?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final titleTextFinder = find.text('Short Run');
      expect(titleTextFinder, findsOneWidget);
      await tester.tap(titleTextFinder);
      await tester.pumpAndSettle(Duration(seconds: 3));

      expect(find.byType(RunSession), findsOneWidget);
      expect(find.text('Choose Session'), findsNothing);
      expect(find.text('Warm up'), findsOneWidget);
    });
    testWidgets('Can we select second session?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final titleTextFinder = find.text(appData.sessionList[1].sessionName);
      expect(titleTextFinder, findsOneWidget);
      await tester.tap(titleTextFinder);
      await tester.pumpAndSettle(Duration(seconds: 3));

      expect(find.byType(RunSession), findsOneWidget);
      expect(find.text('Choose Session'), findsNothing);
    });
    testWidgets('Can we select last session?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final titleTextFinder = find.text(
          appData.sessionList[appData.sessionList.length - 1].sessionName);
      expect(titleTextFinder, findsOneWidget);
      await tester.tap(titleTextFinder);
      await tester.pumpAndSettle(Duration(seconds: 3));

      expect(find.byType(RunSession), findsOneWidget);
      expect(find.text('Choose Session'), findsNothing);
    });
    // back buttopn works
    testWidgets('Does the back button work?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      await tester.pumpAndSettle();
      final bottomIconButtonFinder = find.byType(BottomIconButton);
      expect(bottomIconButtonFinder, findsOneWidget);
      await tester.tap(bottomIconButtonFinder);
      await tester.pumpAndSettle();
      verify(mockObserver.didPop(any, any));
      await tester.pumpAndSettle();
    });
  });
}
