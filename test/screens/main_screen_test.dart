import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/screens/choose_session_screen.dart';
import 'package:exercise/screens/main_screen.dart';
import 'package:exercise/screens/session_data_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

class MockFileHandlingFunctions extends Mock implements FileHandlingFunctions {}

main() {
  List<SessionData> emptySessionDataList = [];

  MockNavigatorObserver mockNavigationObserver;
  MockFileHandlingFunctions mockFileHandlingFunctions;

  Widget testWidget;
  setUp(() async {
    mockNavigationObserver = MockNavigatorObserver();
    mockFileHandlingFunctions = MockFileHandlingFunctions();

    when(mockFileHandlingFunctions.loadData())
        .thenAnswer((_) async => emptySessionDataList);
    when(mockFileHandlingFunctions.partialSessionExists())
        .thenAnswer((_) async => false);

    testWidget = ChangeNotifierProvider(
      create: (context) => AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      ),
      child: MaterialApp(
        initialRoute: MainScreen.id,
        routes: {
          MainScreen.id: (context) => MainScreen(),
          ChooseSessionScreen.id: (context) => ChooseSessionScreen(),
          SessionDataScreen.id: (context) => SessionDataScreen(),
        },
        navigatorObservers: [mockNavigationObserver],
      ),
    );
  });
  group('Appearance', () {
    testWidgets('Does the Big Logo appear?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      final logoFinder = find.byKey(Key('mainScreenLogo'));
      expect(logoFinder, findsOneWidget);
      final logoWidget =
          (tester.firstWidget(logoFinder) as Image).image as AssetImage;
      expect(logoWidget.assetName, 'assets/logo_2_full.png');
    });
    testWidgets('Is Choose Session visible?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      final chooseSessionFinder = find.text('Choose Session');
      expect(chooseSessionFinder, findsOneWidget);
    });
    testWidgets('Is Session Data visible?', (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      final sessionDataFinder = find.text('Session Records');
      expect(sessionDataFinder, findsOneWidget);
    });
  });
  group('Function', () {
    testWidgets('Does Choose Session link to the ChooseSession Screen?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      final chooseSessionFinder = find.byKey(Key('mainScreenChooseSession'));
      await tester.tap(chooseSessionFinder);
      await tester.pumpAndSettle();
      verify(mockNavigationObserver.didPush(any, any));
      await tester.pumpAndSettle();
      expect(find.byType(MainScreen), findsNothing);
      expect(find.byType(ChooseSessionScreen), findsOneWidget);
    });
    testWidgets('Does Session Data link to the SessionData Screen?',
        (WidgetTester tester) async {
      await tester.pumpWidget(testWidget);
      final sessionDataFinder = find.byKey(Key('mainScreenSessionData'));
      await tester.tap(sessionDataFinder);
      await tester.pumpAndSettle();
      verify(mockNavigationObserver.didPush(any, any));
      await tester.pumpAndSettle();
      expect(find.byType(MainScreen), findsNothing);
      expect(find.byType(SessionDataScreen), findsOneWidget);
    });
  });
}
