import 'package:exercise/model/failure.dart';
import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/model/run_session_timer.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/basic_data_classes/session_data_partial.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/screens/loading_screen.dart';
import 'package:exercise/screens/main_screen.dart';
import 'package:exercise/screens/run_session_screen.dart';
import 'package:exercise/widgets/logo.dart';
import 'package:file/memory.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../basic_data_classes/session_data_partial_test_data.dart';
import '../basic_data_classes/session_data_test_data.dart';

class FakeRunSession extends StatelessWidget {
  FakeRunSession();
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Text('Fake Run Session Screen'));
  }
}

class MockFileHandlingFunctions extends Mock implements FileHandlingFunctions {}

main() {
  MemoryFileSystem fileSystem;
  RunSessionTimer runSessionTimer;

  AppData appData;
  MockFileHandlingFunctions mockFileHandlingFunctions;
  List<SessionData> sessionDataList = SessionDataTestData.sessionDataList;
  SessionDataPartial sessionDataPartial =
      SessionDataPartialTestData.unfinishedSession;

  Widget testMaterialApp;

  setUp(() {
    mockFileHandlingFunctions = MockFileHandlingFunctions();
    fileSystem = MemoryFileSystem();
    testMaterialApp = MaterialApp(
      //home: LoadingScreen(),
      initialRoute: LoadingScreen.id,
      routes: {
        LoadingScreen.id: (context) => LoadingScreen(),
        MainScreen.id: (context) => MainScreen(),
      },
    );
  });
  group('Regular Behaviour Tests:', () {
    setUp(() {});
    testWidgets('Is the logo Visible on startup?', (WidgetTester tester) async {
      when(mockFileHandlingFunctions.loadData())
          .thenAnswer((_) async => sessionDataList);
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => false);
      appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      runSessionTimer = RunSessionTimer(
        fileSystem: fileSystem,
        speachFunction: (val) {},
        storeSessionFunction: appData.addSessionData,
      );
      await tester.pumpWidget(
        MultiProvider(
          providers: [
            ChangeNotifierProvider<RunSessionTimer>(
                create: (_) => runSessionTimer),
            ChangeNotifierProvider<AppData>(create: (_) => appData),
          ],
          child: testMaterialApp,
        ),
      );
      final logoFinder = find.byType(Logo);
      expect(logoFinder, findsOneWidget);
      final mainScreenFinder = find.byType(MainScreen);
      expect(mainScreenFinder, findsNothing);
      verify(mockFileHandlingFunctions.loadData());
      verify(mockFileHandlingFunctions.partialSessionExists());
    });
    testWidgets('Is the main screen visible after startup?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenAnswer((_) async => sessionDataList);
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => false);

        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));
        tester.binding.scheduleFrame();
        await Future.delayed(Duration(seconds: 2));

        final loadingScreenFinder = find.byType(LoadingScreen);
        expect(loadingScreenFinder, findsNothing);
        final spinnerFinder = find.byType(SpinKitWanderingCubes);
        expect(spinnerFinder, findsNothing);
        final mainScreenFinder = find.byType(MainScreen);
        expect(mainScreenFinder, findsOneWidget);
        verify(mockFileHandlingFunctions.loadData());
        verify(mockFileHandlingFunctions.partialSessionExists());
      });
    });
  });
  group('Error Screen Tests:', () {
    setUp(() {});
    testWidgets('Is there an error after loadData Failure?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenThrow(Failure('This is a failure'));
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => false);

        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));

        final loadingScreenFinder = find.byType(LoadingScreen);
        expect(loadingScreenFinder, findsOneWidget);
        final errorScreenFinder = find.byType(AlertDialog);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder, matching: find.text('Error Loading Data'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenFailureText = find.descendant(
            of: errorScreenFinder, matching: find.text('This is a failure'));
        expect(errorScreenFailureText, findsOneWidget);
        final errorScreenContinue = find.descendant(
            of: errorScreenFinder, matching: find.text('Try to Continue'));
        expect(errorScreenContinue, findsOneWidget);

        final errorScreenRetry = find.descendant(
            of: errorScreenFinder, matching: find.text('Retry'));
        expect(errorScreenRetry, findsOneWidget);

        final spinnerFinder = find.byType(SpinKitWanderingCubes);
        expect(spinnerFinder, findsNothing);
        final mainScreenFinder = find.byType(MainScreen);
        expect(mainScreenFinder, findsNothing);
        verify(mockFileHandlingFunctions.loadData());
        verifyNever(mockFileHandlingFunctions.partialSessionExists());
      });
    });
    testWidgets('''After there is an loadData Failure 
    pressing Continue loads the main screen?''', (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenThrow(Failure('This is a failure'));
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => false);
        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));

        final errorScreenFinder = find.byType(AlertDialog);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder, matching: find.text('Error Loading Data'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenContinue = find.descendant(
            of: errorScreenFinder, matching: find.text('Try to Continue'));
        expect(errorScreenContinue, findsOneWidget);

        final errorScreenRetry = find.descendant(
            of: errorScreenFinder, matching: find.text('Retry'));
        expect(errorScreenRetry, findsOneWidget);
        await tester.tap(errorScreenContinue);
        await tester.pumpAndSettle(Duration(seconds: 3));
        tester.binding.scheduleFrame();
        await Future.delayed(Duration(seconds: 2));
        final mainScreenFinder = find.byType(MainScreen);
        expect(mainScreenFinder, findsOneWidget);
      });
    });
    testWidgets('''After a loadData Failure pressing 
    Retry relaods the Loading Screen?''', (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenThrow(Failure('This is a failure'));
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => false);
        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));
        final errorScreenFinder = find.byType(AlertDialog);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder, matching: find.text('Error Loading Data'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenContinue = find.descendant(
            of: errorScreenFinder, matching: find.text('Try to Continue'));
        expect(errorScreenContinue, findsOneWidget);

        final errorScreenRetry = find.descendant(
            of: errorScreenFinder, matching: find.text('Retry'));
        expect(errorScreenRetry, findsOneWidget);

        await tester.tap(errorScreenRetry);
        await tester.pumpAndSettle(Duration(seconds: 3));
        tester.binding.scheduleFrame();
        await Future.delayed(Duration(seconds: 2));
        final mainScreenFinder = find.byType(LoadingScreen);
        expect(mainScreenFinder, findsOneWidget);
      });
    });
  });
  group('Partial Session Found Tests:', () {
    setUp(() {});
    testWidgets('Does the Partial Data Found Screen load?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenAnswer((_) async => sessionDataList);
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => true);
        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));

        final errorScreenFinder = find.byType(AlertDialog);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder,
            matching: find.text('Incomplete Session Data Found'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenContinue = find.descendant(
            of: errorScreenFinder, matching: find.text('Continue the session'));
        expect(errorScreenContinue, findsOneWidget);

        final errorScreenRetry = find.descendant(
            of: errorScreenFinder, matching: find.text('Delete the data'));
        expect(errorScreenRetry, findsOneWidget);
      });
    });
    testWidgets('When Partial Data Found and Loaded RunSession screen shows?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenAnswer((_) async => sessionDataList);
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => true);
        when(mockFileHandlingFunctions.loadPartialSession())
            .thenAnswer((_) async => sessionDataPartial);
        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));

        final errorScreenFinder = find.byType(AlertDialog);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder,
            matching: find.text('Incomplete Session Data Found'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenContinue = find.descendant(
            of: errorScreenFinder, matching: find.text('Continue the session'));
        expect(errorScreenContinue, findsOneWidget);

        final errorScreenDelete = find.descendant(
            of: errorScreenFinder, matching: find.text('Delete the data'));
        expect(errorScreenDelete, findsOneWidget);

        await tester.tap(errorScreenContinue);
        await tester.pumpAndSettle(Duration(seconds: 3));
        final runSessionScreenFinder = find.byType(RunSession);
        expect(runSessionScreenFinder, findsOneWidget);
        verify(mockFileHandlingFunctions.loadPartialSession());
      });
    });
    testWidgets('''If Partial Data Found and Delete 
    selected the file is deleted and MainScreen loads?''',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenAnswer((_) async => sessionDataList);
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => true);
        when(mockFileHandlingFunctions.loadPartialSession())
            .thenAnswer((_) async => sessionDataPartial);
        when(mockFileHandlingFunctions.deletePartialSession())
            .thenReturn((_) => () {
                  return;
                });
        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));

        final errorScreenFinder = find.byType(AlertDialog);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder,
            matching: find.text('Incomplete Session Data Found'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenContinue = find.descendant(
            of: errorScreenFinder, matching: find.text('Continue the session'));
        expect(errorScreenContinue, findsOneWidget);

        final errorScreenDelete = find.descendant(
            of: errorScreenFinder, matching: find.text('Delete the data'));
        expect(errorScreenDelete, findsOneWidget);

        await tester.tap(errorScreenDelete);

        await tester.pumpAndSettle(Duration(seconds: 3));

        final mainScreenFinder = find.byType(MainScreen);
        expect(mainScreenFinder, findsOneWidget);
        verify(mockFileHandlingFunctions.deletePartialSession(
            filename: "currentSessionData.xml"));
      });
    });
  });
  group('Partial Session Error Tests:', () {
    setUp(() {});
    testWidgets('''Does the Error Screen show if 
    there is a Partial Data load error?''', (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenAnswer((_) async => sessionDataList);
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => true);
        when(mockFileHandlingFunctions.loadPartialSession())
            .thenThrow(Failure('This is a failure'));
        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));

        await Future.delayed(Duration(seconds: 2));
        final incompleteScreenFinder = find.byType(ParitalFoundErrorScreen);
        expect(incompleteScreenFinder, findsOneWidget);
        final incompleteScreenTitleText = find.descendant(
            of: incompleteScreenFinder,
            matching: find.text('Incomplete Session Data Found'));
        expect(incompleteScreenTitleText, findsOneWidget);
        final incompleteScreenContinue = find.descendant(
            of: incompleteScreenFinder,
            matching: find.text('Continue the session'));
        expect(incompleteScreenContinue, findsOneWidget);

        final incompleteScreenDelete = find.descendant(
            of: incompleteScreenFinder, matching: find.text('Delete the data'));
        expect(incompleteScreenDelete, findsOneWidget);

        await tester.tap(incompleteScreenContinue);
        await tester.pumpAndSettle(Duration(seconds: 3));
        verify(mockFileHandlingFunctions.loadPartialSession());

        final errorScreenFinder = find.byType(PartialLoadErrorScreen);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder,
            matching: find.text('Error Loading Restart Session'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenRetry = find.descendant(
            of: errorScreenFinder, matching: find.text('Retry'));
        expect(errorScreenRetry, findsOneWidget);

        final errorScreenDelete = find.descendant(
            of: errorScreenFinder, matching: find.text('Delete'));
        expect(errorScreenDelete, findsOneWidget);
      });
    });
    testWidgets('''On Partial Data Error and Retry 
    pressed the Loading Screen reloads?''', (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenAnswer((_) async => sessionDataList);
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => true);
        when(mockFileHandlingFunctions.loadPartialSession())
            .thenThrow(Failure('This is a failure'));
        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));

        await Future.delayed(Duration(seconds: 2));
        final incompleteScreenFinder = find.byType(ParitalFoundErrorScreen);
        expect(incompleteScreenFinder, findsOneWidget);
        final incompleteScreenTitleText = find.descendant(
            of: incompleteScreenFinder,
            matching: find.text('Incomplete Session Data Found'));
        expect(incompleteScreenTitleText, findsOneWidget);
        final incompleteScreenContinue = find.descendant(
            of: incompleteScreenFinder,
            matching: find.text('Continue the session'));
        expect(incompleteScreenContinue, findsOneWidget);

        final incompleteScreenDelete = find.descendant(
            of: incompleteScreenFinder, matching: find.text('Delete the data'));
        expect(incompleteScreenDelete, findsOneWidget);

        await tester.tap(incompleteScreenContinue);
        await tester.pumpAndSettle(Duration(seconds: 3));
        verify(mockFileHandlingFunctions.loadPartialSession());

        final errorScreenFinder = find.byType(PartialLoadErrorScreen);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder,
            matching: find.text('Error Loading Restart Session'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenRetry = find.descendant(
            of: errorScreenFinder, matching: find.text('Retry'));
        expect(errorScreenRetry, findsOneWidget);

        final errorScreenDelete = find.descendant(
            of: errorScreenFinder, matching: find.text('Delete'));
        expect(errorScreenDelete, findsOneWidget);

        await tester.tap(errorScreenRetry);
        await tester.pumpAndSettle(Duration(seconds: 3));
        tester.binding.scheduleFrame();
        await Future.delayed(Duration(seconds: 2));
        final mainScreenFinder = find.byType(LoadingScreen);
        expect(mainScreenFinder, findsOneWidget);
      });
    });
    testWidgets('''If Partial Data Error occurs and Delete selected is the 
    file deleted and the MainScreen appear?''', (WidgetTester tester) async {
      await tester.runAsync(() async {
        when(mockFileHandlingFunctions.loadData())
            .thenAnswer((_) async => sessionDataList);
        when(mockFileHandlingFunctions.partialSessionExists())
            .thenAnswer((_) async => true);
        when(mockFileHandlingFunctions.loadPartialSession())
            .thenThrow(Failure('This is a failure'));
        when(mockFileHandlingFunctions.deletePartialSession())
            .thenReturn((_) => () {
                  return;
                });
        appData = AppData(
          fileHandlingFunctions: mockFileHandlingFunctions,
        );
        runSessionTimer = RunSessionTimer(
          fileSystem: fileSystem,
          speachFunction: (val) {},
          storeSessionFunction: appData.addSessionData,
        );
        await tester.pumpWidget(
          MultiProvider(
            providers: [
              ChangeNotifierProvider<RunSessionTimer>(
                  create: (_) => runSessionTimer),
              ChangeNotifierProvider<AppData>(create: (_) => appData),
            ],
            child: testMaterialApp,
          ),
        );
        await tester.pumpAndSettle(Duration(seconds: 3));
        //tester.binding.scheduleFrame();
        await Future.delayed(Duration(seconds: 2));
        final incompleteScreenFinder = find.byType(ParitalFoundErrorScreen);
        expect(incompleteScreenFinder, findsOneWidget);
        final incompleteScreenTitleText = find.descendant(
            of: incompleteScreenFinder,
            matching: find.text('Incomplete Session Data Found'));
        expect(incompleteScreenTitleText, findsOneWidget);
        final incompleteScreenContinue = find.descendant(
            of: incompleteScreenFinder,
            matching: find.text('Continue the session'));
        expect(incompleteScreenContinue, findsOneWidget);

        final incompleteScreenDelete = find.descendant(
            of: incompleteScreenFinder, matching: find.text('Delete the data'));
        expect(incompleteScreenDelete, findsOneWidget);

        await tester.tap(incompleteScreenContinue);
        await tester.pumpAndSettle(Duration(seconds: 3));

        verify(mockFileHandlingFunctions.loadPartialSession());
        final errorScreenFinder = find.byType(PartialLoadErrorScreen);
        expect(errorScreenFinder, findsOneWidget);
        final errorScreenTitleText = find.descendant(
            of: errorScreenFinder,
            matching: find.text('Error Loading Restart Session'));
        expect(errorScreenTitleText, findsOneWidget);
        final errorScreenRetry = find.descendant(
            of: errorScreenFinder, matching: find.text('Retry'));
        expect(errorScreenRetry, findsOneWidget);

        final errorScreenDelete = find.descendant(
            of: errorScreenFinder, matching: find.text('Delete'));
        expect(errorScreenDelete, findsOneWidget);

        await tester.tap(errorScreenDelete);

        await tester.pumpAndSettle(Duration(seconds: 3));
        await Future.delayed(Duration(seconds: 2));
        verify(mockFileHandlingFunctions.deletePartialSession(
            filename: "currentSessionData.xml"));
        final mainScreenFinder = find.byType(MainScreen);
        expect(mainScreenFinder, findsOneWidget);
      });
    });
  });
}
