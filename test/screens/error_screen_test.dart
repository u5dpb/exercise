import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:exercise/screens/error_screen.dart';

void main() {
  String log;
  Widget testWidget;
  setUp(() {
    log = '';
    testWidget = MaterialApp(
        home: ErrorScreen(
      title: 'Error Screen',
      description: 'This is a test error screen',
      actions: [
        ErrorAction(
            description: 'This is action one.',
            onChoose: () {
              log = 'action one';
            }),
        ErrorAction(
            description: 'This is action two.',
            onChoose: () {
              log = 'action two';
            }),
        ErrorAction(
            description: 'This is action three.',
            onChoose: () {
              log = 'action three';
            })
      ],
    ));
  });
  testWidgets('Is the description and title correct?',
      (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    final titleFinder = find.text('Error Screen');
    final messageFinder = find.text('This is a test error screen');

    expect(titleFinder, findsOneWidget);
    expect(messageFinder, findsOneWidget);
  });
  testWidgets('Do the buttons tap correctly?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);

    await tester.tap(find.byKey(Key('errorAction_0')));
    expect(log, 'action one');
    await tester.tap(find.byKey(Key('errorAction_1')));
    expect(log, 'action two');
    await tester.tap(find.byKey(Key('errorAction_2')));
    expect(log, 'action three');
  });
}
