//import 'dart:math';

import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/screens/show_session_data_screen.dart';
import 'package:exercise/widgets/title_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../basic_data_classes/session_data_test_data.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

main() {
  SessionData sessionData = SessionDataTestData.longSessionData;
  Widget testWidget;
  MockNavigatorObserver mockNavigationObserver;

  setUp(() async {
    mockNavigationObserver = MockNavigatorObserver();

    testWidget = MaterialApp(
      home: ShowSessionDataScreen(
        sessionData: sessionData,
      ),
      navigatorObservers: [mockNavigationObserver],
    );
  });
  testWidgets('Is the Title Visible?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    final appBarFinder = find.byType(TitleAppBar);
    expect(appBarFinder, findsOneWidget);
    String expectedTitle =
        sessionData.getDate() + ' ' + sessionData.sessionName;
    final titleTextFinder =
        find.descendant(of: appBarFinder, matching: find.text(expectedTitle));
    expect(titleTextFinder, findsOneWidget);
  });
  testWidgets('Is the First Activity Visible?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = 0;
    final firstActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(firstActivityFinder, findsOneWidget);
    final firstActivityTextFinder = find.descendant(
        of: firstActivityFinder,
        matching: find.text(sessionData.activities[index].activityName));
    expect(firstActivityTextFinder, findsOneWidget);
  });
  testWidgets('Is the Second Activity Visible?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = 1;
    final secondActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(secondActivityFinder, findsOneWidget);
    final secondActivityTextFinder = find.descendant(
        of: secondActivityFinder,
        matching: find.text(sessionData.activities[index].activityName));
    expect(secondActivityTextFinder, findsOneWidget);
  });
  testWidgets('Is the Last Activity Not Visible?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = sessionData.activities.length - 1;
    final lastActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(lastActivityFinder, findsNothing);
  });
  testWidgets('Can we Scroll to the Last Activity Visible?',
      (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    final listViewFinder = find.byType(ListView);
    int index = sessionData.activities.length - 1;
    final lastActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(lastActivityFinder, findsNothing);
    await tester.dragUntilVisible(
      lastActivityFinder,
      listViewFinder,
      const Offset(0, -250),
    );
    expect(lastActivityFinder, findsOneWidget);
    final lastActivityTextFinder = find.descendant(
        of: lastActivityFinder,
        matching: find.text(sessionData.activities[index].activityName));
    expect(lastActivityTextFinder, findsOneWidget);
  });
  testWidgets('On Scroll is the First Activity Not Visible?',
      (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    final listViewFinder = find.byType(ListView);
    int index = sessionData.activities.length - 1;
    final lastActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(lastActivityFinder, findsNothing);
    await tester.dragUntilVisible(
      lastActivityFinder,
      listViewFinder,
      const Offset(0, -250),
    );
    expect(lastActivityFinder, findsOneWidget);
    index = 0;
    final firstActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(firstActivityFinder, findsNothing);
  });
  testWidgets('Is the Bottom App Bar Visible?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    final bottomBarFinder = find.byType(BottomAppBar);
    expect(bottomBarFinder, findsOneWidget);
  });
  testWidgets('Is the Back Button Visible?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    final bottomBarFinder = find.byType(BottomAppBar);
    expect(bottomBarFinder, findsOneWidget);
    final backButtonFinder = find.descendant(
        of: bottomBarFinder, matching: find.byKey(Key('backButton')));
    expect(backButtonFinder, findsOneWidget);
  });
  testWidgets('Does the Back Button Work?', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    final bottomBarFinder = find.byType(BottomAppBar);
    expect(bottomBarFinder, findsOneWidget);
    final backButtonFinder = find.descendant(
        of: bottomBarFinder, matching: find.byKey(Key('backButton')));
    expect(backButtonFinder, findsOneWidget);
    await tester.tap(backButtonFinder);
    await tester.pumpAndSettle();
    verify(mockNavigationObserver.didPop(any, any));
  });
}
