import 'package:exercise/basic_data_classes/basic_sessions.dart';
import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/model/run_session_timer.dart';
//import 'package:exercise/model/run_session_timer.dart';
import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/model/speak.dart';
import 'package:exercise/screens/main_screen.dart';
import 'package:exercise/screens/run_session_screen.dart';
import 'package:exercise/widgets/list_view/partial_activity_list.dart';
import 'package:exercise/widgets/run_session_bottom_bar.dart';
import 'package:exercise/widgets/run_session_controls.dart';
import 'package:exercise/widgets/title_bar.dart';
import 'package:file/memory.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart';
import 'package:fake_async/fake_async.dart';
//import 'package:time/time.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

class MockSpeak extends Mock implements Speak {}

class MockFileHandlingFunctions extends Mock implements FileHandlingFunctions {}

main() {
  BasicSessions();
  MemoryFileSystem fileSystem;
  Session testSession = BasicSessions.miniCircuits;
  List<SessionData> sessionDataList = [];
  RunSessionTimer runSessionTimer;

  AppData appData;

  Widget testWidget;
  MockNavigatorObserver mockObserver;
  MockFileHandlingFunctions mockFileHandlingFunctions;

  MockSpeak mockSpeak;
  String mockSpeakText = "";
  setUp(() {
    fileSystem = MemoryFileSystem();

    mockObserver = MockNavigatorObserver();

    mockSpeak = MockSpeak();
    mockSpeakText = "";
    when(mockSpeak.speak(any)).thenAnswer(
      (args) async {
        mockSpeakText = args.positionalArguments[0];
      },
    );

    mockFileHandlingFunctions = MockFileHandlingFunctions();
    when(mockFileHandlingFunctions.loadData())
        .thenAnswer((_) async => sessionDataList);
    when(mockFileHandlingFunctions.partialSessionExists())
        .thenAnswer((_) async => false);

    appData = AppData(
      fileHandlingFunctions: mockFileHandlingFunctions,
    );

    runSessionTimer = RunSessionTimer(
      fileSystem: fileSystem,
      speachFunction: (val) {
        mockSpeak.speak(val);
      },
      storeSessionFunction: appData.addSessionData,
    );
    runSessionTimer.currentSession = testSession;
    testWidget = MultiProvider(
      providers: [
        ChangeNotifierProvider<RunSessionTimer>(create: (_) => runSessionTimer),
        ChangeNotifierProvider<AppData>(create: (_) => appData),
      ],
      child: MaterialApp(
        initialRoute: 'main',
        routes: {
          'main': (context) => RunSession(
                fileSystem: fileSystem,
                speechDriver: mockSpeak,
                restoreSession: false,
              ),
          MainScreen.id: (context) => Container(child: Text('A Main Screen')),
          '/': (context) => Container(child: Text('The Previous Screen'))
        },
        navigatorObservers: [mockObserver],
      ),
    );
  });
  group('Initial Appearance Tests:', () {
    testWidgets('Is the Title Bar Visible?', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle(Duration(seconds: 3));
        final titleBarFinder = find.byType(TitleAppBar);
        expect(titleBarFinder, findsOneWidget);
        final titleBarTextFinder = find.descendant(
            of: titleBarFinder, matching: find.text('Run Session'));
        expect(titleBarTextFinder, findsOneWidget);
      });
    });
    testWidgets('is the First Actvitiy Visible?', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle(Duration(seconds: 3));
        final firstActivityFinder = find.byKey(Key('SessionActivityTile_0'));
        expect(firstActivityFinder, findsOneWidget);
        final firstActivityTextFinder = find.descendant(
            of: firstActivityFinder, matching: find.text('Squats'));
        expect(firstActivityTextFinder, findsOneWidget);
      });
    });
    testWidgets('Is the Pause Button Visible?', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle(Duration(seconds: 3));
        final pauseButtonFinder = find.byKey(Key('pauseButton'));
        expect(pauseButtonFinder, findsOneWidget);
      });
    });
    testWidgets('Is the Play Button Visible?', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle(Duration(seconds: 3));
        final playButtonFinder = find.byKey(Key('playButton'));
        expect(playButtonFinder, findsOneWidget);
      });
    });
    testWidgets('is the Stop Button Visible?', (WidgetTester tester) async {
      await tester.runAsync(() async {
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle(Duration(seconds: 3));
        final stopButtonFinder = find.byKey(Key('stopButton'));
        expect(stopButtonFinder, findsOneWidget);
      });
    });
    group('Scrollable View:', () {
      testWidgets('Is the Second Activity Visible?',
          (WidgetTester tester) async {
        await tester.runAsync(() async {
          await tester.pumpWidget(testWidget);
          await tester.pumpAndSettle(Duration(seconds: 3));
          final secondActivityFinder = find.byKey(Key('SessionActivityTile_1'));
          expect(secondActivityFinder, findsOneWidget);
          final secondActivityTextFinder = find.descendant(
              of: secondActivityFinder, matching: find.text('Lunges'));
          expect(secondActivityTextFinder, findsOneWidget);
        });
      });
      testWidgets('Is the Last Acitvity Not Visible?',
          (WidgetTester tester) async {
        await tester.runAsync(() async {
          await tester.pumpWidget(testWidget);
          await tester.pumpAndSettle(Duration(seconds: 3));
          final ninthActivityFinder = find.byKey(Key('SessionActivityTile_8'));
          expect(ninthActivityFinder, findsNothing);
        });
      });
      testWidgets('Can we Scroll to the Last Activity?',
          (WidgetTester tester) async {
        await tester.runAsync(() async {
          await tester.pumpWidget(testWidget);
          await tester.pumpAndSettle(Duration(seconds: 3));
          final listViewFinder = find.byType(PartialActivityList);
          expect(listViewFinder, findsOneWidget);
          final ninthActivityFinder = find.byKey(Key('SessionActivityTile_8'));
          expect(ninthActivityFinder, findsNothing);
          await tester.dragUntilVisible(
            ninthActivityFinder,
            listViewFinder,
            const Offset(0, -250),
          );
          expect(ninthActivityFinder, findsOneWidget);
        });
      });
      testWidgets('''On Scroll is the First Activity Still 
      Visible and the Second Not Visible?''', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await tester.pumpWidget(testWidget);
          await tester.pumpAndSettle(Duration(seconds: 3));
          final listViewFinder = find.byType(PartialActivityList);
          expect(listViewFinder, findsOneWidget);
          final ninthActivityFinder = find.byKey(Key('SessionActivityTile_8'));
          expect(ninthActivityFinder, findsNothing);
          await tester.dragUntilVisible(
            ninthActivityFinder,
            listViewFinder,
            const Offset(0, -250),
          );
          expect(ninthActivityFinder, findsOneWidget);
          final firstActivityFinder = find.byKey(Key('SessionActivityTile_0'));
          expect(firstActivityFinder, findsOneWidget);
          final secondActivityFinder = find.byKey(Key('SessionActivityTile_1'));
          expect(secondActivityFinder, findsNothing);
        });
      });
    });
    group('Bottom Bar:', () {
      testWidgets('is it Visible?', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await tester.pumpWidget(testWidget);
          await tester.pumpAndSettle(Duration(seconds: 3));
          final bottomBarFinder = find.byType(RunSessionBottomBar);
          expect(bottomBarFinder, findsOneWidget);
        });
      });
      testWidgets('is the Back Button Visible?', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await tester.pumpWidget(testWidget);
          await tester.pumpAndSettle(Duration(seconds: 3));
          final backButtonFinder = find.byKey(Key('backButton'));
          expect(backButtonFinder, findsOneWidget);
        });
      });
      testWidgets('Does the Back Button Go Back?', (WidgetTester tester) async {
        await tester.runAsync(() async {
          await tester.pumpWidget(testWidget);
          await tester.pumpAndSettle(Duration(seconds: 3));
          final backButtonFinder = find.byKey(Key('backButton'));
          expect(backButtonFinder, findsOneWidget);
          await tester.tap(backButtonFinder);
          await tester.pumpAndSettle();
          verify(mockObserver.didPop(any, any));
        });
      });
      testWidgets('Is the Home Button Not Visible?',
          (WidgetTester tester) async {
        await tester.runAsync(() async {
          await tester.pumpWidget(testWidget);
          await tester.pumpAndSettle(Duration(seconds: 3));
          final backButtonFinder = find.byKey(Key('homeButton'));
          expect(backButtonFinder, findsNothing);
        });
      });
    });
  });
  group('Appearance on Play Tests:', () {
    group('Current Activity Section:', () {
      testWidgets('Does the Play Button Start the Clock?',
          (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // find the modified state
          final controlPanelFinder = find.byType(RunSessionControls);
          final controlPanelText = find.descendant(
              of: controlPanelFinder, matching: find.text('00:00:04'));
          expect(controlPanelText, findsOneWidget);
        });
      });
      testWidgets('Is the Bottom Bar Not Visible?',
          (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // find the modified state
          final bottomBarFinder = find.byType(RunSessionBottomBar);

          expect(bottomBarFinder, findsNothing);
        });
      });
      testWidgets('Does the Pause button Pause the Display?',
          (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // find the button
          final pauseButtonFinder = find.byKey(Key('pauseButton'));
          expect(pauseButtonFinder, findsOneWidget);
          // push the button
          tester.tap(pauseButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // find the modified state
          final controlPanelFinder_1 = find.byType(RunSessionControls);
          final controlPanelText_1 = find.descendant(
              of: controlPanelFinder_1, matching: find.text('00:00:04'));
          expect(controlPanelText_1, findsOneWidget);
          // advance the clock
          async.elapse(Duration(seconds: 40));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // find the modified state
          final controlPanelFinder_2 = find.byType(RunSessionControls);
          final controlPanelText_2 = find.descendant(
              of: controlPanelFinder_2, matching: find.text('00:00:04'));
          expect(controlPanelText_2, findsOneWidget);
        });
      });
      testWidgets('Does the First Activity Disappear on completion?',
          (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // check first activity is there
          final firstActivityFinder_1 =
              find.byKey(Key('SessionActivityTile_0'));
          expect(firstActivityFinder_1, findsOneWidget);
          final firstActivityTextFinder_1 = find.descendant(
              of: firstActivityFinder_1, matching: find.text('Squats'));
          expect(firstActivityTextFinder_1, findsOneWidget);
          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 12));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // check first actvitiy is no longer there
          final firstActivityFinder_2 =
              find.byKey(Key('SessionActivityTile_0'));
          expect(firstActivityFinder_2, findsNothing);
        });
      });
      testWidgets('On start of the Second Activity is it Always Visible?',
          (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 12));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // check second activity is there
          final secondActivityFinder_1 =
              find.byKey(Key('SessionActivityTile_1'));
          expect(secondActivityFinder_1, findsOneWidget);
          final secondActivityTextFinder_1 = find.descendant(
              of: secondActivityFinder_1, matching: find.text('Lunges'));
          expect(secondActivityTextFinder_1, findsOneWidget);
          // find the last activity
          final listViewFinder = find.byType(PartialActivityList);
          expect(listViewFinder, findsOneWidget);
          // check last activity not there
          final ninthActivityFinder = find.byKey(Key('SessionActivityTile_8'));
          expect(ninthActivityFinder, findsNothing);
          // scroll to the last activity
          tester.pumpAndSettle();
          async.flushMicrotasks();
          tester.dragUntilVisible(
            ninthActivityFinder,
            listViewFinder,
            const Offset(0, -250),
          );
          async.elapse(Duration(seconds: 2));
          // check last actvitiy there
          expect(ninthActivityFinder, findsOneWidget);
          // check second activity is still there
          expect(secondActivityFinder_1, findsOneWidget);
        });
      });
    });
    group('Scrollable View:', () {
      testWidgets('''On start of the second activity is 
      the Third Activity  Visible?''', (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 22));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // check second activity is there
          final thirdActivityFinder = find.byKey(Key('SessionActivityTile_2'));
          expect(thirdActivityFinder, findsOneWidget);
          final thirdActivityTextFinder = find.descendant(
              of: thirdActivityFinder, matching: find.text('Push-Ups'));
          expect(thirdActivityTextFinder, findsOneWidget);
        });
      });
      testWidgets('''On start of the second activity 
      is the Last Activity Not Visible''', (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 22));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // check last activity not there
          final ninthActivityFinder = find.byKey(Key('SessionActivityTile_8'));
          expect(ninthActivityFinder, findsNothing);
        });
      });
      testWidgets('''On start of the second activity 
      can we Scroll to the Last Activity''', (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 22));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // check last activity not there
          final ninthActivityFinder = find.byKey(Key('SessionActivityTile_8'));
          expect(ninthActivityFinder, findsNothing);
          // scroll to the last activity
          tester.pumpAndSettle();
          async.flushMicrotasks();
          final listViewFinder = find.byType(PartialActivityList);
          expect(listViewFinder, findsOneWidget);
          tester.dragUntilVisible(
            ninthActivityFinder,
            listViewFinder,
            const Offset(0, -250),
          );
          async.elapse(Duration(seconds: 2));
          // check last actvitiy there
          expect(ninthActivityFinder, findsOneWidget);
        });
      });
    });
    group('Session Progression:', () {
      testWidgets('On Last Activity do No Further Activities Appear?',
          (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 92));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // check last activity is there
          final ninthActivityFinder = find.byKey(Key('SessionActivityTile_8'));
          expect(ninthActivityFinder, findsOneWidget);
          // no other activities there
          for (int i = 0; i < 8; ++i) {
            final activityFinder =
                find.byKey(Key('SessionActivityTile_' + i.toString()));
            expect(activityFinder, findsNothing);
          }
        });
      });
    });
  });
  group('Appearance on Finish Tests:', () {
    testWidgets('On Finish are No Activities Visible?',
        (WidgetTester tester) async {
      FakeAsync().run((async) async {
        // set up screen
        tester.pumpWidget(testWidget);
        async.elapse(Duration(seconds: 4));
        tester.pumpAndSettle();
        async.flushMicrotasks();

        // find button
        final playButtonFinder = find.byKey(Key('playButton'));
        expect(playButtonFinder, findsOneWidget);
        // push button
        tester.tap(playButtonFinder);
        async.flushMicrotasks();
        tester.pumpAndSettle();
        async.flushMicrotasks();
        // advance the clock
        async.elapse(Duration(seconds: 102));
        tester.pumpAndSettle();
        async.flushMicrotasks();

        // no other activities there
        for (int i = 0; i < 9; ++i) {
          final activityFinder =
              find.byKey(Key('SessionActivityTile_' + i.toString()));
          expect(activityFinder, findsNothing);
        }
      });
    });
    testWidgets('On Finish is the Summary Visible?',
        (WidgetTester tester) async {
      FakeAsync().run((async) async {
        // set up screen
        tester.pumpWidget(testWidget);
        async.elapse(Duration(seconds: 4));
        tester.pumpAndSettle();
        async.flushMicrotasks();

        // find button
        final playButtonFinder = find.byKey(Key('playButton'));
        expect(playButtonFinder, findsOneWidget);
        // push button
        tester.tap(playButtonFinder);
        async.flushMicrotasks();
        tester.pumpAndSettle();
        async.flushMicrotasks();
        // advance the clock
        async.elapse(Duration(seconds: 102));
        tester.pumpAndSettle();
        async.flushMicrotasks();

        // summary visible
        final summaryFinder = find.byType(SessionSummary);
        expect(summaryFinder, findsOneWidget);
      });
    });
    group('Bottom Bar:', () {
      testWidgets('is it Visible?', (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 102));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // summary visible
          final bottomBarFinder = find.byType(BottomAppBar);
          expect(bottomBarFinder, findsOneWidget);
        });
      });

      testWidgets('Is the Home Button Visible?', (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 102));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // summary visible
          final bottomBarFinder = find.byType(BottomAppBar);
          expect(bottomBarFinder, findsOneWidget);
          final homeButtonFinder = find.descendant(
              of: bottomBarFinder, matching: find.byKey(Key("homeButton")));
          expect(homeButtonFinder, findsOneWidget);
        });
      });
      testWidgets('Is the Back Button Not Visible?',
          (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 102));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // summary visible
          final bottomBarFinder = find.byType(BottomAppBar);
          expect(bottomBarFinder, findsOneWidget);
          final backButtonFinder = find.descendant(
              of: bottomBarFinder, matching: find.byKey(Key("backButton")));
          expect(backButtonFinder, findsNothing);
        });
      });
      testWidgets('Does the Home Button Go Home?', (WidgetTester tester) async {
        FakeAsync().run((async) async {
          // set up screen
          tester.pumpWidget(testWidget);
          async.elapse(Duration(seconds: 4));
          tester.pumpAndSettle();
          async.flushMicrotasks();

          // find button
          final playButtonFinder = find.byKey(Key('playButton'));
          expect(playButtonFinder, findsOneWidget);
          // push button
          tester.tap(playButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // advance the clock
          async.elapse(Duration(seconds: 102));
          tester.pumpAndSettle();
          async.flushMicrotasks();
          // summary visible
          final bottomBarFinder = find.byType(BottomAppBar);
          expect(bottomBarFinder, findsOneWidget);
          final homeButtonFinder = find.descendant(
              of: bottomBarFinder, matching: find.byKey(Key("homeButton")));
          expect(homeButtonFinder, findsOneWidget);
          tester.tap(homeButtonFinder);
          async.flushMicrotasks();
          tester.pumpAndSettle();
          async.flushMicrotasks();
          verify(mockObserver.didPush(any, any));
          verify(mockObserver.didPush(any, any));
          expect(find.text('The First Screen'), findsNothing);
          expect(find.text('The Previous Screen'), findsNothing);
          expect(find.text('The Main Screen'), findsOneWidget);
        });
      });
    });
  });
  group('Function Tests:', () {
    testWidgets('Does currentSession.xml not exist?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        // set up screen
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle(Duration(seconds: 4));
        var dir = await (getApplicationDocumentsDirectory());
        var exists =
            await fileSystem.file(dir.path + '/currentSession.xml').exists();
        expect(exists, false);
      });
    });
    testWidgets('Does Pressing Play Speak?', (WidgetTester tester) async {
      await tester.runAsync(() async {
        // set up screen
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle();
        // find button
        final playButtonFinder = find.byKey(Key('playButton'));
        expect(playButtonFinder, findsOneWidget);
        // push button
        await tester.tap(playButtonFinder);
        await tester.pumpAndSettle();
        // advance the clock
        await untilCalled(mockSpeak.speak("Start"));

        // find the modified state
        verify(mockSpeak.speak(any)).called(1);
        expect(mockSpeakText, "Start");
      });
    });
    testWidgets('Does starting the Second Activity Speak and write to XML?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        // set up screen
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle();
        // find button
        final playButtonFinder = find.byKey(Key('playButton'));
        expect(playButtonFinder, findsOneWidget);
        // push button
        await tester.tap(playButtonFinder);
        await tester.pumpAndSettle();
        await untilCalled(mockSpeak.speak("Start Lunges"));
        verify(mockSpeak.speak("Start Lunges")).called(1);
        // find the modified state
        expect(mockSpeakText, "Start Lunges");
        // find the modified state
        var dir = await (getApplicationDocumentsDirectory());

        var file = fileSystem.file(dir.path + '/currentSession.xml');
        var exists = await file.exists();
        expect(exists, true);
        String str = await file.readAsString();
        RegExp exp = RegExp(
            r'<performance_interval number="1" >\n<duration[^>]*>\n</performance_interval>');
        expect(exp.hasMatch(str), true);
      });
    });
    testWidgets('Does starting the Third Activity Speak and write to XML?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        // set up screen
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle();
        // find button
        final playButtonFinder = find.byKey(Key('playButton'));
        expect(playButtonFinder, findsOneWidget);
        // push button
        await tester.tap(playButtonFinder);
        await tester.pumpAndSettle();
        // advance the clock
        await untilCalled(mockSpeak.speak("Start Push-ups"));

        // find the modified state
        verify(mockSpeak.speak(any)).called(3);
        expect(mockSpeakText, "Start Push-ups");
        // find the modified state
        var dir = await (getApplicationDocumentsDirectory());
        var file = fileSystem.file(dir.path + '/currentSession.xml');
        var exists = await file.exists();
        expect(exists, true);
        String str = await file.readAsString();

        RegExp exp = RegExp(
            r'<performance_interval number="2" >\n<duration[^>]*>\n</performance_interval>');
        expect(exp.hasMatch(str), true);
      });
    });
    testWidgets('Does starting the Final Activity Speak and wrtie XML?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        // set up screen
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle();
        // find button
        final playButtonFinder = find.byKey(Key('playButton'));
        expect(playButtonFinder, findsOneWidget);
        // push button
        await tester.tap(playButtonFinder);
        await tester.pumpAndSettle();
        // advance the clock
        await untilCalled(mockSpeak.speak("Start Bridge"));
        // find the modified state
        expect(mockSpeakText, "Start Bridge");
        // find the modified state
        var dir = await (getApplicationDocumentsDirectory());
        var file = fileSystem.file(dir.path + '/currentSession.xml');
        var exists = await file.exists();
        expect(exists, true);
        String str = await file.readAsString();

        RegExp exp = RegExp(
            r'<performance_interval number="8" >\n<duration[^>]*>\n</performance_interval>');
        expect(exp.hasMatch(str), true);
      });
    });
    testWidgets('Does Finish Speak and delete currentSession.xml?',
        (WidgetTester tester) async {
      await tester.runAsync(() async {
        // set up screen
        await tester.pumpWidget(testWidget);
        await tester.pumpAndSettle();
        // find button
        final playButtonFinder = find.byKey(Key('playButton'));
        expect(playButtonFinder, findsOneWidget);
        // push button
        await tester.tap(playButtonFinder);
        await tester.pumpAndSettle();
        // advance the clock
        await untilCalled(mockSpeak.speak("You have finished this session"));

        // find the modified state
        expect(mockSpeakText, "You have finished this session");
        // find the modified state
        var dir = await (getApplicationDocumentsDirectory());
        var exists =
            await fileSystem.file(dir.path + '/currentSession.xml').exists();
        expect(exists, false);
      });
    });
  });
}
