A series of tests to check what happens when the app reloads having exited for some reason.

A series of partial currentSession.xml files have been created. The bash script "run_test" takes the file name as an argument and copies the file to the emulator. It then pauses when you restart the app. On resuming is copies the currentSessionData2.xml file from the emulator to the tmp directors and cat's the output to review the app output.


File: currentSession_missingendtag.xml
Desc: An xml file where everything has been completed but the end tag of the performance is missing. 
Expected Result: The session should finish and write the session to the session data.
Status: Done

File: currentSession_missingfooter.xml
Desc: An xml file where everything has been completed but the end session data tag
Expected Result: The session should finish and write the session to the session data.
Status: Done

File: currentSession_partialheadermidactivity.xml
Desc: The description of the session is incomplete with not all of the activities defined.
Expected Result: The app should not attempt to reload the file and should delete it.
Status: Done

File: currentSession_partialheadermidendtag.xml
Desc: The description of the session is incomplete with not all of the activities defined.
Expected Result: The app should not attempt to reload the file and should delete it.
Status: Done

File: currentSession_partialheadermidopentag.xml
Desc: The description of the session is incomplete with not all of the activities defined.
Expected Result: The app should not attempt to reload the file and should delete it.
Status: Done

File: currentSession_partialheadermissingfooter.xml
Desc: The description of the session is not quite complete with a missing end tag. The correct details of the session are defined.
Expected Result: The app should load the session but not start automatically. It should wait for the user to start it.
Status: Done

File: currentSession_partialheadermissingintervals.xml
Desc: The description of the session is incomplete with not all of the activities defined.
Expected Result: The app should not attempt to reload the file and should delete it.
Status: Done

File: currentSession_partialheadernoactivity.xml
Desc: The description of the session is incomplete with not all of the activities defined.
Expected Result: The app should not attempt to reload the file and should delete it.
Status: Done

File: currentSession_partialheadernoendtag.xml
Desc: The description of the session is incomplete with not all of the activities defined.
Expected Result: The app should not attempt to reload the file and should delete it.
Status: Done

File: currentSession_stopped.xml
Desc: The session performance has stopped.
Expected Result: The session should restart or wrap up if overtime.
Status: Done

File: currentSession_unfinished.xml
Desc: The session performance has stopped.
Expected Result: The session should restart or wrap up if overtime.
Status: Done

File: currentSession_unstarted.xml
Desc: The description of the session is complete. The correct details of the session are defined. The session performance has not started.
Expected Result: The app should load the session but not start automatically. It should wait for the user to start it.
Status: Done

File: currentSession.xml
Desc: A file with all of the above details.
