import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

String sample = '''
<session_datas>
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="1m" />
</performance_interval>
<performance_interval number="2" >
<duration time="1m" />
</performance_interval>
<performance_interval number="3" >
<duration time="1m" />
</performance_interval>
<performance_interval number="4" >
<duration time="1m" />
</performance_interval>
<performance_interval number="5" >
<duration time="2m" />
</performance_interval>
<performance_interval number="6" >
<duration time="1m" />
</performance_interval>
<performance_interval number="7" >
<duration time="1m" />
</performance_interval>
<performance_interval number="8" >
<duration time="1m" />
</performance_interval>
<performance_interval number="9" >
<duration time="1m" />
</performance_interval>
</session_data>
</session_datas>
''';

main() async {
  test("Is the dirname okay?", () async {
    final directory = await getApplicationDocumentsDirectory();
    final dirName = '${directory.path}/data';

    expect("/home/danb/Documents/data", dirName);
  });
  test("Is toXML is the same?", () async {
    final directory = await getApplicationDocumentsDirectory();
    final dirName = '${directory.path}/data';
    for (int i = 0; i < 10; ++i) {
      String filename = dirName +
          '/' +
          DateTime.now().toString().replaceAll(new RegExp(r'[^0-9]'), '');
      await File(filename).writeAsString(sample);
    }
    var sessionDataList = await SessionData.readSessionDataDir();
    for (var sessionData in sessionDataList) {
      expect('<session_datas>\n' + sessionData.toXML() + '</session_datas>\n',
          sample);
    }
    // delete the test files
    await for (var file in Directory(dirName).list()) {
      file.delete();
    }
  });
}
