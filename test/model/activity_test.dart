import 'package:exercise/basic_data_classes/activity.dart';
import 'package:time/time.dart';
import 'package:xml/xml.dart';
import 'package:flutter_test/flutter_test.dart';

String sample = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="" />
</activities>
''';

main() {
  test("Do readActivities and writeActivties match the original?", () {
    expect(sample, Activity.writeActivities(Activity.readActivities(sample)));
  });
  test("Does Xml getElement not throw?", () {
    expect(() => XmlDocument.parse(sample).getElement('activities'),
        returnsNormally);
  });
  test("Can Xml getElements form a list?", () {
    expect(
        () => XmlDocument.parse(sample)
            .getElement('activities')
            .findElements('activity')
            .map<Activity>((e) => Activity.fromElement(e))
            .toList(),
        returnsNormally);
  });
  test("Does Xml getElements list have the correct values?", () {
    var activities = XmlDocument.parse(sample)
        .getElement('activities')
        .findElements('activity')
        .map<Activity>((e) => Activity.fromElement(e))
        .toList();
    expect(activities[0].activityName, 'Warm Up');
    expect(activities[0].activityDescription, 'Walk');
    expect(activities[0].activityTime, 5.minutes);
    expect(activities[1].activityName, 'Warm Down');
    expect(activities[1].activityDescription, 'Walk');
    expect(activities[1].activityTime, 5.minutes);
    expect(activities[2].activityName, 'Run');
    expect(activities[2].activityDescription, 'Run as fast as you can');
    expect(activities[2].activityTime, 30.minutes);
  });
  test("Is activity.toXML() correct?", () {
    List<Activity> activityList = Activity.readActivities(sample);
    expect(activityList[0].toXML(),
        '<activity name="Warm Up" description="Walk" duration="5m" mp3file="" />');
    expect(activityList[1].toXML(),
        '<activity name="Warm Down" description="Walk" duration="5m" mp3file="" />');
    expect(activityList[2].toXML(),
        '<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="" />');
  });
  test("Is writeActivites() correct?", () {
    List<Activity> activityList = Activity.readActivities(sample);

    expect(Activity.writeActivities(activityList), sample);
  });
}
