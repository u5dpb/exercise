// test the functions within the RunSessionTimer class

import 'package:clock/clock.dart';
import 'package:exercise/model/run_session_timer.dart';
import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/basic_data_classes/session_data_partial.dart';

import 'package:fake_async/fake_async.dart';
import 'package:flutter_test/flutter_test.dart';
//import 'package:mocking/main.dart';
import 'package:file/memory.dart';

String circuitSessionXml = '''
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="" /></interval>
</session>''';

String incompleteCircuitSessionXml = '''
<session_data timedata="2021-06-19 17:00:00.0000">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="1m" />
</performance_interval>
<performance_interval number="2" >
<duration time="1m" />
</performance_interval>
<performance_interval number="3" >
<duration time="1m" />
</performance_interval>
<performance_interval number="4" >
''';

main() async {
  RunSessionTimer timer;
  Session session;
  SessionDataPartial sdp;
  String speach;

  test("contructor : Does the new session load okay?", () {
    Session session = Session.readSessions(
        '<sessions>\n' + circuitSessionXml + '</sessions>\n')[0];
    expect(() {
      timer = RunSessionTimer(
        speachFunction: (val) {
          print(val);
        },
        storeSessionFunction: (val) {},
      );
      timer.setupSessionData(
        currentSession: session,
      );
    }, returnsNormally);
  });
  group("on loaded", () {
    setUp(() {
      session = Session.readSessions(
          '<sessions>\n' + circuitSessionXml + '</sessions>\n')[0];
      timer = RunSessionTimer(
        speachFunction: (val) {
          print(val);
        },
        storeSessionFunction: (val) {},
      );
      timer.setupSessionData(
        currentSession: session,
      );
    });
    test("Is the session state set to initial?", () {
      expect(timer.state, SessionState.initial);
    });
    test("Is the controlPanelSeconds 0?", () {
      expect(timer.controlPanelSeconds, 0);
    });
    test("Is currentActivity 0?", () {
      expect(timer.currentActivity(), 0);
    });
    test("Does setupSessionData have no session data?", () {
      String currentSessionData = timer.currentSessionData().toXML();
      String expectedSessionData = '<session_data timedata="null">\n' +
          circuitSessionXml +
          '\n</session_data>\n';
      expect(currentSessionData, expectedSessionData);
    });
  });
  group("new session started", () {
    setUp(() {
      speach = 'none';
      session = Session.readSessions(
          '<sessions>\n' + circuitSessionXml + '</sessions>\n')[0];
      // no timer = as FakeAsync does not like it
    });
    test("Is the session state set to running?", () {
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {},
        );
        timer.setupSessionData(
          currentSession: session,
        );
        timer.startSession();
        async.elapse(Duration(seconds: 2));

        expect(timer.state, SessionState.running);
      });
    });
    test("Does the speach function work?", () {
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {},
        );
        timer.setupSessionData(
          currentSession: session,
        );
        timer.startSession();
        async.elapse(Duration(seconds: 2));
        expect(speach, 'start');
      });
    });
    test("Does the session pause", () {
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {},
        );
        timer.setupSessionData(
          currentSession: session,
        );
        timer.startSession();
        async.elapse(Duration(seconds: 2));
        // test sessionPaused
        timer.togglePause();
        expect(timer.state, SessionState.paused);
      });
    });
    test("seconds", () {
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {},
        );
        timer.setupSessionData(
          currentSession: session,
        );
        timer.startSession();
        async.elapse(Duration(seconds: 5));
        timer.togglePause();
        expect(timer.controlPanelSeconds, 5);
      });
    });
    test("Does the currentActivity increment?", () {
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {},
        );
        timer.setupSessionData(
          currentSession: session,
        );
        timer.startSession();
        async.elapse(Duration(seconds: 62));

        timer.togglePause();
        expect(timer.currentActivity(), 1);
      });
    });
    test("Does currentSessionData update?", () {
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {},
        );
        timer.setupSessionData(
          currentSession: session,
        );
        timer.startSession();
        async.elapse(Duration(seconds: 62));
        // test sessionPaused
        timer.togglePause();
        String currentSessionData = timer.currentSessionData().toXML();
        int indexOfNewline = currentSessionData.indexOf('\n', 0) + 1;
        String outputSessionData = currentSessionData.substring(
            indexOfNewline, currentSessionData.length);
        String expectedSessionData = //'<session_data timedata="null">\n' +
            circuitSessionXml +
                '\n' + //'</session_data>\n' +
                '<performance_interval number="1" >\n' +
                '<duration time="1m" />\n' +
                '</performance_interval>\n' +
                '</session_data>\n'; // needed because the toXML includes this
        expect(outputSessionData, expectedSessionData);
      });
    });
    test("Does the session restart after pause?", () {
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {},
        );
        timer.setupSessionData(
          currentSession: session,
        );
        timer.startSession();
        async.elapse(Duration(seconds: 32));
        timer.togglePause(); // paused after 32 seconds
        expect(timer.currentActivity(), 0);
        async.elapse(Duration(seconds: 32));
        expect(timer.currentActivity(), 0);
        timer.togglePause(); // unpause
        async.elapse(Duration(seconds: 32));
        timer.togglePause(); // paused after 64 seconds
        expect(timer.currentActivity(), 1);
      });
    });
    test("Does stopSession work?", () {
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {},
        );
        timer.setupSessionData(
          currentSession: session,
        );
        timer.startSession();
        async.elapse(Duration(seconds: 32));
        timer.stopSession(); // stopped after 32 seconds
        expect(timer.state, SessionState.finished);
      });
    });
    test("Is finishSession called?", () {
      String finished = 'not yet';
      FakeAsync().run((async) {
        timer = RunSessionTimer(
          speachFunction: (val) {
            speach = "start";
          },
          fileSystem: MemoryFileSystem(),
          storeSessionFunction: (val) {
            finished = 'yes';
          },
        );
        timer.setupSessionData(
          currentSession: session,
        );

        timer.startSession();
        async.elapse(Duration(minutes: 11));
        expect(timer.state, SessionState.finished);
        expect(finished, 'yes');
      });
    });
  });
  group("restore session", () {
    setUp(() {
      sdp = SessionDataPartial.readIncomplete(incompleteCircuitSessionXml);
    });
    test("Does partial session contructor work?", () {
      RunSessionTimer timer = RunSessionTimer(
        speachFunction: (val) {
          print(val);
        },
        storeSessionFunction: (val) {},
      );
      var call = () => timer.restoreSessionData(
          partialSessionData: sdp,
          aClock: Clock.fixed(DateTime(2021, 06, 19, 17, 3, 30)));
      expect(call, returnsNormally);
    });

    test("Is currentActivity correct?", () {
      SessionDataPartial sdp =
          SessionDataPartial.readIncomplete(incompleteCircuitSessionXml);
      FakeAsync().run((async) {
        RunSessionTimer timer = RunSessionTimer(
          speachFunction: (val) {
            print(val);
          },
          storeSessionFunction: (val) {},
        );
        timer.restoreSessionData(
          partialSessionData: sdp,
          aClock: Clock.fixed(DateTime(2021, 06, 19, 17, 3, 30)),
        );
        expect(timer.currentActivity(), 3);
      });
    });
    test("Is seconds correct?", () {
      FakeAsync().run((async) {
        RunSessionTimer timer = RunSessionTimer(
          speachFunction: (val) {
            print(val);
          },
          storeSessionFunction: (val) {},
        );
        timer.restoreSessionData(
          partialSessionData: sdp,
          aClock: Clock.fixed(DateTime(2021, 06, 19, 17, 3, 30)),
        );
        expect(timer.seconds(), 210);
      });
    });
    test("Do the seconds increment correctly?", () {
      FakeAsync().run((async) {
        RunSessionTimer timer = RunSessionTimer(
          speachFunction: (val) {
            print(val);
          },
          storeSessionFunction: (val) {},
        );
        timer.restoreSessionData(
          partialSessionData: sdp,
          aClock: Clock.fixed(DateTime(2021, 06, 19, 17, 3, 30)),
        );
        async.elapse(Duration(seconds: 11));
        expect(timer.controlPanelSeconds, 221);
      });
    });
    test("Does the next Activity increment correctly?", () {
      FakeAsync().run((async) {
        RunSessionTimer timer = RunSessionTimer(
          speachFunction: (val) {
            print(val);
          },
          storeSessionFunction: (vaL) {},
        );
        timer.restoreSessionData(
          partialSessionData: sdp,
          aClock: Clock.fixed(DateTime(2021, 06, 19, 17, 3, 30)),
        );
        async.elapse(Duration(seconds: 60));
        expect(timer.currentActivity(), 4);
      });
    });
    test("Does finishSession work?", () {
      SessionDataPartial sdp =
          SessionDataPartial.readIncomplete(incompleteCircuitSessionXml);
      String finished = 'not yet';
      FakeAsync().run((async) {
        RunSessionTimer timer = RunSessionTimer(
          speachFunction: (val) {
            print(val);
          },
          storeSessionFunction: (val) {
            finished = 'yes';
          },
        );
        timer.restoreSessionData(
          partialSessionData: sdp,
          aClock: Clock.fixed(DateTime(2021, 06, 19, 17, 3, 30)),
        );
        async.elapse(Duration(minutes: 11));
        expect(timer.state, SessionState.finished);
        expect(finished, 'yes');
      });
    });
  });
}
