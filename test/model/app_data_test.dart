import 'package:exercise/model/failure.dart';
import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/basic_data_classes/session_data_partial.dart';
import 'package:exercise/model/app_data.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

import '../basic_data_classes/session_data_test_data.dart';

String sampleData = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="1m" />
</performance_interval>
<performance_interval number="2" >
<duration time="1m" />
</performance_interval>
<performance_interval number="3" >
<duration time="1m" />
</performance_interval>
<performance_interval number="4" >
''';

class MockFileHandlingFunctions extends Mock implements FileHandlingFunctions {}

main() {
  MockFileHandlingFunctions mockFileHandlingFunctions;
  List<SessionData> sessionDataList;

  setUp(() {
    mockFileHandlingFunctions = MockFileHandlingFunctions();
    sessionDataList = SessionDataTestData.sessionDataList;
  });
  group('File Function Tests:', () {
    test("Do the base sessions set up correctly?", () {
      when(mockFileHandlingFunctions.loadData())
          .thenAnswer((_) async => sessionDataList);
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => false);
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      expect(appData.sessionList[0].sessionName.length > 0, true);
      expect(
          appData.sessionList[0].activities[0].activityName.length > 0, true);
    });

    test("Does loadData session data set the status as loaded on completion?",
        () async {
      when(mockFileHandlingFunctions.loadData())
          .thenAnswer((_) async => sessionDataList);
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => false);
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      await Future.delayed(Duration(seconds: 2));
      expect(appData.state, NotifierState.loaded);
    });
    test("Does loadData session data set the status as loading while loading?",
        () async {
      when(mockFileHandlingFunctions.loadData()).thenAnswer((_) async {
        await Future.delayed(Duration(seconds: 2));
        return sessionDataList;
      });
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => false);
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      appData.loadSessionData();

      expect(appData.state, NotifierState.loading);
    });
    test(
        "Does loadData session data load catch a failure and set the state as error?",
        () async {
      when(mockFileHandlingFunctions.loadData())
          .thenThrow(Failure("This is a failure"));
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => false);
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );

      await Future.delayed(Duration(seconds: 2));
      expect(appData.state, NotifierState.error);
      expect(appData.failure.toString(), "This is a failure");
    });
    test(
        "Does the loadData session data set the state as partial session found?",
        () async {
      when(mockFileHandlingFunctions.loadData())
          .thenAnswer((_) async => sessionDataList);
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => true);
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      await Future.delayed(Duration(seconds: 2));
      expect(appData.state, NotifierState.partialFound);
    });
    test(
        "Does loadPartial data load correctly and set the state as partialLoaded?",
        () async {
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sampleData);
      when(mockFileHandlingFunctions.loadData())
          .thenAnswer((_) async => sessionDataList);
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => true);
      when(mockFileHandlingFunctions.loadPartialSession())
          .thenAnswer((fileName) async => sdp);
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      await Future.delayed(Duration(seconds: 2));

      appData.loadPartialData();

      expect(appData.state, NotifierState.partialLoading);
      await Future.delayed(Duration(seconds: 2));
      expect(appData.state, NotifierState.partialLoaded);
      expect(appData.sessionDataPartial, sdp);
      verify(mockFileHandlingFunctions.loadPartialSession(
          filename: "currentSession.xml"));
    });
    test(
        "Does loadPartial data catch a failure and state set the state as partialError?",
        () async {
      when(mockFileHandlingFunctions.loadData())
          .thenAnswer((_) async => sessionDataList);
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => true);
      when(mockFileHandlingFunctions.loadPartialSession())
          .thenThrow(Failure("This is a failure"));
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      await Future.delayed(Duration(seconds: 2));

      appData.loadPartialData();
      await Future.delayed(Duration(seconds: 2));
      expect(appData.state, NotifierState.partialError);
      expect(appData.failure.toString(), "This is a failure");
    });
  });
  group('SessionData Functions Tests:', () {
    test("Does addSessionData add at the start of the list?", () async {
      when(mockFileHandlingFunctions.loadData())
          .thenAnswer((_) async => sessionDataList);
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => false);
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      await Future.delayed(Duration(seconds: 2));

      SessionData sessionData_0 =
          SessionData(sessionInstance: appData.sessionList[0]);
      SessionData sessionData_1 =
          SessionData(sessionInstance: appData.sessionList[1]);
      appData.addSessionData(sessionData_0);
      appData.addSessionData(sessionData_1);
      expect(sessionData_0.sessionName != sessionData_1.sessionName, true);
      expect(appData.sessionDataList[0].sessionName, sessionData_1.sessionName);
      expect(appData.sessionDataList[1].sessionName, sessionData_0.sessionName);
    });
    test("Does addSessionDatas add correctly?", () async {
      List<SessionData> emptySessionDataList = [];

      when(mockFileHandlingFunctions.loadData())
          .thenAnswer((_) async => emptySessionDataList);
      when(mockFileHandlingFunctions.partialSessionExists())
          .thenAnswer((_) async => false);
      AppData appData = AppData(
        fileHandlingFunctions: mockFileHandlingFunctions,
      );
      await Future.delayed(Duration(seconds: 2));
      SessionData sessionData_0 =
          SessionData(sessionInstance: appData.sessionList[0]);
      SessionData sessionData_1 =
          SessionData(sessionInstance: appData.sessionList[1]);
      appData.addSessionDatas([sessionData_0, sessionData_1]);
      expect(sessionData_0.sessionName != sessionData_1.sessionName, true);
      expect(appData.sessionDataList[0].sessionName, sessionData_0.sessionName);
      expect(appData.sessionDataList[1].sessionName, sessionData_1.sessionName);
    });
  });
}
