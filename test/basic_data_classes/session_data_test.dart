import 'package:exercise/basic_data_classes/session_data.dart';

import 'package:flutter_test/flutter_test.dart';

String sample = '''
<session_datas>
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="1m" />
</performance_interval>
<performance_interval number="2" >
<duration time="1m" />
</performance_interval>
<performance_interval number="3" >
<duration time="1m" />
</performance_interval>
<performance_interval number="4" >
<duration time="1m" />
</performance_interval>
<performance_interval number="5" >
<duration time="2m" />
</performance_interval>
<performance_interval number="6" >
<duration time="1m" />
</performance_interval>
<performance_interval number="7" >
<duration time="1m" />
</performance_interval>
<performance_interval number="8" >
<duration time="1m" />
</performance_interval>
<performance_interval number="9" >
<duration time="1m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-19 17:42:42.129798">
<session name="Short Run" description="30 minute run" numIntervals="3" >
<interval number="1" ><activity name="Warm up" description="Walk" duration="5m" mp3file="" /></interval>
<interval number="2" ><activity name="Run" description="Run" duration="30m" mp3file="" /></interval>
<interval number="3" ><activity name="Warm down" description="Walk" duration="5m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="5m" />
</performance_interval>
<performance_interval number="2" >
<duration time="30m" />
</performance_interval>
<performance_interval number="3" >
<duration time="5m" />
</performance_interval>
</session_data>
</session_datas>
''';

main() {
  test("Does applying readSessionData and writeSessionData match the original?",
      () {
    expect(sample,
        SessionData.writeSessionDatas(SessionData.readSessionDatas(sample)));
  });
  test("Do the partial write XML functions add up to a complete file?", () {
    List<SessionData> sessionDatas = SessionData.readSessionDatas(sample);
    for (SessionData sessionData in sessionDatas) {
      String output = sessionData.headerXML();
      for (int i = 0; i < sessionData.activities.length; ++i)
        output += sessionData.recordActivityCompletedXML(index: i);
      output += sessionData.footerXML();
      expect(sessionData.toXML(), output);
    }
  });
}
