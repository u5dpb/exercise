import 'package:exercise/basic_data_classes/session.dart';
import 'package:flutter_test/flutter_test.dart';

String sample = '''
<sessions>
<session name="Run" description="Run for 20 minutes" numIntervals="3" >
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="" /></interval>
<interval number="2" ><activity name="Run" description="Run as fast as you can" duration="30m" mp3file="" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="" /></interval>
</session>
<session name="Run" description="Run for 60 minutes" numIntervals="3" >
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="" /></interval>
<interval number="2" ><activity name="Run" description="Walk" duration="1h" mp3file="" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="" /></interval>
</session>
</sessions>
''';

String sample2 = '''
<sessions>
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="" /></interval>
<session name="Run" description="Run for 20 minutes" numIntervals="3" >
<interval number="2" ><activity name="Run" description="Walk" duration="30m" mp3file="" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="" /></interval>
</session>
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="" /></interval>
<interval number="2" ><activity name="Run" description="Walk" duration="1h" mp3file="" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="" /></interval>
<session name="Run" description="Run for 60 minutes" numIntervals="3" >
</session>
</sessions>
''';

String sample3 = '''
<sessions>
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="" /></interval>
<session name="Run" description="Run for 20 minutes" numIntervals="3" >
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="" /></interval>
<interval number="2" ><activity name="Run" description="Walk" duration="30m" mp3file="" /></interval>
</session>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="" /></interval>
<interval number="2" ><activity name="Run" description="Walk" duration="1h" mp3file="" /></interval>
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="" /></interval>
<session name="Run" description="Run for 60 minutes" numIntervals="3" >
</session>
</sessions>
''';

main() {
  test("Does applying readSessions and writeSessions match the original?", () {
    expect(sample, Session.writeSessions(Session.readSessions(sample)));
  });

  test("Does readSessions create the correct session info?", () {
    Session session = Session.readSessions(sample)[0];
    expect(session.sessionName, 'Run');
    expect(session.sessionDescription, 'Run for 20 minutes');
  });
  test("Does readSessions create the correct activity info?", () {
    Session session = Session.readSessions(sample)[0];
    expect(session.activities[0].activityName, 'Warm Up');
    expect(session.activities[1].activityName, 'Run');
    expect(session.activities[2].activityName, 'Warm Down');
  });

  test("Is session.toXML correct?", () {
    List<Session> sessionList = Session.readSessions(sample);
    expect(sessionList[0].toXML(),
        '''<session name="Run" description="Run for 20 minutes" numIntervals="3" >
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="" /></interval>
<interval number="2" ><activity name="Run" description="Run as fast as you can" duration="30m" mp3file="" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="" /></interval>
</session>
''');
  });
}
