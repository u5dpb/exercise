import 'package:exercise/basic_data_classes/session_data_partial.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group("Error free sessionData", () {
    test("Does an error free seesionData load okay?", () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="6s" />
</performance_interval>
<performance_interval number="2" >
<duration time="6s" />
</performance_interval>
<performance_interval number="3" >
<duration time="6s" />
</performance_interval>
<performance_interval number="4" >
<duration time="6s" />
</performance_interval>
<performance_interval number="5" >
<duration time="12s" />
</performance_interval>
<performance_interval number="6" >
<duration time="6s" />
</performance_interval>
<performance_interval number="7" >
<duration time="6s" />
</performance_interval>
<performance_interval number="8" >
<duration time="6s" />
</performance_interval>
<performance_interval number="9" >
<duration time="6s" />
</performance_interval>
</session_data>
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, false);
      expect(sdp.incompletePerformance, false);
    });
    test("Does a shorter error free sessionData load okay?", () {
      String sample = '''
<session_data timedata="2021-06-19 17:42:42.129798">
<session name="Short Run" description="30 minute run" numIntervals="3" >
<interval number="1" ><activity name="Warm up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Run" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="30s" />
</performance_interval>
<performance_interval number="2" >
<duration time="3m" />
</performance_interval>
<performance_interval number="3" >
<duration time="30s" />
</performance_interval>
</session_data>
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, false);
      expect(sdp.incompletePerformance, false);
    });
  });
  group("sessionData performance section incomplete", () {
    test("Does an unfinished sessionData load and register is incomplete?", () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="6s" />
</performance_interval>
<performance_interval number="2" >
<duration time="6s" />
</performance_interval>
<performance_interval number="3" >
<duration time="6s" />
</performance_interval>
<performance_interval number="4" >
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, false);
      expect(sdp.incompletePerformance, true);
    });
    test(
        "Does a session that has stopped load reload and register as incomplete?",
        () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="6s" />
</performance_interval>
<performance_interval number="2" >
<duration time="6s" />
</performance_interval>
<performance_interval number="3" >
<duration time="6s" />
</performance_interval>
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, false);
      expect(sdp.incompletePerformance, true);
    });

    test("Does an unstarted session load and register as incomplete?", () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, false);
      expect(sdp.incompletePerformance, true);
    });
  });
  group("Incomplete session information", () {
    test('''Does a sessionData missing only the end 
      tag load as a completed sessionData?''', () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="6s" />
</performance_interval>
<performance_interval number="2" >
<duration time="6s" />
</performance_interval>
<performance_interval number="3" >
<duration time="6s" />
</performance_interval>
<performance_interval number="4" >
<duration time="6s" />
</performance_interval>
<performance_interval number="5" >
<duration time="12s" />
</performance_interval>
<performance_interval number="6" >
<duration time="6s" />
</performance_interval>
<performance_interval number="7" >
<duration time="6s" />
</performance_interval>
<performance_interval number="8" >
<duration time="6s" />
</performance_interval>
<performance_interval number="9" >
<duration time="6s" />
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, false);
      expect(sdp.incompletePerformance, false);
    });

    test(
        "Does a sessionData missing only the footer load as a completed session?",
        () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="6s" />
</performance_interval>
<performance_interval number="2" >
<duration time="6s" />
</performance_interval>
<performance_interval number="3" >
<duration time="6s" />
</performance_interval>
<performance_interval number="4" >
<duration time="6s" />
</performance_interval>
<performance_interval number="5" >
<duration time="12s" />
</performance_interval>
<performance_interval number="6" >
<duration time="6s" />
</performance_interval>
<performance_interval number="7" >
<duration time="6s" />
</performance_interval>
<performance_interval number="8" >
<duration time="6s" />
</performance_interval>
<performance_interval number="9" >
<duration time="6s" />
</performance_interval>
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, false);
      expect(sdp.incompletePerformance, false);
    });

    test('''Does a sessionData missing only the footer from the 
      session section load as an incomplete session?''', () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, false);
      expect(sdp.incompletePerformance, true);
    });
    test('''Does a sessionData that has incomplete session 
      information register as an incomplete session?''', () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, true);
      expect(sdp.incompletePerformance, true);
    });
    test('''Does a sessionData with session 
  information stopping mid-interval end tag register as incomplete?''', () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></inter
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, true);
      expect(sdp.incompletePerformance, true);
    });
    test('''Does a sessionData with session information with no 
  interval end tag register as incomplete?''', () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" />
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, true);
      expect(sdp.incompletePerformance, true);
    });
    test('''Does a sessionData with session information 
  stopping mid-activity register as incomplete?''', () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" descrip
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, true);
      expect(sdp.incompletePerformance, true);
    });

    test('''Does a sessionData with session information 
  stopping pre-activity register as incomplete?''', () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" >
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, true);
      expect(sdp.incompletePerformance, true);
    });
    test('''Does a sessionData with session information 
  stopping mid-interval opening tag register as incomplete?''', () {
      String sample = '''
<session_data timedata="2021-06-19 17:37:30.050583">
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval num
''';
      SessionDataPartial sdp = SessionDataPartial.readIncomplete(sample);
      expect(sdp.incompleteSession, true);
      expect(sdp.incompletePerformance, true);
    });
  });
}
