import 'package:exercise/basic_data_classes/session_data.dart';

class SessionDataTestData {
  static SessionData longSessionData =
      SessionData.readSessionDatas(longSessionDataString)[0];
  static List<SessionData> sessionDataList =
      SessionData.readSessionDatas(sessionDatasString);
  static String sessionDatasString = '''
<session_datas>
<session_data timedata="2021-06-19 17:00:00">
<session name="Session 1" description="This is session 1" numIntervals="3" >
<interval number="1" ><activity name="Activity 1" description="" duration="1m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 2" description="" duration="2m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 3" description="" duration="3m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="1m" />
</performance_interval>
<performance_interval number="2" >
<duration time="2m" />
</performance_interval>
<performance_interval number="3" >
<duration time="3m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-20 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-21 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-22 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-23 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-24 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-25 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-26 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-27 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-28 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-29 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
<session_data timedata="2021-06-30 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-07-01 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
<session_data timedata="2021-07-02 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-07-03 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
<session_data timedata="2021-07-04 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-07-05 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
<session_data timedata="2021-07-06 17:00:00">
<session name="Session 2" description="This is session 2" numIntervals="3" >
<interval number="1" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="4m" />
</performance_interval>
<performance_interval number="2" >
<duration time="5m" />
</performance_interval>
<performance_interval number="3" >
<duration time="6m" />
</performance_interval>
</session_data>
<session_data timedata="2021-07-07 17:00:00">
<session name="Session 3" description="This is session 3" numIntervals="3" >
<interval number="1" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="7m" />
</performance_interval>
<performance_interval number="2" >
<duration time="8m" />
</performance_interval>
<performance_interval number="3" >
<duration time="9m" />
</performance_interval>
</session_data>
</session_datas>
''';
  static String longSessionDataString = '''
<session_datas>
<session_data timedata="2021-06-19 17:00:00">
<session name="Session 1" description="This is session 1" numIntervals="9" >
<interval number="1" ><activity name="Activity 1" description="" duration="1m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 2" description="" duration="2m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 3" description="" duration="3m" mp3file="" /></interval>
<interval number="4" ><activity name="Activity 4" description="" duration="4m" mp3file="" /></interval>
<interval number="5" ><activity name="Activity 5" description="" duration="5m" mp3file="" /></interval>
<interval number="6" ><activity name="Activity 6" description="" duration="6m" mp3file="" /></interval>
<interval number="7" ><activity name="Activity 7" description="" duration="7m" mp3file="" /></interval>
<interval number="8" ><activity name="Activity 8" description="" duration="8m" mp3file="" /></interval>
<interval number="9" ><activity name="Activity 9" description="" duration="9m" mp3file="" /></interval>
</session>
<performance_interval number="1" >
<duration time="1m" />
</performance_interval>
<performance_interval number="2" >
<duration time="2m" />
</performance_interval>
<performance_interval number="3" >
<duration time="3m" />
</performance_interval>
<performance_interval number="4" >
<duration time="4m" />
</performance_interval>
<performance_interval number="5" >
<duration time="5m" />
</performance_interval>
<performance_interval number="6" >
<duration time="6m" />
</performance_interval>
<performance_interval number="7" >
<duration time="7m" />
</performance_interval>
<performance_interval number="8" >
<duration time="8m" />
</performance_interval>
<performance_interval number="9" >
<duration time="9m" />
</performance_interval>
</session_data>
</session_datas>
''';
}
