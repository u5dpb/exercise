import 'package:exercise/constant.dart';
import 'package:exercise/widgets/title_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group('Appearance Tests:', () {
    testWidgets('Text Test', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(appBar: TitleAppBar(title: 'This is a title')),
        ),
      );
      await tester.pumpAndSettle();
      final titleFinder = find.text('This is a title');
      expect(titleFinder, findsOneWidget);
    });
    testWidgets('Text Colour Test', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(appBar: TitleAppBar(title: 'This is a title')),
        ),
      );
      await tester.pumpAndSettle();
      final textFinder = find.byType(Text);
      final textWidget = tester.firstWidget(textFinder) as Text;
      expect(textWidget.style.color, Colors.white);
    });
    testWidgets('Background Test', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(appBar: TitleAppBar(title: 'This is a title')),
        ),
      );
      await tester.pumpAndSettle();
      final appBarFinder = find.byType(AppBar);
      final appBarWidget = tester.firstWidget(appBarFinder) as AppBar;
      expect(appBarWidget.backgroundColor, kPrimaryColor);
    });
    // testWidgets('Logo Test', (WidgetTester tester) async {
    //   await tester.pumpWidget(
    //     MaterialApp(
    //       home: Scaffold(appBar: TitleAppBar(title: 'This is a title')),
    //     ),
    //   );
    //   await tester.pumpAndSettle();
    //   final appBarFinder = find.byType(AppBar);
    //   expect(appBarFinder, findsOneWidget);

    //   final appBarWidget = tester.firstWidget(appBarFinder) as AppBar;
    //   final imageWidget = (appBarWidget.leading as Image).image as AssetImage;
    //   expect(imageWidget.assetName, "assets/logo_2.png");
    // });
  });
}
