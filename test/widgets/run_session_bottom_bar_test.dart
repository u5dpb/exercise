import 'package:exercise/constant.dart';
import 'package:exercise/screens/main_screen.dart';
import 'package:exercise/widgets/run_session_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

main() {
// appearance
// * before start
  group('Appearance Tests:', () {
    testWidgets('Before the start Test', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Container(),
            bottomNavigationBar: RunSessionBottomBar(
              sessionFinished: false,
              sessionInitial: true,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final backButton = find.byKey(Key('backButton'));
      final homeButton = find.byKey(Key('homeButton'));
      expect(backButton, findsOneWidget);
      expect(homeButton, findsNothing);
    });
// * running
    testWidgets('When just started Test', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Container(),
            bottomNavigationBar: RunSessionBottomBar(
              sessionInitial: false,
              sessionFinished: false,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final backButton = find.byKey(Key('backButton'));
      final homeButton = find.byKey(Key('homeButton'));
      expect(backButton, findsNothing);
      expect(homeButton, findsNothing);
    });

// * finished
    testWidgets('When finished Test', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Container(),
            bottomNavigationBar: RunSessionBottomBar(
              sessionFinished: true,
              sessionInitial: false,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final backButton = find.byKey(Key('backButton'));
      final homeButton = find.byKey(Key('homeButton'));
      expect(backButton, findsNothing);
      expect(homeButton, findsOneWidget);
    });
// * color
    testWidgets('Color Test', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: Container(),
            bottomNavigationBar: RunSessionBottomBar(
              sessionInitial: false,
              sessionFinished: true,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final bottomAppBar =
          tester.firstWidget(find.byType(BottomAppBar)) as BottomAppBar;
      expect(bottomAppBar.color, kPrimaryColor);
    });
// * padding
    testWidgets('Padding Test', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            bottomNavigationBar: RunSessionBottomBar(
              sessionInitial: false,
              sessionFinished: true,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.byKey(Key('runSessionBottomBarPadding')), findsOneWidget);
      final bottomPadding =
          tester.firstWidget(find.byKey(Key('runSessionBottomBarPadding')))
              as Padding;
      expect(bottomPadding.padding, EdgeInsets.all(5));
    });
  });
// function
// * back pressed
  group('Function Tests:', () {
    testWidgets('Back Button Test', (WidgetTester tester) async {
      final mockObserver = MockNavigatorObserver();
      await tester.pumpWidget(
        MaterialApp(
          initialRoute: 'main',
          routes: {
            'main': (context) => Scaffold(
                  body: Container(),
                  bottomNavigationBar: RunSessionBottomBar(
                    sessionFinished: false,
                    sessionInitial: true,
                  ),
                ),
            MainScreen.id: (context) => Container(child: Text('A Main Screen')),
            '/': (context) => Container(child: Text('The Previous Screen'))
          },
          navigatorObservers: [mockObserver],
        ),
      );
      await tester.pumpAndSettle();
      expect(find.byKey(Key('backButton')), findsOneWidget);
      await tester.tap(find.byKey(Key('backButton')));
      await tester.pumpAndSettle();
      // Route r = verify(mockObserver.didPop(captureAny, any)).captured.single;
      // expect(r.settings.name, '/');
      verify(mockObserver.didPop(any, any));
      await tester.pumpAndSettle();
      // It appears that there is no previous screen to pop back to
      //    so expecting any text is superfluous
      // expect(find.text('A Main Screen'), findsNothing);
      // expect(find.text('The Previous Screen'), findsOneWidget);
    });
// * home pressed
    testWidgets('Home Button Test', (WidgetTester tester) async {
      final mockObserver = MockNavigatorObserver();
      await tester.pumpWidget(
        MaterialApp(
          initialRoute: 'main',
          routes: {
            'main': (context) => Scaffold(
                  body: Container(child: Text('The First Screen')),
                  bottomNavigationBar: RunSessionBottomBar(
                    sessionInitial: false,
                    sessionFinished: true,
                  ),
                ),
            MainScreen.id: (context) =>
                Container(child: Text('The Main Screen')),
            '/': (context) => Container(child: Text('The Previous Screen'))
          },
          navigatorObservers: [mockObserver],
        ),
      );
      await tester.pumpAndSettle();
      expect(find.byKey(Key('homeButton')), findsOneWidget);
      await tester.tap(find.byKey(Key('homeButton')));
      await tester.pumpAndSettle();
      //Route r = verify(mockObserver.didPop(captureAny, any)).captured.single;
      //print(r.settings.name);
      verify(mockObserver.didPush(any, any));
      expect(find.text('The First Screen'), findsNothing);
      expect(find.text('The Previous Screen'), findsNothing);
      expect(find.text('The Main Screen'), findsOneWidget);
    });
  });
}
