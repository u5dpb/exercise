import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/widgets/list_tiles/session_data_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  // test SessionDataTile
  // * subtitle padding
  // * tile edge insets
  // * title text
  // * tile text
  // * onTap works
  testWidgets('SessionDataTile Subtitle Padding Test',
      (WidgetTester tester) async {
    String sessionDataString = '''
<session_datas>
<session_data timedata="2021-06-19 17:42:42.129798">
<session name="Short Run" description="30 minute run" numIntervals="3" >
<interval number="1" ><activity name="Warm up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Run" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="30s" />
</performance_interval>
<performance_interval number="2" >
<duration time="3m" />
</performance_interval>
<performance_interval number="3" >
<duration time="30s" />
</performance_interval>
</session_data>
</session_datas>
''';
    SessionData sessionData =
        SessionData.readSessionDatas(sessionDataString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionDataTile(sessionData: sessionData, onTap: () {}),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final widgetMargin =
        ((tester.firstWidget(find.byType(ListTile)) as ListTile).subtitle
                as Padding)
            .padding as EdgeInsets;
    expect(widgetMargin, EdgeInsets.only(top: 5));
  });
  testWidgets('SessionDataTile Tile EdgeInsets Test',
      (WidgetTester tester) async {
    String sessionDataString = '''
<session_datas>
<session_data timedata="2021-06-19 17:42:42.129798">
<session name="Short Run" description="30 minute run" numIntervals="3" >
<interval number="1" ><activity name="Warm up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Run" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="30s" />
</performance_interval>
<performance_interval number="2" >
<duration time="3m" />
</performance_interval>
<performance_interval number="3" >
<duration time="30s" />
</performance_interval>
</session_data>
</session_datas>
''';
    SessionData sessionData =
        SessionData.readSessionDatas(sessionDataString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionDataTile(sessionData: sessionData, onTap: () {}),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final widgetMargin = (tester.firstWidget(find.byType(ListTile)) as ListTile)
        .contentPadding as EdgeInsets;
    expect(widgetMargin, EdgeInsets.all(10));
  });
  testWidgets('SessionDataTile Title Text Test', (WidgetTester tester) async {
    String sessionDataString = '''
<session_datas>
<session_data timedata="2021-06-19 17:42:42.129798">
<session name="Short Run" description="30 minute run" numIntervals="3" >
<interval number="1" ><activity name="Warm up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Run" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="30s" />
</performance_interval>
<performance_interval number="2" >
<duration time="3m" />
</performance_interval>
<performance_interval number="3" >
<duration time="30s" />
</performance_interval>
</session_data>
</session_datas>
''';
    SessionData sessionData =
        SessionData.readSessionDatas(sessionDataString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionDataTile(sessionData: sessionData, onTap: () {}),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('19/06/2021  Short Run');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionDataTile Tile Text Test', (WidgetTester tester) async {
    String sessionDataString = '''
<session_datas>
<session_data timedata="2021-06-19 17:42:42.129798">
<session name="Short Run" description="30 minute run" numIntervals="3" >
<interval number="1" ><activity name="Warm up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Run" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="30s" />
</performance_interval>
<performance_interval number="2" >
<duration time="3m" />
</performance_interval>
<performance_interval number="3" >
<duration time="30s" />
</performance_interval>
</session_data>
</session_datas>
''';
    SessionData sessionData =
        SessionData.readSessionDatas(sessionDataString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionDataTile(sessionData: sessionData, onTap: () {}),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('17:42   Total time: 40 minutes');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionDataTile onTap Works Test', (WidgetTester tester) async {
    bool onTapWorks = false;
    String sessionDataString = '''
<session_datas>
<session_data timedata="2021-06-19 17:42:42.129798">
<session name="Short Run" description="30 minute run" numIntervals="3" >
<interval number="1" ><activity name="Warm up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Run" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
<performance_interval number="1" >
<duration time="30s" />
</performance_interval>
<performance_interval number="2" >
<duration time="3m" />
</performance_interval>
<performance_interval number="3" >
<duration time="30s" />
</performance_interval>
</session_data>
</session_datas>
''';
    SessionData sessionData =
        SessionData.readSessionDatas(sessionDataString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionDataTile(
              sessionData: sessionData,
              onTap: () {
                onTapWorks = true;
              }),
        ),
      ),
    );
    await tester.pumpAndSettle();
    await tester.tap(find.byType(SessionDataTile));
    expect(onTapWorks, true);
  });
}
