import 'package:exercise/basic_data_classes/activity.dart';
import 'package:exercise/widgets/list_tiles/activity_list_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time/time.dart';

main() {
  // Test for
  // * title text
  // * show_description true shows text
  // * show_description false shows no text
  // * duration given shows text
  // * duration null shows no text
  // * endtime shows text
  // * endtime null shows no text
  // * subtitle padding
  // * list tile padding
  testWidgets('ActivityListTile Title Text Test', (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(activity: activity),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Run');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('ActivityListTile Show Description true Text Test',
      (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(
            activity: activity,
            showDescription: true,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Run as fast as you can');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('ActivityListTile Show Description false Text Test',
      (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(
            activity: activity,
            showDescription: false,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Run as fast as you can');
    expect(textFinder, findsNothing);
  });
  testWidgets('ActivityListTile non-null Duration Text Test',
      (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(
            activity: activity,
            duration: 30.minutes,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Duration: 30 minutes');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('ActivityListTile null Duration Text Test',
      (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(
            activity: activity,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Duration: 30 minutes');
    expect(textFinder, findsNothing);
  });
  testWidgets('ActivityListTile endtime shows Text Test',
      (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(
            activity: activity,
            endTime: 35.minutes,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Finish: 35m');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('ActivityListTile endtime doesnt show Text Test',
      (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(
            activity: activity,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Finish: 35m');
    expect(textFinder, findsNothing);
  });

  testWidgets('ActivityListTile Subtitle Padding Test',
      (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(activity: activity),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final widgetMargin =
        ((tester.firstWidget(find.byType(ListTile)) as ListTile).subtitle
                as Padding)
            .padding as EdgeInsets;
    expect(widgetMargin, EdgeInsets.only(top: 5));
  });
  testWidgets('ActivityListTile ListTile Padding Test',
      (WidgetTester tester) async {
    String activityString = '''
<activities>
<activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" />
<activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" />
<activity name="Run" description="Run as fast as you can" duration="30m" mp3file="assets/mp3/run.mp3" />
</activities>
''';
    Activity activity = Activity.readActivities(activityString)[2];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ActivityListTile(activity: activity),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final widgetMargin = (tester.firstWidget(find.byType(ListTile)) as ListTile)
        .contentPadding as EdgeInsets;
    expect(widgetMargin, EdgeInsets.all(10));
  });
}
