import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/widgets/list_tiles/session_activity_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  // Test:
  // * index title
  // * index description
  // * index duration
  // * index endTime
  // * index first
  // * index second
  // * index last

  testWidgets('SessionActivityTile Title Text Test',
      (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionActivityTile(
            index: 0,
            currentSession: session,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Squats');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionActivityTile Description Text Test',
      (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="Do some squats" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="Do some Lunges" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="Do some push-ups" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="Do some dips" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="Do some burpees" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="Do some push ups" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="Do the bridge" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionActivityTile(
            index: 0,
            currentSession: session,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Do some squats');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionActivityTile Duration Text Test',
      (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="Do some squats" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="Do some Lunges" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="Do some push-ups" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="Do some dips" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="Do some burpees" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="Do some push ups" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="Do the bridge" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionActivityTile(
            index: 0,
            currentSession: session,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Duration: 1 minute');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionActivityTile Endtime Text Test',
      (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="Do some squats" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="Do some Lunges" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="Do some push-ups" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="Do some dips" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="Do some burpees" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="Do some push ups" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="Do the bridge" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionActivityTile(
            index: 1,
            currentSession: session,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Finish: 2m');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionActivityTile First Title Text Test',
      (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionActivityTile(
            index: 0,
            currentSession: session,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Squats');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionActivityTile Second Title Text Test',
      (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionActivityTile(
            index: 1,
            currentSession: session,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Lunges');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionActivityTile Last Title Text Test',
      (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Circuit Training 1" description="Daily Circuits" numIntervals="9" >
<interval number="1" ><activity name="Squats" description="" duration="1m" mp3file="assets/mp3/squats.mp3" /></interval>
<interval number="2" ><activity name="Lunges" description="" duration="1m" mp3file="assets/mp3/lunges.mp3" /></interval>
<interval number="3" ><activity name="Push-ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="4" ><activity name="Dead Lift" description="One Leg Dead Lifts" duration="1m" mp3file="assets/mp3/deadlift.mp3" /></interval>
<interval number="5" ><activity name="Knee Lifts" description="Wall Sit Knee Lifts" duration="2m" mp3file="assets/mp3/kneelifts.mp3" /></interval>
<interval number="6" ><activity name="Dips" description="" duration="1m" mp3file="assets/mp3/dips.mp3" /></interval>
<interval number="7" ><activity name="Burpees" description="" duration="1m" mp3file="assets/mp3/burpees.mp3" /></interval>
<interval number="8" ><activity name="Push Ups" description="" duration="1m" mp3file="assets/mp3/pushups.mp3" /></interval>
<interval number="9" ><activity name="Bridge" description="" duration="1m" mp3file="assets/mp3/bridge.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionActivityTile(
            index: 8,
            currentSession: session,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Bridge');
    expect(textFinder, findsOneWidget);
  });
}
