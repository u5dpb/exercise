import 'package:exercise/constant.dart';
import 'package:exercise/widgets/list_tiles/list_card.dart';
// import 'package:exercise/model/session.dart';
// import 'package:exercise/model/session_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  // test ListCard
  // * colours
  // * border
  // * edge insets
  // * ListTile displayed
  testWidgets('ListCard Colours Test', (WidgetTester tester) async {
    ListTile tile = ListTile(title: Text('This is a title'));
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ListCard(
            tile: tile,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final widgetContainer =
        tester.firstWidget(find.byType(Container)) as Container;
    expect(widgetContainer.color, Colors.white);
    final widgetCard = tester.firstWidget(find.byType(Card)) as Card;
    expect(widgetCard.color, kLightPrimaryColor);
  });

  testWidgets('ListCard Border Test', (WidgetTester tester) async {
    ListTile tile = ListTile(title: Text('This is a title'));
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ListCard(
            tile: tile,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final widgetBorder = (tester.firstWidget(find.byType(Card)) as Card).shape
        as RoundedRectangleBorder;
    expect(widgetBorder.borderRadius, BorderRadius.circular(20.0));
    expect(widgetBorder.side, BorderSide.none);
  });
  testWidgets('ListCard EdgeInsets Test', (WidgetTester tester) async {
    ListTile tile = ListTile(title: Text('This is a title'));
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ListCard(
            tile: tile,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final widgetMargin =
        (tester.firstWidget(find.byType(Card)) as Card).margin as EdgeInsets;
    expect(widgetMargin, EdgeInsets.symmetric(horizontal: 15, vertical: 10));
  });
  testWidgets('ListCard ListTile Displayed Test', (WidgetTester tester) async {
    ListTile tile = ListTile(title: Text('This is a title'));
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: ListCard(
            tile: tile,
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('This is a title');
    expect(textFinder, findsOneWidget);
  });
}
