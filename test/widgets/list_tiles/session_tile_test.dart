import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/widgets/list_tiles/session_tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  // test SessionTile
  // * tile edge insets
  // * session name text
  // * session description text
  // * onTap works

  testWidgets('SessionTile EdgeInsets Test', (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Run" description="Run for 20 minutes" numIntervals="3" >
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Walk" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionTile(session: session, onTap: () {}),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final widgetMargin = (tester.firstWidget(find.byType(ListTile)) as ListTile)
        .contentPadding as EdgeInsets;
    expect(widgetMargin, EdgeInsets.all(10));
  });
  testWidgets('SessionTile Session Name Test', (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Run" description="Run for 20 minutes" numIntervals="3" >
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Walk" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionTile(session: session, onTap: () {}),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Run');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionTile Session Description Test',
      (WidgetTester tester) async {
    String sessionString = '''
<sessions>
<session name="Run" description="Run for 20 minutes" numIntervals="3" >
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Walk" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionTile(session: session, onTap: () {}),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final textFinder = find.text('Run for 20 minutes');
    expect(textFinder, findsOneWidget);
  });
  testWidgets('SessionTile onTap works', (WidgetTester tester) async {
    bool onTapWorks = false;
    String sessionString = '''
<sessions>
<session name="Run" description="Run for 20 minutes" numIntervals="3" >
<interval number="1" ><activity name="Warm Up" description="Walk" duration="5m" mp3file="assets/mp3/warmup.mp3" /></interval>
<interval number="2" ><activity name="Run" description="Walk" duration="30m" mp3file="assets/mp3/run.mp3" /></interval>
<interval number="3" ><activity name="Warm Down" description="Walk" duration="5m" mp3file="assets/mp3/warmdown.mp3" /></interval>
</session>
</sessions>
''';
    Session session = Session.readSessions(sessionString)[0];
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: SessionTile(
              session: session,
              onTap: () {
                onTapWorks = true;
              }),
        ),
      ),
    );
    await tester.pumpAndSettle();
    await tester.tap(find.byType(SessionTile));
    expect(onTapWorks, true);
  });
}
