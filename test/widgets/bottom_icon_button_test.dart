import 'package:exercise/constant.dart';
import 'package:exercise/widgets/bottom_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Icon and onPressed Test', (WidgetTester tester) async {
    bool pressed = false;
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: BottomIconButton(
            buttonIcon: Icons.home,
            onPressed: () {
              pressed = true;
            },
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final homeIcon = find.byType(Icon);
    expect(homeIcon, findsOneWidget);
    await tester.tap(homeIcon);
    await tester.pumpAndSettle();
    expect(pressed, true);
  });
  testWidgets('Colours Test', (WidgetTester tester) async {
    await tester.pumpWidget(
      MaterialApp(
        home: Scaffold(
          body: BottomIconButton(
            buttonIcon: Icons.home,
            onPressed: () {},
          ),
        ),
      ),
    );
    await tester.pumpAndSettle();
    final homeIconButton = find.byType(IconButton);
    final widgetIconButton = tester.widget<IconButton>(homeIconButton);
    expect(widgetIconButton.color, kPrimaryColor);

    expect(
        ((tester.firstWidget(find.byType(Ink)) as Ink).decoration
                as ShapeDecoration)
            .color,
        Colors.white);
  });
}
