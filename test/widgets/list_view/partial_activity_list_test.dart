import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/widgets/list_view/partial_activity_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';
import '../../basic_data_classes/session_data_test_data.dart';

String sessionString = '''
<sessions>
<session name="Run" description="Run for 20 minutes" numIntervals="20" >
<interval number="1" ><activity name="Activity 1" description="Do something 1" duration="1m" mp3file="" /></interval>
<interval number="2" ><activity name="Activity 2" description="Do something 2" duration="1m" mp3file="" /></interval>
<interval number="3" ><activity name="Activity 3" description="Do something 3" duration="1m" mp3file="" /></interval>
<interval number="4" ><activity name="Activity 4" description="Do something 4" duration="1m" mp3file="" /></interval>
<interval number="5" ><activity name="Activity 5" description="Do something 5" duration="1m" mp3file="" /></interval>
<interval number="6" ><activity name="Activity 6" description="Do something 6" duration="1m" mp3file="" /></interval>
<interval number="7" ><activity name="Activity 7" description="Do something 7" duration="1m" mp3file="" /></interval>
<interval number="8" ><activity name="Activity 8" description="Do something 8" duration="1m" mp3file="" /></interval>
<interval number="9" ><activity name="Activity 9" description="Do something 9" duration="1m" mp3file="" /></interval>
<interval number="10" ><activity name="Activity 10" description="Do something 10" duration="1m" mp3file="" /></interval>
<interval number="11" ><activity name="Activity 11" description="Do something 11" duration="1m" mp3file="" /></interval>
<interval number="12" ><activity name="Activity 12" description="Do something 12" duration="1m" mp3file="" /></interval>
<interval number="13" ><activity name="Activity 13" description="Do something 13" duration="1m" mp3file="" /></interval>
<interval number="14" ><activity name="Activity 14" description="Do something 14" duration="1m" mp3file="" /></interval>
<interval number="15" ><activity name="Activity 15" description="Do something 15" duration="1m" mp3file="" /></interval>
<interval number="16" ><activity name="Activity 16" description="Do something 16" duration="1m" mp3file="" /></interval>
<interval number="17" ><activity name="Activity 17" description="Do something 17" duration="1m" mp3file="" /></interval>
<interval number="18" ><activity name="Activity 18" description="Do something 18" duration="1m" mp3file="" /></interval>
<interval number="19" ><activity name="Activity 19" description="Do something 19" duration="1m" mp3file="" /></interval>
<interval number="20" ><activity name="Activity 20" description="Do something 20" duration="1m" mp3file="" /></interval>
</session>
</sessions>
''';

class MockFileHandlingFunctions extends Mock implements FileHandlingFunctions {}

main() {
  MockFileHandlingFunctions mockFileHandlingFunctions;
  List<SessionData> sessionDataList;

  // Function loadData = () async {
  //   return SessionDataTestData.sessionDataList;
  // };
  // Function checkPartial = () async {
  //   return false;
  // };

  setUp(() {
    mockFileHandlingFunctions = MockFileHandlingFunctions();
    sessionDataList = SessionDataTestData.sessionDataList;

    when(mockFileHandlingFunctions.loadData())
        .thenAnswer((_) async => sessionDataList);
    when(mockFileHandlingFunctions.partialSessionExists())
        .thenAnswer((_) async => false);
  });
  group('Scrollability Tests:', () {
    Session session = Session.readSessions(sessionString)[0];

    testWidgets('Test only the top activities show',
        (WidgetTester tester) async {
      await tester.pumpWidget(
        ChangeNotifierProvider(
          create: (context) => AppData(
            fileHandlingFunctions: mockFileHandlingFunctions,
          ),
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  PartialActivityList(
                    session: session,
                    fromActivity: 0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text("Activity 2"), findsOneWidget);
      expect(find.text("Activity 20"), findsNothing);
    });
    testWidgets('Test only bottom activities show after scrolling',
        (WidgetTester tester) async {
      await tester.pumpWidget(
        ChangeNotifierProvider(
          create: (context) => AppData(
            fileHandlingFunctions: mockFileHandlingFunctions,
          ),
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  PartialActivityList(
                    session: session,
                    fromActivity: 0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final gesture = await tester
          .startGesture(Offset(10, 10) /* THe position of your listview */);
      await gesture.moveBy(const Offset(10, -1500));
      await tester.pump(); // flush the widget tree
      expect(find.text("Activity 2"), findsNothing);
      expect(find.text("Activity 20"), findsOneWidget);
    });
  });
  group('Activities omitted at the top Tests:', () {
    Session session = Session.readSessions(sessionString)[0];

    testWidgets('Omit Activity 1', (WidgetTester tester) async {
      await tester.pumpWidget(
        ChangeNotifierProvider(
          create: (context) => AppData(
            fileHandlingFunctions: mockFileHandlingFunctions,
          ),
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  PartialActivityList(
                    session: session,
                    fromActivity: 0,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text("Activity 1"), findsNothing);
      expect(find.text("Activity 2"), findsOneWidget);
    });
    testWidgets('Omit Activity 2', (WidgetTester tester) async {
      await tester.pumpWidget(
        ChangeNotifierProvider(
          create: (context) => AppData(
            fileHandlingFunctions: mockFileHandlingFunctions,
          ),
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  PartialActivityList(
                    session: session,
                    fromActivity: 1,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text("Activity 2"), findsNothing);
      expect(find.text("Activity 3"), findsOneWidget);
    });
    testWidgets('Omit Activity 3', (WidgetTester tester) async {
      await tester.pumpWidget(
        ChangeNotifierProvider(
          create: (context) => AppData(
            fileHandlingFunctions: mockFileHandlingFunctions,
          ),
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  PartialActivityList(
                    session: session,
                    fromActivity: 2,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text("Activity 3"), findsNothing);
      expect(find.text("Activity 4"), findsOneWidget);
    });
    testWidgets('Omit Activity 10', (WidgetTester tester) async {
      await tester.pumpWidget(
        ChangeNotifierProvider(
          create: (context) => AppData(
            fileHandlingFunctions: mockFileHandlingFunctions,
          ),
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  PartialActivityList(
                    session: session,
                    fromActivity: 9,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text("Activity 10"), findsNothing);
      expect(find.text("Activity 11"), findsOneWidget);
    });
    testWidgets('Omit Activity 19', (WidgetTester tester) async {
      await tester.pumpWidget(
        ChangeNotifierProvider(
          create: (context) => AppData(
            fileHandlingFunctions: mockFileHandlingFunctions,
          ),
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  PartialActivityList(
                    session: session,
                    fromActivity: 18,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text("Activity 19"), findsNothing);
      expect(find.text("Activity 20"), findsOneWidget);
    });
    testWidgets('Omit Activity 20', (WidgetTester tester) async {
      await tester.pumpWidget(
        ChangeNotifierProvider(
          create: (context) => AppData(
            fileHandlingFunctions: mockFileHandlingFunctions,
          ),
          child: MaterialApp(
            home: Scaffold(
              body: Column(
                children: [
                  PartialActivityList(
                    session: session,
                    fromActivity: 19,
                  ),
                ],
              ),
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text("Activity 19"), findsNothing);
      expect(find.text("Activity 20"), findsNothing);
    });
  });
}
