import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:exercise/widgets/list_view/session_data_list_view.dart';
import 'package:mockito/mockito.dart';

import '../../basic_data_classes/session_data_test_data.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  SessionData sessionData = SessionDataTestData.longSessionData;
  Widget testWidget;
  MockNavigatorObserver mockNavigationObserver;

  setUp(() async {
    mockNavigationObserver = MockNavigatorObserver();

    testWidget = MaterialApp(
      home: SessionDataListView(
        sessionData: sessionData,
      ),
      navigatorObservers: [mockNavigationObserver],
    );
  });

  testWidgets('First Activity Visible', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = 0;
    final firstActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(firstActivityFinder, findsOneWidget);
    final firstActivityTextFinder = find.descendant(
        of: firstActivityFinder,
        matching: find.text(sessionData.activities[index].activityName));
    expect(firstActivityTextFinder, findsOneWidget);
  });
  testWidgets('Second Activity Visible', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = 1;
    final secondActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(secondActivityFinder, findsOneWidget);
    final secondActivityTextFinder = find.descendant(
        of: secondActivityFinder,
        matching: find.text(sessionData.activities[index].activityName));
    expect(secondActivityTextFinder, findsOneWidget);
  });
  testWidgets('Last Activity Not Visible', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = sessionData.activities.length - 1;
    final lastActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(lastActivityFinder, findsNothing);
  });
  testWidgets('Can Scroll to Last Activity Visible',
      (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    final listViewFinder = find.byType(ListView);
    int index = sessionData.activities.length - 1;
    final lastActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(lastActivityFinder, findsNothing);
    await tester.dragUntilVisible(
      lastActivityFinder,
      listViewFinder,
      const Offset(0, -250),
    );
    expect(lastActivityFinder, findsOneWidget);
    final lastActivityTextFinder = find.descendant(
        of: lastActivityFinder,
        matching: find.text(sessionData.activities[index].activityName));
    expect(lastActivityTextFinder, findsOneWidget);
  });
  testWidgets('On Scroll First Activity Not Visible',
      (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    final listViewFinder = find.byType(ListView);
    int index = sessionData.activities.length - 1;
    final lastActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(lastActivityFinder, findsNothing);
    await tester.dragUntilVisible(
      lastActivityFinder,
      listViewFinder,
      const Offset(0, -250),
    );
    expect(lastActivityFinder, findsOneWidget);
    index = 0;
    final firstActivityFinder =
        find.byKey(Key('showSessionDataActivity_' + index.toString()));
    expect(firstActivityFinder, findsNothing);
  });
}
