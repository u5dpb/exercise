import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/widgets/list_view/session_list_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:provider/provider.dart';

import '../../basic_data_classes/session_data_test_data.dart';

class MockFileHandlingFunctions extends Mock implements FileHandlingFunctions {}

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

main() {
  MockFileHandlingFunctions mockFileHandlingFunctions;
  MockNavigatorObserver mockObserver;

  Widget testWidget;

  List<SessionData> sessionDataList;

  AppData appData;

  setUp(() {
    sessionDataList = SessionDataTestData.sessionDataList;

    mockFileHandlingFunctions = MockFileHandlingFunctions();
    when(mockFileHandlingFunctions.loadData())
        .thenAnswer((_) async => sessionDataList);
    when(mockFileHandlingFunctions.partialSessionExists())
        .thenAnswer((_) async => false);

    appData = AppData(
      fileHandlingFunctions: mockFileHandlingFunctions,
    );
    mockObserver = MockNavigatorObserver();
    testWidget = ChangeNotifierProvider(
      create: (context) => appData,
      child: MaterialApp(
        home: ChooseSessionListView(),
        navigatorObservers: [mockObserver],
      ),
    );
  });
  testWidgets('Can see the first item', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = 0;
    final itemFinder = find.byKey(Key('chooseSession_' + index.toString()));
    expect(itemFinder, findsOneWidget);
    final itemTitleFinder = find.descendant(
        of: itemFinder,
        matching: find.text(appData.sessionList[index].sessionName));
    expect(itemTitleFinder, findsOneWidget);
    //expect(itemListTileWidget.title, 'Short Run');
  });
  testWidgets('Can see the second item', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = 1;
    final itemFinder = find.byKey(Key('chooseSession_' + index.toString()));
    expect(itemFinder, findsOneWidget);
    final itemTitleFinder = find.descendant(
        of: itemFinder,
        matching: find.text(appData.sessionList[index].sessionName));
    expect(itemTitleFinder, findsOneWidget);
    //expect(itemListTileWidget.title, 'Short Run');
  });
  testWidgets('Can see the last item', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle();
    int index = appData.sessionList.length - 1;
    final itemFinder = find.byKey(Key('chooseSession_' + index.toString()));
    expect(itemFinder, findsOneWidget);
    final itemTitleFinder = find.descendant(
        of: itemFinder,
        matching: find.text(appData.sessionList[index].sessionName));
    expect(itemTitleFinder, findsOneWidget);
    //expect(itemListTileWidget.title, 'Short Run');
  });
}
