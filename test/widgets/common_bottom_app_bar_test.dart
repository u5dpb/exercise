import 'package:exercise/widgets/common_bottom_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class MockNavigatorObserver extends Mock implements NavigatorObserver {}

void main() {
  MockNavigatorObserver mockObserver;
  Widget testWidget;
  setUp(() {
    mockObserver = MockNavigatorObserver();
    testWidget = MaterialApp(
      initialRoute: 'main',
      home: Scaffold(
        body: Container(),
        bottomNavigationBar: CommonBottomAppBar(),
      ),
      navigatorObservers: [mockObserver],
    );
  });
  testWidgets('Back Button Test', (WidgetTester tester) async {
    await tester.pumpWidget(testWidget);
    expect(find.byKey(Key('backButton')), findsOneWidget);
    await tester.tap(find.byKey(Key('backButton')));
    await tester.pumpAndSettle();
    verify(mockObserver.didPop(any, any));
  });
}
