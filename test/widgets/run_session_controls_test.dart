import 'package:exercise/widgets/run_session_controls.dart';
import 'package:exercise/widgets/run_session_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group('Appearance Tests:', () {
    testWidgets('Container Colour', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: 0,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final containerWidget =
          tester.firstWidget(find.byKey(Key('runSessionControls_Material')))
              as Material;
      expect(containerWidget.color, Colors.white);
    });
    testWidgets('Time Text 0', (WidgetTester tester) async {
      int seconds = 0;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('00:00:00'), findsOneWidget);
    });
    testWidgets('Time Text 1', (WidgetTester tester) async {
      int seconds = 1;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('00:00:01'), findsOneWidget);
    });
    testWidgets('Time Text 2', (WidgetTester tester) async {
      int seconds = 2;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('00:00:02'), findsOneWidget);
    });
    testWidgets('Time Text 59', (WidgetTester tester) async {
      int seconds = 59;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('00:00:59'), findsOneWidget);
    });
    testWidgets('Time Text 60', (WidgetTester tester) async {
      int seconds = 60;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('00:01:00'), findsOneWidget);
    });
    testWidgets('Time Text 61', (WidgetTester tester) async {
      int seconds = 61;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('00:01:01'), findsOneWidget);
    });
    testWidgets('Time Text 3599', (WidgetTester tester) async {
      int seconds = 3599;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('00:59:59'), findsOneWidget);
    });
    testWidgets('Time Text 3600', (WidgetTester tester) async {
      int seconds = 3600;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('01:00:00'), findsOneWidget);
    });
    testWidgets('Time Text 3601', (WidgetTester tester) async {
      int seconds = 3601;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: seconds,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      expect(find.text('01:00:01'), findsOneWidget);
    });
    testWidgets('Pause Icon', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: 0,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final iconButtonFinder = find.byKey(Key('pauseButton'));
      final icon =
          (tester.firstWidget(iconButtonFinder) as RunSessionIconButton)
              .buttonIcon;

      expect(icon, Icons.pause_rounded);
    });
    testWidgets('Play Icon', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: 0,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final iconButtonFinder = find.byKey(Key('playButton'));
      final icon =
          (tester.firstWidget(iconButtonFinder) as RunSessionIconButton)
              .buttonIcon;

      expect(icon, Icons.play_arrow_rounded);
    });
    testWidgets('Stop Icon', (WidgetTester tester) async {
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {},
              seconds: 0,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final iconButtonFinder = find.byKey(Key('stopButton'));
      final icon =
          (tester.firstWidget(iconButtonFinder) as RunSessionIconButton)
              .buttonIcon;

      expect(icon, Icons.stop_rounded);
    });
  });
  group('Function Tests:', () {
    // check pause function
    testWidgets('Pause Button', (WidgetTester tester) async {
      bool paused = false;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {
                paused = true;
              },
              onStop: () {},
              seconds: 0,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final iconButtonFinder = find.byKey(Key('pauseButton'));
      await tester.tap(iconButtonFinder);
      expect(paused, true);
    });
// check play function
    testWidgets('Play Button', (WidgetTester tester) async {
      bool playing = false;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {
                playing = true;
              },
              onPause: () {},
              onStop: () {},
              seconds: 0,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final iconButtonFinder = find.byKey(Key('playButton'));
      await tester.tap(iconButtonFinder);
      expect(playing, true);
    });
    testWidgets('Stop Button', (WidgetTester tester) async {
      bool stopped = false;
      await tester.pumpWidget(
        MaterialApp(
          home: Scaffold(
            body: RunSessionControls(
              onPlay: () {},
              onPause: () {},
              onStop: () {
                stopped = true;
              },
              seconds: 0,
            ),
          ),
        ),
      );
      await tester.pumpAndSettle();
      final iconButtonFinder = find.byKey(Key('stopButton'));
      await tester.tap(iconButtonFinder);
      expect(stopped, true);
    });
// check stop function
  });
}
