import 'package:exercise/io/firebase_sync.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:firebase_core_platform_interface/firebase_core_platform_interface.dart';

class MockFirebaseAuth extends Mock implements FirebaseAuth {
  Future<MockUserCredential> signInWithEmailAndPassword(
      {@required String email, @required String password});
  User currentUser;
}

class MockUser extends Mock implements User {}

class MockFirebase extends Mock implements Firebase {}

class MockUserCredential extends Mock implements UserCredential {}

void setupFirebaseAuthMocks() {
  TestWidgetsFlutterBinding.ensureInitialized();

  MethodChannelFirebase.channel.setMockMethodCallHandler((call) async {
    if (call.method == 'Firebase#initializeCore') {
      return [
        {
          'name': defaultFirebaseAppName,
          'options': {
            'apiKey': '123',
            'appId': '123',
            'messagingSenderId': '123',
            'projectId': '123',
          },
          'pluginConstants': {},
        }
      ];
    }

    if (call.method == 'Firebase#initializeApp') {
      return {
        'name': call.arguments['appName'],
        'options': call.arguments['options'],
        'pluginConstants': {},
      };
    }

    return null;
  });
}

Future<T> neverEndingFuture<T>() async {
  // ignore: literal_only_boolean_expressions
  while (true) {
    await Future.delayed(const Duration(minutes: 5));
  }
}

main() {
  setupFirebaseAuthMocks();
  MockFirebaseAuth mockFirebaseAuth;
  // ignore: unused_local_variable
  MockFirebase mockFirebase;
  MockUserCredential mockUserCredential;
  MockUser mockUser;

  FirebaseSync firebaseSync;
  FirebaseSync firebaseSyncNotSetup;

  setUp(() {
    mockFirebase = MockFirebase();
    mockFirebaseAuth = MockFirebaseAuth();
    mockUserCredential = MockUserCredential();
    mockUser = MockUser();
    when(
      mockFirebaseAuth.signInWithEmailAndPassword(
        email: argThat(contains("email"), named: "email"),
        password: argThat(contains("password"), named: "password"),
      ),
    ).thenAnswer((_) async {
      mockFirebaseAuth.currentUser = mockUser;
      return mockUserCredential;
    });
    when(
      mockFirebaseAuth.signInWithEmailAndPassword(
        email: anyNamed("email"),
        password: argThat(contains("incorrectPassword"), named: "password"),
      ),
    ).thenThrow(FirebaseAuthException(code: 'wrong-password'));
    when(
      mockFirebaseAuth.signInWithEmailAndPassword(
        email: argThat(contains("incorrectEmail"), named: "email"),
        password: anyNamed("password"),
      ),
    ).thenThrow(FirebaseAuthException(code: 'user-not-found'));
    firebaseSync = FirebaseSync(firebaseAuth: mockFirebaseAuth);
    firebaseSyncNotSetup = FirebaseSync(
        firebaseAuth: mockFirebaseAuth,
        firebaseInitializeApp: () async {
          await Future.delayed(Duration(seconds: 2));
        });
  });
  group("Sign in tests", () {
    test("Does the correct login not login if setup has not finished?",
        () async {
      firebaseSync.setUser(email: "email", password: "password");
      expect(firebaseSyncNotSetup.state, FirebaseSyncState.initial);

      Function call = () {
        firebaseSyncNotSetup.signIn();
      };
      expect(call, returnsNormally);
      //await Future.delayed(Duration(seconds: 2));

      expect(firebaseSyncNotSetup.state, FirebaseSyncState.initial);

      //Firebase.initializeApp();
      await Future.delayed(Duration(seconds: 2));
      expect(call, returnsNormally);
      await Future.delayed(Duration(seconds: 2));

      expect(firebaseSyncNotSetup.state, FirebaseSyncState.loggedin);
    });
    test("Does the correct login details log in correctly?", () async {
      firebaseSync.setUser(email: "email", password: "password");
      Function call = () {
        firebaseSync.signIn();
      };
      expect(call, returnsNormally);
      await Future.delayed(Duration(seconds: 2));

      expect(firebaseSync.state, FirebaseSyncState.loggedin);
    });
    test("Does a bad username throw the correct Failure?", () async {
      firebaseSync.setUser(
          email: "incorrectEmail", password: "correctPassword");

      expect(firebaseSync.state, FirebaseSyncState.setup);
      // expect(call, throwsException);
      Function callThrown = (failure) => failure.toString() == 'user-not-found';
      expect(() => firebaseSync.signIn(), throwsA(callThrown));
    });

    test("Does a bad password throw the correct failure?", () {
      firebaseSync.setUser(
          email: "correctEmail", password: "incorrectPassword");

      expect(firebaseSync.state, FirebaseSyncState.setup);
      // expect(call, throwsException);
      Function callThrown = (failure) => failure.toString() == 'wrong-password';
      expect(() => firebaseSync.signIn(), throwsA(callThrown));
    });
  });
  group("Sync SessionData Tests", () {
    test("Does sync method log in if not already logged in?", () async {
      firebaseSync.setUser(email: "email", password: "password");
      verifyNever(mockFirebaseAuth.signInWithEmailAndPassword(
        email: argThat(contains("email"), named: "email"),
        password: argThat(contains("password"), named: "password"),
      ));
      firebaseSync.syncData();
      await Future.delayed(Duration(seconds: 2));
      verify(mockFirebaseAuth.signInWithEmailAndPassword(
        email: argThat(contains("email"), named: "email"),
        password: argThat(contains("password"), named: "password"),
      ));
    });
    test("Does sync method not log in if already logged in?", () async {
      firebaseSync.setUser(email: "email", password: "password");
      firebaseSync.signIn();
      verify(mockFirebaseAuth.signInWithEmailAndPassword(
        email: argThat(contains("email"), named: "email"),
        password: argThat(contains("password"), named: "password"),
      )).called(1);
      firebaseSync.syncData();
      await Future.delayed(Duration(seconds: 2));
      verifyNever(mockFirebaseAuth.signInWithEmailAndPassword(
        email: argThat(contains("email"), named: "email"),
        password: argThat(contains("password"), named: "password"),
      ));
    });
    // connects to firestore
    // downloads the session data
    // adds any new sessions
    // does not add any existing sessions
    // uploads any new sessions
    // does not upload any existing sessions
  });
}
