import 'dart:io';

import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/io/file_io_functions.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/basic_data_classes/session_data_partial.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:path_provider/path_provider.dart';
import '../basic_data_classes/session_data_partial_test_data.dart';
import '../basic_data_classes/session_data_test_data.dart';

class MockFileIOFunctions extends Mock implements FileIOFunctions {}

main() {
  List<SessionData> sessionDataList;
  FileHandlingFunctions fileHandlingFunctions;
  MockFileIOFunctions mockFileIOFunctions;
  Function readSessionDataDir;
  bool readSessionDataDirCalled;

  setUp(() {
    sessionDataList = SessionDataTestData.sessionDataList;
    mockFileIOFunctions = MockFileIOFunctions();

    readSessionDataDirCalled = false;
  });

  group('loadData Tests', () {
    test('Is a List<SessionData>> returned correctly?', () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      readSessionDataDirCalled = false;
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      List<SessionData> returnList = await fileHandlingFunctions.loadData(
          readSessionDataDir: readSessionDataDir);
      await Future.delayed(Duration(seconds: 2));

      expect(returnList, sessionDataList);
      verify(mockFileIOFunctions.getApplicationDirectory());
      verify(mockFileIOFunctions.directoryExists(any));
      verifyNever(mockFileIOFunctions.directoryCreate(any));
      expect(readSessionDataDirCalled, true);
    });
    test('Is a new data directory created correctly if it does not exist?',
        () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(false));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));

      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      readSessionDataDirCalled = false;
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      List<SessionData> returnList = await fileHandlingFunctions.loadData(
          readSessionDataDir: readSessionDataDir);
      await Future.delayed(Duration(seconds: 2));
      expect(returnList, sessionDataList);
      verify(mockFileIOFunctions.getApplicationDirectory());
      verify(mockFileIOFunctions.directoryExists(any));
      verify(mockFileIOFunctions.directoryCreate(any));
      expect(readSessionDataDirCalled, true);
    });
    test('Is a MissingPlatformDirectoryException caught when thrown?',
        () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(false));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenThrow(MissingPlatformDirectoryException("None"));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      Function callToThrow = () => fileHandlingFunctions.loadData(
            readSessionDataDir: readSessionDataDir,
          );
      Function callThrown =
          (failure) => failure.toString() == 'Could not open app directory';
      expect(callToThrow, throwsA(callThrown));
      await Future.delayed(Duration(seconds: 2));
      verify(mockFileIOFunctions.getApplicationDirectory());
      verifyNever(mockFileIOFunctions.directoryExists(any));
      verifyNever(mockFileIOFunctions.directoryCreate(any));
      expect(readSessionDataDirCalled, false);
    });
    test('Is a FileSystemException caught when thrown in directoryExists?',
        () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenThrow(FileSystemException("None"));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      Function callToThrow = () => fileHandlingFunctions.loadData(
            readSessionDataDir: readSessionDataDir,
          );
      Function callThrown = (failure) =>
          failure.toString() ==
          "Error accessing the phone's files\nFileSystemException: None, path = ''";
      expect(callToThrow, throwsA(callThrown));
      await Future.delayed(Duration(seconds: 2));
      verify(mockFileIOFunctions.getApplicationDirectory());
      verify(mockFileIOFunctions.directoryExists(any));
      verifyNever(mockFileIOFunctions.directoryCreate(any));
      expect(readSessionDataDirCalled, false);
    });
    test('Is a FileSystemException caught when thrown in directoryCreate?',
        () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(false));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenThrow(FileSystemException("None"));
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      Function callToThrow = () => fileHandlingFunctions.loadData(
            readSessionDataDir: readSessionDataDir,
          );
      Function callThrown = (failure) =>
          failure.toString() ==
          "Error accessing the phone's files\nFileSystemException: None, path = ''";
      expect(callToThrow, throwsA(callThrown));
      await Future.delayed(Duration(seconds: 2));

      verify(mockFileIOFunctions.getApplicationDirectory());
      verify(mockFileIOFunctions.directoryExists(any));
      verify(mockFileIOFunctions.directoryCreate(any));
      expect(readSessionDataDirCalled, false);
    });
  });
  group('partialSessionExists Tests', () {
    test('is bool returned correctly as true?', () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.fileExists(any))
          .thenAnswer((_) async => Future.value(true));
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      bool result = await fileHandlingFunctions.partialSessionExists();
      await Future.delayed(Duration(seconds: 2));

      expect(result, true);
      verify(mockFileIOFunctions.getApplicationDirectory());
      verify(mockFileIOFunctions.fileExists(any));
    });
    test('is bool returned correctly as false?', () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.fileExists(any))
          .thenAnswer((_) async => Future.value(false));
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      bool result = await fileHandlingFunctions.partialSessionExists();
      await Future.delayed(Duration(seconds: 2));

      expect(result, false);
      verify(mockFileIOFunctions.getApplicationDirectory());
      verify(mockFileIOFunctions.fileExists(any));
    });
    test('Is a MissingPlatformDirectoryException caught when thrown?',
        () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenThrow(MissingPlatformDirectoryException("None"));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.fileExists(any))
          .thenAnswer((_) async => Future.value(false));
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      var callToThrow = () => fileHandlingFunctions.partialSessionExists();
      var callThrown =
          (failure) => failure.toString() == 'Could not open app directory';
      expect(callToThrow, throwsA(callThrown));
      await Future.delayed(Duration(seconds: 2));
      verify(mockFileIOFunctions.getApplicationDirectory());
      verifyNever(mockFileIOFunctions.fileExists(any));
    });

    test('Is a FileSystemException caught when thrown?', () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.fileExists(any))
          .thenThrow(FileSystemException("None"));
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      var callToThrow = () => fileHandlingFunctions.partialSessionExists();
      var callThrown = (failure) =>
          failure.toString() ==
          "Error accessing the phone's files\nFileSystemException: None, path = ''";
      expect(callToThrow, throwsA(callThrown));
      await Future.delayed(Duration(seconds: 2));
      //expect(result, true);
      verify(mockFileIOFunctions.getApplicationDirectory());
      verify(mockFileIOFunctions.fileExists(any));
    });
  });
  group('loadPartialSession Tests', () {
    bool readIncompleteSessionDataCalled;
    SessionDataPartial returnSdp;

    Function readIncompleteSessionData = (dataString) {
      readIncompleteSessionDataCalled = true;
      return SessionDataPartialTestData.unfinishedSession;
    };
    setUp(() {
      returnSdp = null;
    });
    test('Is SessionDataPartial returned correctly?', () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.fileExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.readFileAsString(any))
          .thenAnswer((_) async => Future.value("string"));
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );

      returnSdp = await fileHandlingFunctions.loadPartialSession(
        readIncompleteSessionData: readIncompleteSessionData,
      );
      await Future.delayed(Duration(seconds: 2));
      expect(returnSdp, SessionDataPartialTestData.unfinishedSession);
      verify(mockFileIOFunctions.readFileAsString(any));
      expect(readIncompleteSessionDataCalled, true);
    });
    test('Does FileException catch and throw a Failure?', () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.fileExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.readFileAsString(any))
          .thenThrow(FileSystemException("None"));
      readIncompleteSessionDataCalled = false;
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      var callToThrow = () => fileHandlingFunctions.loadPartialSession(
            readIncompleteSessionData: readIncompleteSessionData,
          );
      var callThrown = (failure) =>
          failure.toString() ==
          "Error accessing the phone's files\nFileSystemException: None, path = ''";
      expect(callToThrow, throwsA(callThrown));
      await Future.delayed(Duration(seconds: 2));
      verify(mockFileIOFunctions.readFileAsString(any));
      expect(readIncompleteSessionDataCalled, false);
    });
    test('Does incompleteSession catch and throw a Failure?', () async {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.fileExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.readFileAsString(any))
          .thenAnswer((_) async => Future.value("string"));
      readIncompleteSessionDataCalled = false;
      readSessionDataDirCalled = false;
      readSessionDataDir = () {
        readSessionDataDirCalled = true;
        return sessionDataList;
      };
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      var callToThrow = () => fileHandlingFunctions.loadPartialSession(
            readIncompleteSessionData: (dataString) {
              readIncompleteSessionDataCalled = true;
              return SessionDataPartialTestData.incompleteSession;
            },
          );
      var callThrown =
          (failure) => failure.toString() == 'Incomplete Session Data';
      expect(callToThrow, throwsA(callThrown));
      await Future.delayed(Duration(seconds: 2));
      verify(mockFileIOFunctions.readFileAsString(any));
      expect(readIncompleteSessionDataCalled, true);
    });
  });
  group('deletePartialSession Tests', () {
    setUp(() {
      when(mockFileIOFunctions.directoryExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.getApplicationDirectory())
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.directoryCreate(any))
          .thenAnswer((_) async => Future.value(Directory('/home/danb')));
      when(mockFileIOFunctions.fileExists(any))
          .thenAnswer((_) async => Future.value(true));
      when(mockFileIOFunctions.deleteFile(any))
          .thenAnswer((_) async => Future.value(null));
    });
    test('Is File deleted correctly?', () async {
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      fileHandlingFunctions.deletePartialSession();
      await Future.delayed(Duration(seconds: 2));

      verify(mockFileIOFunctions.deleteFile(any));
    });
    test('Does FileException throw and catch a Failure?', () async {
      when(mockFileIOFunctions.deleteFile(any))
          .thenThrow(FileSystemException("None"));
      fileHandlingFunctions = FileHandlingFunctions(
        fileIOFunctions: mockFileIOFunctions,
      );
      var callToThrow = () => fileHandlingFunctions.deletePartialSession();
      var callThrown = (failure) =>
          failure.toString() == "Problem deleting the incomplete session";
      expect(callToThrow, throwsA(callThrown));
      await Future.delayed(Duration(seconds: 2));

      verify(mockFileIOFunctions.deleteFile(any));
    });
  });
}
