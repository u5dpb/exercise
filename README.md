# Exercise App

This is an app for running and keeping records of times exercise sessions. Timed exercise sessions could include running (e.g. Couch to 5K), cycling (e.g. interval sessions) or circuit training.

Specifically a sessions is a series of times activities. Activities are:
* a form of exercise
* a "warm up"
* "warm down"
* an active rest period (e.g. walking in a running session)
* an inactive rest period

## Intended features

* Time and run an exercise session.
* Optional location monitoring.
* Built in sessions for Couch to 5K and circuit training.
* The ability to create custom sessions and custom activities within sessions.
* A record of each session performed.
* Data on performance.  (e.g. graphs and maps).

## Compiling

The App is written in Flutter so this needs to be installed. For details on this see: https://flutter.dev/docs/get-started/install

Once Flutter is installed go to the application home directory and run:



## Version 1.0

Features:

![Main Screen](design/v_1.0/screen_main.png)

* The main screen only has two options.
* A rudimentary logo has been made.

![Main Screen](design/v_1.0/screen_choose_session.png)

* A couple of sessions have been built in.
* Currently no option to modify or add sessions.

![Main Screen](design/v_1.0/screen_session_records.png)

* Basic records of the sessions performed will be presented.

![Main Screen](design/v_1.0/screen_session_go.png)

* The session will scroll through the activities.
* The session cannot be paused or stopped (these features will be added later).

This version will be run on my own phone and tested for robustness.

## Testing

Testing scenarios need to be devised. Exception handling needs to be included from this starting stage.

Robust:
* You cannot go back from the RunSession screen (done)
* If you move to another app the Session will continue to run (done)
* When you go back to this app you go back to the RunSession screen (done)
* If the app stops it will continue with the current session (or finish it) (done)
* If the app stops mid session it will ask if you want to:
** continue (done)
** restart from the current interval (done)
** stop and save (done)
** stop and abandon (done)

# Future Features

* Allow data syncing with Firebase.
* Create a resistance band circuit training session. (done)
* Stop session will allow the current session to be abandonded with the option of saving (or not) the partial session data.
* Run session screen will allow users to browse the session and choose it before the session controls will appear.
* The browse session will allow editing of sessions.
* The choose session screen will allow creation of new sessions.
* Choice of day or night themes.
* Activities will be able to record distance data.
* Location ping times for distance data will be editable.
* Weekly and monthly summaries.
