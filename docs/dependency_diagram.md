# Dependency Diagrams

Generated using Lakos

https://pub.dev/packages/lakos

Commands:

lakos -m --ignore=*test*/** . >out.dot

dot -i out.dot -Tpng -Gdpi=200 -o example.png

Some details of the .dot file that might need to be added:

digraph g{
    graph [pad="0.5", nodesep="1", ranksep="2"];