This file regards running and optimising gradle for this project.

To scan:
cd android/
./gradlew assumbleDebug --profile

Note that for the command to work java needs to be set to version 11. I don't know how vscode gets around this as it worked when the java version was higher.

Problem: this command completes rather quickly while in vscode it takes forever.

I have moved on to using:
https://github.com/gradle/gradle-profiler

The async profiler didn't seem to provide very interesting results. Everything appeared to be stuck at a single bottleneck.

I have put this in the android/gradle.properties file. Hopefully this will speed things up.
org.gradle.caching=true
=======
Using Gradle Profiler:
https://github.com/gradle/gradle-profiler

