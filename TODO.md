# TODO

- [ ] Firebase tests.
- [ ] Sync data with firestore.
- [ ] - separate app that auto logs in downloads and views Sessions via the Choose Session Screen
- [ ] - create a login screen
- [ ] - set up a remember me option of some sort
- [ ] - allow continue with logging in
- [ ] - integrate with the current app
- [ ] Documentation:
- [ ] - overview: current status, goal
- [ ] - example (web app)
- [ ] - installation
- [ ] - development details
- [ ] API for non-widgets
- [ ] - RunSessionTimer
- [ ] When function passed as an argument the required fields should be specified.

# TODOing

- [ ] Integration test with pause and restart.

# TODOne

- [x] Tidy up home directory.
- [x] Seesion extends ChangeNotifier. Does anything listen to this?
- [x] - extension removed
- [x] - verify app still works okay.
- [x] Integration Test - cannot find session name?