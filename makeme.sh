#!/bin/bash

pushd android
./gradlew app:assembleAndroidTest
./gradlew app:assembleDebug -Ptarget=integration_test/goto_and_run_session_test.dart
popd
