import 'package:exercise/model/failure.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

enum FirebaseSyncState { initial, setup, loggedin }

class FirebaseSync {
  FirebaseSyncState _state;
  FirebaseAuth firebaseAuth;
  var instance;
  Function firebaseInitializeApp;
  String _email;
  String _password;

  void setUser({@required email, @required password}) {
    _email = email;
    _password = password;
  }

  FirebaseSyncState get state => _state;

  FirebaseSync({this.firebaseAuth, this.firebaseInitializeApp}) {
    _state = FirebaseSyncState.initial;
    _email = "";
    _password = "";
    if (firebaseInitializeApp == null) {
      firebaseInitializeApp = Firebase.initializeApp;
    }
    setup();
  }

  void setup() async {
    instance = await firebaseInitializeApp();
    if (firebaseAuth == null) {
      firebaseAuth = FirebaseAuth.instance;
    }
    _state = FirebaseSyncState.setup;
  }

  void signIn() async {
    if (_state != FirebaseSyncState.initial) {
      try {
        // ignore: unused_local_variable
        UserCredential signedInUser = await firebaseAuth
            .signInWithEmailAndPassword(email: _email, password: _password);
        _state = FirebaseSyncState.loggedin;
      } on FirebaseAuthException catch (e) {
        throw Failure(e.code);
      }
    }
  }

  void syncData() async {
    if (firebaseAuth.currentUser == null) {
      signIn();
    }
  }
}
