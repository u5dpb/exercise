import 'dart:io';
import 'package:exercise/io/file_io_functions.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/basic_data_classes/session_data_partial.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import '../model/failure.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class FileHandlingFunctions {
  final FileIOFunctions fileIOFunctions;

  FileHandlingFunctions({@required this.fileIOFunctions});

  Future<List<SessionData>> loadData(
      {Function readSessionDataDir = SessionData.readSessionDataDir}) async {
    try {
      var directory;
      List<SessionData> sessionDataList = [];
      if (!kIsWeb) {
        directory = await fileIOFunctions.getApplicationDirectory();

        final dirName = '${directory.path}/data';
        print(dirName);
        bool dirExists = await fileIOFunctions.directoryExists(dirName);
        if (!dirExists) {
          await fileIOFunctions.directoryCreate(dirName);
        }
        sessionDataList = await readSessionDataDir();
      }
      return sessionDataList;
    } on MissingPlatformDirectoryException {
      throw Failure('Could not open app directory');
    } on FileSystemException catch (f) {
      throw Failure("Error accessing the phone's files\n" + f.toString());
    }
  }

  Future<bool> partialSessionExists() async {
    try {
      bool exists = false;
      if (!kIsWeb) {
        var directory = await fileIOFunctions.getApplicationDirectory();

        final filename = '${directory.path}/currentSession.xml';
        exists = await fileIOFunctions.fileExists(filename);
      }
      return exists;
    } on MissingPlatformDirectoryException {
      throw Failure('Could not open app directory');
    } on FileSystemException catch (f) {
      throw Failure("Error accessing the phone's files\n" + f.toString());
    }
  }

  Future<SessionDataPartial> loadPartialSession({
    String filename = "currentSession.xml",
    Function readIncompleteSessionData,
  }) async {
    readIncompleteSessionData ??= (dataString) {
      return SessionDataPartial.readIncomplete(dataString);
    };
    String inputString;
    SessionDataPartial partialSessionData;
    try {
      inputString = await fileIOFunctions.readFileAsString(filename);
      partialSessionData = readIncompleteSessionData(inputString);
    } on FileSystemException catch (f) {
      throw Failure("Error accessing the phone's files\n" + f.toString());
    }
    if (partialSessionData.incompleteSession) {
      throw Failure('Incomplete Session Data');
    }
    return partialSessionData;
  }

  void deletePartialSession({
    String filename = "currentSession.xml",
  }) async {
    try {
      await fileIOFunctions.deleteFile(filename);
    } on FileSystemException {
      throw Failure('Problem deleting the incomplete session');
    }
  }
}
