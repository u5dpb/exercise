import 'dart:io';

import 'package:path_provider/path_provider.dart';

class FileIOFunctions {
  Future<Directory> getApplicationDirectory() =>
      getApplicationDocumentsDirectory();

  Future<bool> directoryExists(dirname) async {
    return Directory(dirname).exists();
  }

  Future<Directory> directoryCreate(dirname) async {
    return Directory(dirname).create();
  }

  Future<bool> fileExists(filename) async {
    return File(filename).exists();
  }

  Future<String> readFileAsString(filename) async {
    return File(filename).readAsString();
  }

  Future<FileSystemEntity> deleteFile(filename) async {
    return File(filename).delete();
  }
}
