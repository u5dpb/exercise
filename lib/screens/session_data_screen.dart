import 'package:exercise/widgets/common_bottom_app_bar.dart';
import 'package:exercise/widgets/list_view/session_data_summary_list_view.dart';
import 'package:flutter/material.dart';
import 'package:exercise/widgets/title_bar.dart';

class SessionDataScreen extends StatefulWidget {
  static const String id = "session_data";
  @override
  _SessionDataScreenState createState() => _SessionDataScreenState();
}

class _SessionDataScreenState extends State<SessionDataScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TitleAppBar(title: 'Session Data'),
      body: SessionDataSummaryListView(),
      bottomNavigationBar: CommonBottomAppBar(),
    );
  }
}
