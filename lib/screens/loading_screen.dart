import 'package:exercise/constant.dart';
import 'package:exercise/model/run_session_timer.dart';
import 'package:exercise/basic_data_classes/session_data_partial.dart';
import 'package:exercise/model/speak.dart';
import 'package:exercise/screens/run_session_screen.dart';
import 'package:exercise/widgets/logo.dart';
import 'package:flutter/material.dart';
import 'package:exercise/screens/main_screen.dart';
import 'package:exercise/model/app_data.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/scheduler.dart';

// the LoadingScreen displays the progress of the setup/loading
// functions of the AppData class

class LoadingScreen extends StatefulWidget {
  static const String id = 'loading_screen';
  final spinnerWidget = Scaffold(
    body: SpinKitWanderingCubes(color: Colors.white, size: 100),
  );
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  @override
  Widget build(BuildContext context) {
    if (Provider.of<AppData>(context).state == NotifierState.partialFound) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        showDialog<String>(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => ParitalFoundErrorScreen(),
        );
      });
    } else if (Provider.of<AppData>(context).state == NotifierState.error) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        showDialog<String>(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => LoadingErrorScreen(),
        );
      });
    } else if (Provider.of<AppData>(context).state ==
        NotifierState.partialError) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        showDialog<String>(
          barrierDismissible: false,
          context: context,
          builder: (BuildContext context) => PartialLoadErrorScreen(),
        );
      });
    } else if (Provider.of<AppData>(context).state ==
        NotifierState.partialLoaded) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) {
              SessionDataPartial sessionDataPartial =
                  Provider.of<AppData>(context, listen: false)
                      .sessionDataPartial;
              Provider.of<RunSessionTimer>(context, listen: false)
                  .currentSession = sessionDataPartial;
              // Provider.of<RunSessionTimer>(context, listen: false).initialise(
              //     restoreSession: sessionDataPartial.activityCompleted[0]);
              return RunSession(
                speechDriver: Speak(),
                restoreSession: sessionDataPartial.activityCompleted[0],
              );
            },
          ),
        );
      });
    } else if (Provider.of<AppData>(context).state == NotifierState.loaded) {
      SchedulerBinding.instance.addPostFrameCallback((_) {
        Navigator.pushReplacementNamed(context, MainScreen.id);
      });
    }
    print(Provider.of<AppData>(context).state);
    return Logo(); //widget.spinnerWidget;
  }
}

class LoadingErrorScreen extends StatelessWidget {
  const LoadingErrorScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Error Loading Data',
        style: kDialogueTitle,
      ),
      content: Text(
        Provider.of<AppData>(context, listen: false).failure.toString(),
        style: kDialogueText,
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.pushReplacementNamed(context, MainScreen.id);
          },
          child: const Text(
            'Try to Continue',
            style: kDialogueButton,
          ),
        ),
        TextButton(
          onPressed: () {
            Provider.of<AppData>(context, listen: false).loadSessionData();
          },
          child: const Text(
            'Retry',
            style: kDialogueButton,
          ),
        ),
      ],
    );
  }
}

class ParitalFoundErrorScreen extends StatelessWidget {
  const ParitalFoundErrorScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Incomplete Session Data Found',
        style: kDialogueTitle,
      ),
      content: Text(
        Provider.of<AppData>(context, listen: false).failure.toString(),
        style: kDialogueText,
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Provider.of<AppData>(context, listen: false)
                .fileHandlingFunctions
                .deletePartialSession(filename: "currentSessionData.xml");
            Navigator.pushReplacementNamed(context, MainScreen.id);
          },
          child: const Text(
            'Delete the data',
            style: kDialogueButton,
          ),
        ),
        TextButton(
          onPressed: () {
            Provider.of<AppData>(context, listen: false).loadPartialData();
            //Navigator.pushReplacementNamed(context, LoadingScreen.id);
          },
          child: const Text(
            'Continue the session',
            style: kDialogueButton,
          ),
        ),
      ],
    );
  }
}

class PartialLoadErrorScreen extends StatelessWidget {
  const PartialLoadErrorScreen({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Error Loading Restart Session',
        style: kDialogueTitle,
      ),
      content: Text(
        Provider.of<AppData>(context, listen: false).failure.toString(),
        style: kDialogueText,
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Provider.of<AppData>(context, listen: false)
                .fileHandlingFunctions
                .deletePartialSession(filename: "currentSessionData.xml");
            Navigator.pushReplacementNamed(context, MainScreen.id);
          },
          child: const Text(
            'Delete',
            style: kDialogueButton,
          ),
        ),
        TextButton(
          onPressed: () {
            print('Retry');
            Navigator.pushReplacementNamed(context, LoadingScreen.id);
          },
          child: const Text(
            'Retry',
            style: kDialogueButton,
          ),
        ),
      ],
    );
  }
}
