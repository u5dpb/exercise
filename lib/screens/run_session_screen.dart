import 'package:exercise/constant.dart';
// import 'package:exercise/model/run_session_state.dart';
import 'package:exercise/model/run_session_timer.dart';
import 'package:exercise/model/speak.dart';
import 'package:exercise/widgets/list_tiles/session_activity_tile.dart';
import 'package:exercise/widgets/list_view/partial_activity_list.dart';
import 'package:exercise/widgets/run_session_bottom_bar.dart';
import 'package:exercise/widgets/run_session_controls.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/widgets/title_bar.dart';
import 'package:file/file.dart';
import 'package:file/local.dart';

class RunSession extends StatefulWidget {
  static const String id = "run_session";
  // final Session currentSession;
  // final bool restoreSession;
  // final SessionDataPartial partialSessionData;
  final FileSystem fileSystem;
  final Speak speechDriver;
  final bool restoreSession;

  RunSession({
    // this.currentSession,
    // this.restoreSession,
    // this.partialSessionData,
    this.fileSystem = const LocalFileSystem(),
    this.speechDriver,
    @required this.restoreSession,
  });

  @override
  _RunSessionState createState() => _RunSessionState();
}

class _RunSessionState extends State<RunSession> {
  //int _controlPanelSeconds = 0;

  @override
  void initState() {
    SchedulerBinding.instance.addPostFrameCallback((_) {
      Provider.of<RunSessionTimer>(context, listen: false)
          .initialise(restoreSession: widget.restoreSession);
    });
    // Provider.of<RunSessionTimer>(context).setState(SessionState.initial);

    // if (widget.restoreSession) {
    //   Provider.of<RunSessionTimer>(context).restoreSessionData(
    //     partialSessionData: widget.partialSessionData,
    //     storeSessionFunction: (filename) {
    //       Provider.of<AppData>(context, listen: false).addSessionData(filename);
    //     },
    //   );
    // } else {
    //   Provider.of<RunSessionTimer>(context).setupSessionData(
    //     currentSession: widget.currentSession,
    //     storeSessionFunction: (filename) {
    //       Provider.of<AppData>(context, listen: false).addSessionData(filename);
    //     },
    //   );
    // }
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false, // to disable the back function in Android
      child: Scaffold(
          appBar: TitleAppBar(title: "Run Session"),
          body: Column(
            children: [
              if (Provider.of<RunSessionTimer>(context).state !=
                  SessionState.finished)
                SessionActivityTile(
                    index:
                        Provider.of<RunSessionTimer>(context).currentActivity(),
                    currentSession:
                        Provider.of<RunSessionTimer>(context).currentSession),
              if (Provider.of<RunSessionTimer>(context).state !=
                  SessionState.finished)
                RunSessionControls(
                  onPlay: () {
                    Provider.of<RunSessionTimer>(context, listen: false)
                        .startSession();
                  },
                  onPause: () {
                    Provider.of<RunSessionTimer>(context, listen: false)
                        .togglePause();
                  },
                  onStop: () {
                    if (Provider.of<RunSessionTimer>(context).state ==
                            SessionState.running ||
                        Provider.of<RunSessionTimer>(context).state ==
                            SessionState.paused) {
                      showDialog<String>(
                        barrierDismissible: false,
                        context: context,
                        builder: (BuildContext context) =>
                            StopSessionAlertDialog(
                          stopFunction: () {
                            Provider.of<RunSessionTimer>(context, listen: false)
                                .stopSession();
                            Navigator.pop(context, 'OK');
                          },
                        ),
                      );
                    }
                  },
                  seconds:
                      Provider.of<RunSessionTimer>(context).controlPanelSeconds,
                ),
              if (Provider.of<RunSessionTimer>(context).state ==
                  SessionState.finished)
                SessionSummary(
                    sessionData: Provider.of<RunSessionTimer>(context)
                        .currentSessionData()),
              if (Provider.of<RunSessionTimer>(context).state !=
                  SessionState.finished)
                PartialActivityList(
                  session: Provider.of<RunSessionTimer>(context).currentSession,
                  fromActivity:
                      Provider.of<RunSessionTimer>(context).currentActivity(),
                ),
            ],
          ),
          bottomNavigationBar: (Provider.of<RunSessionTimer>(context).state ==
                      SessionState.running ||
                  Provider.of<RunSessionTimer>(context).state ==
                      SessionState.paused)
              ? null
              : RunSessionBottomBar(
                  sessionInitial: Provider.of<RunSessionTimer>(context).state ==
                      SessionState.initial,
                  sessionFinished:
                      Provider.of<RunSessionTimer>(context).state ==
                          SessionState.finished,
                )),
    );
  }
}

class StopSessionAlertDialog extends StatelessWidget {
  const StopSessionAlertDialog({
    Key key,
    @required this.stopFunction,
  }) : super(key: key);

  final Function stopFunction;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text(
        'Stop Session',
        style: kDialogueTitle,
      ),
      content: const Text(
        'Do you wish to cancel the current session?',
        style: kDialogueText,
      ),
      actions: <Widget>[
        TextButton(
          onPressed: () {
            Navigator.pop(context, 'Cancel');
          },
          child: const Text(
            'Cancel',
            style: kDialogueButton,
          ),
        ),
        TextButton(
          onPressed: stopFunction,
          child: const Text(
            'OK',
            style: kDialogueButton,
          ),
        ),
      ],
    );
  }
}

class SessionSummary extends StatelessWidget {
  final SessionData sessionData;

  SessionSummary({this.sessionData});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
            child: Text(sessionData.sessionSummary(),
                style: TextStyle(fontSize: 30))),
      ],
    );
  }
}
