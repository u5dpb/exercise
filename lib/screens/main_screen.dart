import 'package:exercise/constant.dart';
import 'package:exercise/widgets/logo.dart';
import 'package:exercise/widgets/main_screen_big_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:exercise/screens/choose_session_screen.dart';
import 'package:exercise/screens/session_data_screen.dart';

class MainScreen extends StatelessWidget {
  static const String id = "main_screen";
  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightPrimaryColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Logo(),
          ),
          Expanded(
            flex: 1,
            child: MainScreenBigButton(
              key: Key('mainScreenChooseSession'),
              buttonRoute: ChooseSessionScreen.id,
              text: 'Choose Session',
            ),
          ),
          Expanded(
            flex: 1,
            child: MainScreenBigButton(
              key: Key('mainScreenSessionData'),
              buttonRoute: SessionDataScreen.id,
              text: 'Session Records',
            ),
          ),
        ],
      ),
    );
  }
}
