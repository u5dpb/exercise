import 'package:exercise/widgets/common_bottom_app_bar.dart';
import 'package:exercise/widgets/list_view/session_list_view.dart';
import 'package:flutter/material.dart';

import 'package:exercise/widgets/title_bar.dart';

class ChooseSessionScreen extends StatelessWidget {
  static const String id = "choose_session";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TitleAppBar(title: 'Choose Session'),
      body: ChooseSessionListView(),
      bottomNavigationBar: CommonBottomAppBar(),
    );
  }
}
