import 'package:exercise/widgets/common_bottom_app_bar.dart';
import 'package:exercise/widgets/list_view/session_data_list_view.dart';
import 'package:flutter/material.dart';

import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/widgets/title_bar.dart';

class ShowSessionDataScreen extends StatefulWidget {
  static const String id = "show_session_data";

  final SessionData sessionData;

  ShowSessionDataScreen({this.sessionData});
  @override
  _ShowSessionDataScreenState createState() => _ShowSessionDataScreenState();
}

class _ShowSessionDataScreenState extends State<ShowSessionDataScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TitleAppBar(
          title: widget.sessionData.getDate() +
              ' ' +
              widget.sessionData.sessionName),
      body: SessionDataListView(sessionData: widget.sessionData),
      bottomNavigationBar: CommonBottomAppBar(),
    );
  }
}
