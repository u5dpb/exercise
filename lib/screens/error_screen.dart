import 'package:flutter/material.dart';
import 'package:exercise/constant.dart';
import 'package:exercise/widgets/title_bar.dart';

class ErrorScreen extends StatelessWidget {
  static String id = 'error_screen';

  final String title;
  final String description;
  final List<ErrorAction> actions;

  ErrorScreen({this.title, this.description, this.actions});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: TitleAppBar(title: title),
      body: Column(
        children: [
          Text(description, style: kMainScreenText),
          Expanded(
            child: ErrorActionListView(actions: actions),
          )
        ],
      ),
    );
  }
}

class ErrorActionListView extends StatelessWidget {
  const ErrorActionListView({
    Key key,
    @required this.actions,
  }) : super(key: key);

  final List<ErrorAction> actions;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: actions.length,
        itemBuilder: (context, index) {
          return Card(
            color: Colors.grey,
            child: ListTile(
                key: Key('errorAction_$index'),
                title: Text(actions[index].description, style: kMainScreenText),
                onTap: actions[index].onChoose),
          );
        });
  }
}

class ErrorAction {
  final String description;
  final Function onChoose;
  ErrorAction({this.description, this.onChoose});
}
