//import 'package:exercise/model/session_data_partial.dart';
import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/io/file_io_functions.dart';
import 'package:exercise/model/speak.dart';
import 'package:exercise/screens/choose_session_screen.dart';
import 'package:exercise/screens/loading_screen.dart';
import 'package:exercise/screens/session_data_screen.dart';
import 'package:file/local.dart';
import 'package:file/memory.dart';
//import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:exercise/screens/main_screen.dart';
import 'package:exercise/model/app_data.dart';
import 'package:provider/provider.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import 'model/run_session_timer.dart';
// import 'package:exercise/screens/session_run.dart';
// import 'dart:async';
// import 'dart:io';
// import 'package:time/time.dart';
// import 'package:path_provider/path_provider.dart';

// TODO Customise the "red screen of death"

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  FileIOFunctions fileIOFunctions = FileIOFunctions();
  FileHandlingFunctions fileHandlingFunctions =
      FileHandlingFunctions(fileIOFunctions: fileIOFunctions);
  Speak speech = Speak();
  AppData appData = AppData(fileHandlingFunctions: fileHandlingFunctions);
  RunSessionTimer runSessionTimer = RunSessionTimer(
      fileSystem: kIsWeb ? MemoryFileSystem() : LocalFileSystem(),
      speachFunction: speech.speak,
      storeSessionFunction: appData.addSessionData);
  runApp(MyApp(
    fileIOFunctions: fileIOFunctions,
    fileHandlingFunctions: fileHandlingFunctions,
    speech: speech,
    appData: appData,
    runSessionTimer: runSessionTimer,
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // final bool partialSession;
  // MyApp({this.partialSession});
  final FileIOFunctions fileIOFunctions;
  final FileHandlingFunctions fileHandlingFunctions;
  final Speak speech;
  final AppData appData;
  final RunSessionTimer runSessionTimer;
  MyApp({
    @required this.fileIOFunctions,
    @required this.fileHandlingFunctions,
    @required this.speech,
    @required this.appData,
    @required this.runSessionTimer,
  });

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider<AppData>(create: (_) => appData),
          ChangeNotifierProvider<RunSessionTimer>(
              create: (_) => runSessionTimer)
        ],
        child: MaterialApp(initialRoute: LoadingScreen.id, routes: {
          LoadingScreen.id: (context) => LoadingScreen(),
          MainScreen.id: (context) => MainScreen(),
          ChooseSessionScreen.id: (context) => ChooseSessionScreen(),
          SessionDataScreen.id: (context) => SessionDataScreen(),
        }));
  }
}
