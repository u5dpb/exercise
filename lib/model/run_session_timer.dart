import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:path_provider/path_provider.dart';
import 'package:time/time.dart';
import 'package:clock/clock.dart';
import 'package:file/file.dart';
import 'package:file/local.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/basic_data_classes/session_data_partial.dart';

enum SessionState { initial, running, paused, finished }

class RunSessionTimer extends ChangeNotifier {
  SessionState _state = SessionState.initial;
  SessionState get state => _state;
  void setState(SessionState state) {
    _state = state;
    notifyListeners();
  }

  int _controlPanelSeconds = 0;
  int get controlPanelSeconds => _controlPanelSeconds;
  void setControlPanelSeconds(int controlPanelSeconds) {
    _controlPanelSeconds = controlPanelSeconds;
    notifyListeners();
  }

  Session currentSession;
  //final bool restoreSession;
  SessionDataPartial partialSessionData;
  Function _storeSessionFunction;
  final FileSystem fileSystem;
  final Function speachFunction;

  SessionData _currentSessionData;
  int _currentActivity = 0;
  Timer _timer;
  Stopwatch _stopwatch = clock.stopwatch(); //Stopwatch();
  Duration _activityStart = 0.seconds;
  int _seconds = 0;
  int _secondsOffset = 0;
  File currentSessionFile;

  int seconds() => _seconds;
  int currentActivity() => _currentActivity;
  SessionData currentSessionData() => _currentSessionData;

  RunSessionTimer(
      {this.fileSystem = const LocalFileSystem(),
      this.speachFunction,
      storeSessionFunction}) {
    _storeSessionFunction = storeSessionFunction;
  }

  @override
  void dispose() {
    if (_timer != null) {
      _timer.cancel();
    }
    super.dispose();
  }

  void initialise({bool restoreSession}) {
    setState(SessionState.initial);
    _stopwatch.reset();
    _currentActivity = 0;
    _activityStart = 0.seconds;
    _seconds = 0;
    _secondsOffset = 0;
    _controlPanelSeconds = 0;
    //File currentSessionFile;

    if (restoreSession) {
      // restoreSessionData(partialSessionData: partialSessionData);
      restoreSessionData(partialSessionData: currentSession);
    } else {
      setupSessionData(
        currentSession: currentSession,
      );
    }
  }

  void setupSessionData({Session currentSession}) {
    this.currentSession = currentSession;
    _currentSessionData = new SessionData(sessionInstance: currentSession);
  }

  void restoreSessionData({
    SessionDataPartial partialSessionData,
    Clock aClock = const Clock(),
  }) {
    this.currentSession = partialSessionData;
    _currentSessionData = partialSessionData;
    // find the last activity recorded as completed
    // for (int i = 0; i < _currentSessionData.activities.length; ++i) {
    //   if (_currentSessionData.activityCompleted[i]) {
    //     _currentActivity++;
    //   }
    // }
    _currentActivity = lastActivityLabelledCompleted() + 1;

    _secondsOffset =
        (aClock.now().difference(_currentSessionData.getDateTime())).inSeconds;
    _seconds = _secondsOffset;
    if (!_currentSessionData.validInterval(_currentActivity)) {
      // if the restored session was actually finished finish the session
      finishSession();
    } else {
      // restore the session mid-session
      String partialXML = '';
      // how much time has passed between the start of the session and now
      // _secondsOffset =
      //     (aClock.now().difference(_currentSessionData.getDateTime()))
      //         .inSeconds;
      // _seconds = _secondsOffset;

      partialXML = updateActivitiesCompleted(false);
      if (!_currentSessionData.validInterval(_currentActivity)) {
        // if all activities have passe their finish times finish the session
        _currentActivity = _currentSessionData.activities.length - 1;

        finishSession();
      } else {
        // else there are some activities yet to be finished to restart the session
        restartSession(partialXML);
      }
    }
  }

  String updateActivitiesCompleted(bool speak) {
    String partialXML = "";
    while (currentSession.validInterval(_currentActivity) &&
        currentSession.hasActivityFinished(
            _currentActivity, _seconds.seconds)) {
      // mark past activities as completed and write their data
      _currentSessionData.setActivityCompleted(
          index: _currentActivity,
          activityDuration: _seconds.seconds - _activityStart);
      partialXML += _currentSessionData.recordActivityCompletedXML(
          index: _currentActivity);
      _activityStart = elapsedTime();
      _currentActivity += 1;
      if (speak && currentSession.validInterval(_currentActivity)) {
        speachFunction("Start " +
            currentSession.activities[_currentActivity].activityName);
      }
    }
    return partialXML;
  }

  void togglePause() {
    if (state == SessionState.paused) {
      setState(SessionState.running);
      _stopwatch.start();
      _timer = newTimer();
    } else if (state == SessionState.running) {
      setState(SessionState.paused);
      _stopwatch.stop();
      _timer.cancel();
    }
  }

  Future<void> setupCurrentSessionFile() async {
    if (currentSessionFile == null) {
      String path = "";
      if (!kIsWeb) {
        path = (await getApplicationDocumentsDirectory()).path.toString();
      }
      Directory directory = fileSystem.directory(path);
      currentSessionFile = directory.childFile('currentSession.xml');
      currentSessionFile.create(recursive: true);
    }
  }

  Future<void> writeToCurrentSessionFile(String writeString) async {
    if (currentSessionFile != null) {
      await currentSessionFile.writeAsString(writeString);
    }
  }

  Future<void> appendToCurrentSessionFile(String writeString) async {
    if (currentSessionFile != null) {
      await currentSessionFile.writeAsString(writeString,
          mode: FileMode.append);
    }
  }

  Future<void> deleteCurrentSessionFile() async {
    if (currentSessionFile != null) {
      await currentSessionFile.delete();
    }
  }

  void startSession() async {
    if (state == SessionState.initial) {
      _currentSessionData.setDateTime(clock.now());
      //final directory = await getApplicationDocumentsDirectory();
      //currentSessionFile = File('${directory.path}/currentSession.xml');
      await setupCurrentSessionFile();
      await writeToCurrentSessionFile(_currentSessionData.headerXML());

      setState(SessionState.running);
      _stopwatch.start();
      _timer = newTimer();
      _activityStart = 0.seconds;
      speachFunction("Start");
    }
  }

  void restartSession(String partialXML) async {
    _stopwatch.start();
    _timer = newTimer();
    setState(SessionState.running);
    if (_currentActivity == 0) {
      _activityStart = 0.seconds;
    } else if (_currentSessionData.validInterval(_currentActivity)) {
      _activityStart =
          _currentSessionData.activityEndTime[_currentActivity - 1];
    }
    await setupCurrentSessionFile();
    await appendToCurrentSessionFile(partialXML);
  }

  void stopSession() {
    setState(SessionState.finished);

    _timer.cancel();
    setControlPanelSeconds(_seconds);
  }

  void finishSession() async {
    speachFunction("You have finished this session");
    if (state == SessionState.running || state == SessionState.paused) {
      setState(SessionState.finished);

      _stopwatch.stop();
      _timer.cancel();
    }
    _storeSessionFunction(_currentSessionData);

    await deleteCurrentSessionFile();
    await writeSessionData();

    _currentActivity = 0;
    _activityStart = 0.seconds;
    _seconds = 0;
    _secondsOffset = 0;
    _controlPanelSeconds = 0;
  }

  Future<void> writeSessionData() async {
    String path = "";
    if (!kIsWeb) {
      path = (await getApplicationDocumentsDirectory()).path.toString();
    }
    Directory directory = fileSystem.directory(path);
    // currentSessionFile = directory.childFile('currentSession.xml');
    // currentSessionFile.create(recursive: true);

    String filename = //'${directory.path}'+
        //'/data/' +
        clock.now().toString().replaceAll(new RegExp(r'[^0-9]'), '') + ".xml";
    final File outputFile =
        directory.childDirectory('data').childFile(filename);
    outputFile.create(recursive: true);
    //await File(filename).writeAsString(
    await outputFile.writeAsString(
        '<session_datas>\n' + _currentSessionData.toXML() + '</session_datas>');
  }

  Timer newTimer() {
    return Timer.periodic(10.milliseconds, (Timer timer) {
      //setState(() {
      _seconds = _secondsOffset + _stopwatch.elapsedMilliseconds ~/ 1000;
      setControlPanelSeconds(_seconds);
      String partialXML = "";
      partialXML = updateActivitiesCompleted(true);
      if (partialXML != "") appendToCurrentSessionFile(partialXML);
      if (!_currentSessionData.validInterval(_currentActivity)) {
        _currentActivity = _currentSessionData.activities.length - 1;

        finishSession();
      }
    });
  }

  int lastActivityLabelledCompleted() {
    for (int i = 0; i < _currentSessionData.activities.length; ++i) {
      if (!_currentSessionData.activityCompleted[i]) {
        return i - 1;
      }
    }
    return _currentSessionData.activities.length;
  }

  Duration elapsedTime() {
    return ((_stopwatch.elapsedMilliseconds ~/ 1000) + _secondsOffset).seconds;
  }
}
