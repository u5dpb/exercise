import 'package:exercise/basic_data_classes/basic_sessions.dart';
import 'package:exercise/model/failure.dart';
import 'package:exercise/io/file_handling_functions.dart';
import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/basic_data_classes/session_data_partial.dart';
import 'package:flutter/material.dart';
import 'package:collection/collection.dart';

enum NotifierState {
  initial,
  loading,
  loaded,
  error,
  partialFound,
  partialLoading,
  partialLoaded,
  partialError
}

class AppData extends ChangeNotifier {
  final FileHandlingFunctions fileHandlingFunctions;
  // Function runSession;
  AppData({
    @required this.fileHandlingFunctions,
    //  this.runSession,
  }) {
    //runSession ??= _runSession;
    BasicSessions();
    _sessionList.add(BasicSessions.shortRun);
    _sessionList.add(BasicSessions.shortCircuits);
    _sessionList.add(BasicSessions.miniCircuits);
    _sessionList.add(BasicSessions.resistanceBandCircuits);

    loadSessionData();
  }

  NotifierState _state = NotifierState.initial;
  NotifierState get state => _state;

  void _setState(NotifierState state) {
    _state = state;
    notifyListeners();
  }

  Failure _failure;
  Failure get failure => _failure;
  void _setFailure(Failure failure) {
    _failure = failure;
    notifyListeners();
  }

  List<Session> _sessionList = [];
  UnmodifiableListView<Session> get sessionList {
    return UnmodifiableListView(_sessionList);
  }

  List<SessionData> _sessionDataList = [];
  UnmodifiableListView<SessionData> get sessionDataList {
    return UnmodifiableListView(_sessionDataList);
  }

  SessionDataPartial _sessionDataPartial;
  SessionDataPartial get sessionDataPartial => _sessionDataPartial;

  Session currentSession;

  // RunSession _runSession(
  //     {@required BuildContext context, @required restoreSession}) {
  //   Provider.of<RunSessionTimer>(context)
  //       .initialise(context: context, restoreSession: restoreSession);
  //   return RunSession(
  //     speechDriver: Speak(),
  //   );
  // }

  void loadSessionData() async {
    _setState(NotifierState.loading);

    try {
      final tempSessionDataList = await fileHandlingFunctions.loadData();
      addSessionDatas(tempSessionDataList);
    } on Failure catch (failure) {
      _setFailure(failure);
      _setState(NotifierState.error);
      return;
    }
    bool partialExists = await fileHandlingFunctions.partialSessionExists();
    if (partialExists) {
      _setState(NotifierState.partialFound);
    } else {
      _setState(NotifierState.loaded);
    }
  }

  void loadPartialData() async {
    _setState(NotifierState.partialLoading);
    try {
      _sessionDataPartial = await fileHandlingFunctions.loadPartialSession(
          filename: "currentSession.xml");
      _setState(NotifierState.partialLoaded);
    } on Failure catch (failure) {
      _setFailure(failure);
      _setState(NotifierState.partialError);
    }
  }

  void addSessionData(SessionData sessionData) {
    _sessionDataList.insert(0, sessionData);
  }

  void addSessionDatas(List<SessionData> sessionDataList) {
    _sessionDataList += sessionDataList;
  }
}
