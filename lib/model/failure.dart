class Failure {
  final String _description;
  Failure(this._description);

  @override
  toString() => _description;
}
