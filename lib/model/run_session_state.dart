import 'package:flutter/cupertino.dart';

enum SessionState { initial, running, paused, finished }

class RunSessionState extends ChangeNotifier {
  SessionState _state = SessionState.initial;
  SessionState get state => _state;
  void setState(SessionState state) {
    _state = state;
    notifyListeners();
  }

  int _controlPanelSeconds = 0;
  int get controlPanelSeconds => _controlPanelSeconds;
  void setControlPanelSeconds(int controlPanelSeconds) {
    _controlPanelSeconds = controlPanelSeconds;
    notifyListeners();
  }
}
