import 'package:flutter_tts/flutter_tts.dart';
import 'package:flutter_beep/flutter_beep.dart';

class Speak {
  FlutterTts _flutterTts;
  Speak() {
    setUpTTS();
  }
  void speak(String text) async {
    await FlutterBeep.beep(false);
    await _flutterTts.speak(text);
  }

  void setUpTTS() async {
    _flutterTts = FlutterTts();
    await _flutterTts.setVolume(1.0);
    await _flutterTts.setSpeechRate(0.5);
    await _flutterTts.setPitch(0.5);
  }
}
