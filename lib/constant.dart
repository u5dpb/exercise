// global varianbles
import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

const Color kLightPrimaryColor = Color(0xFFB3E5FC);
const Color kDarkPrimaryColor = Color(0xFF0288D1);
const Color kPrimaryColor = Color(0xFF03A9F4);
const Color kAccentColor = Color(0xFF9E9E9E);
const Color kPrimaryText = Color(0xFF212121);
const Color kSecondaryText = Color(0xFF757575);
const Color kDividerColor = Color(0xFFBDBDBD);

const kMainScreenText = TextStyle(color: kPrimaryText, fontSize: 30);

const kTitleText = TextStyle(color: Colors.white, fontSize: 30);

const double kTitleAppBarHeight = 56.0;

const kDialogueText = TextStyle(fontSize: 25);
const kDialogueButton = TextStyle(fontSize: 25, color: kDarkPrimaryColor);
const kDialogueTitle = TextStyle(fontSize: 30);

var kAlertStyle = AlertStyle(
    animationType: AnimationType.fromTop,
    isCloseButton: false,
    isOverlayTapDismiss: false,
    descStyle: TextStyle(fontWeight: FontWeight.bold),
    animationDuration: Duration(milliseconds: 400),
    alertBorder: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(0.0),
      side: BorderSide(
        color: Colors.grey,
      ),
    ),
    titleStyle: TextStyle(
      color: Colors.red,
    ),
    constraints: BoxConstraints.expand(width: 300),
    //First to chars "55" represents transparency of color
    overlayColor: Color(0x55000000),
    alertElevation: 0,
    alertAlignment: Alignment.topCenter);
