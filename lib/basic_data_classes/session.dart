import 'package:exercise/basic_data_classes/activity.dart';
//import 'package:provider/provider.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
//import 'package:duration/duration.dart';
import 'package:xml/xml.dart';

// TODO throw some exceptions if xml information is incomplete

class Session {
  String sessionName;
  String sessionDescription;
  int numIntervals = 0;

  Session({this.sessionName, this.sessionDescription});

  List<Activity> _activities = [];
  List<Duration> _activityEndTime = [];

  UnmodifiableListView<Activity> get activities {
    return UnmodifiableListView(_activities);
  }

  UnmodifiableListView<Duration> get activityEndTime {
    return UnmodifiableListView(_activityEndTime);
  }

  bool validInterval(int interval) => interval < numIntervals;

  void addActivity(Activity newActivity) {
    if (_activityEndTime.length > 0)
      _activityEndTime.add(_activityEndTime[_activityEndTime.length - 1] +
          newActivity.activityTime);
    else
      _activityEndTime.add(newActivity.activityTime);
    _activities.add(newActivity);
    numIntervals++;
    //notifyListeners();
  }

  void setActivityEndTime(int index, Duration endTime) =>
      _activityEndTime[index] = endTime;

  bool hasActivityFinished(int index, Duration currentTime) =>
      currentTime > _activityEndTime[index];

  void deleteActivity(int refValue) {
    _activities.removeAt(refValue);
    numIntervals--;
    //notifyListeners();
  }

  String toXML() {
    String returnString = "";
    returnString += "<session name=\"" +
        sessionName +
        "\" description=\"" +
        sessionDescription +
        "\" numIntervals=\"" +
        numIntervals.toString() +
        "\" >\n";
    int intervalNum = 1;
    for (Activity activity in _activities) {
      returnString += "<interval number=\"" + intervalNum.toString() + "\" >";
      returnString += activity.toXML();
      returnString += "</interval>\n";
      intervalNum++;
    }
    returnString += "</session>\n";
    return returnString;
  }

  static Session readSession(XmlElement xmlElement) {
    var sessionMap = {
      xmlElement.getAttribute('name'): [
        xmlElement.getAttribute('description'),
        xmlElement.getAttribute('numIntervals'),
        xmlElement.findElements('interval').map((node2) =>
            ({node2.getAttribute('number'): node2.findElements('activity')}))
      ]
    };
    Session thisSession = Session(
        sessionName: sessionMap.keys.first.toString(),
        sessionDescription: sessionMap.values.first[0]);
    int intervalCheck = 0;
    for (var interval in sessionMap.values.first[2]) {
      var activity = interval.values.first.first;
      intervalCheck++;
      thisSession.addActivity(Activity.readActivity(activity));
    }
    if (intervalCheck != int.parse(sessionMap.values.first[1]))
      print(
          "static Session readSession(XmlElement xmlElement) mismatch\nExpected " +
              sessionMap.values.first[1] +
              " intervals but found " +
              intervalCheck.toString());

    return thisSession;
  }

  static List<Session> readSessions(String sample) {
    List<Session> sessionList = [];
    for (var sessionXML in XmlDocument.parse(sample)
        .getElement('sessions')
        .findElements('session')) {
      Session thisSession = Session.readSession(sessionXML);
      sessionList.add(thisSession);
    }
    return sessionList;
  }

  static String writeSessions(List<Session> sessionList) {
    String returnString = '';
    returnString = "<sessions>\n";
    for (Session s in sessionList) {
      returnString += s.toXML();
    }
    returnString += "<\/sessions>\n";
    return returnString;
  }
}
