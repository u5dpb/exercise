import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/basic_data_classes/activity.dart';
// import 'package:provider/provider.dart';
// import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
//import 'package:time/time.dart';
import 'package:duration/duration.dart';
//import 'package:xml/xml.dart';

class SessionDataPartial extends SessionData {
  bool incompleteSession = true;
  bool incompletePerformance = true;

  //SessionData getSessionData() => super;

  SessionDataPartial({@required Session sessionInstance})
      : super(sessionInstance: sessionInstance);

  static SessionDataPartial readIncomplete(String inputString) {
    RegExp expSessionData = RegExp(r'<session_data timedata="([^"]*)"');
    RegExp expSession = RegExp(
        r'<session name="([^"]*)" description="([^"]*)" numIntervals="([^"]*)" >');
    RegExp expInterval = RegExp(
        r'<interval number="([^"]*)" ><activity name="([^"]*)" description="([^"]*)" duration="([^"]*)"');
    RegExp expPerformanceInterval = RegExp(
        r'<performance_interval number="([^"]*)"[^<]*<duration time="([^"]*)"');
    //String match = 'none';
    String sessionDataTimedata = "";
    if (expSessionData.hasMatch(inputString)) {
      sessionDataTimedata =
          expSessionData.firstMatch(inputString).group(1).toString();
    }
    String sessionName = "";
    String sessionDesc = "";
    int sessionNumIntervals = -1;
    if (expSession.hasMatch(inputString)) {
      sessionName = expSession.firstMatch(inputString).group(1).toString();
      sessionDesc = expSession.firstMatch(inputString).group(2).toString();
      sessionNumIntervals =
          int.parse(expSession.firstMatch(inputString).group(3).toString());
    }
    //print('Match: ' + sessionName + " " + sessionDesc);
    Session session =
        Session(sessionName: sessionName, sessionDescription: sessionDesc);

    int activityCount = 0;
    if (expInterval.hasMatch(inputString)) {
      for (var match in expInterval.allMatches(inputString)) {
        //String number = match.group(1);
        String activityName = match.group(2);
        String activityDesc = match.group(3);
        String activityDuration = match.group(4);
        session.addActivity(Activity(
            activityName: activityName,
            activityDescription: activityDesc,
            activityTime: parseDuration(activityDuration)));
        activityCount++;
      }
    }

    SessionDataPartial sessionData =
        SessionDataPartial(sessionInstance: session);
    sessionData.setDateTime(DateTime.parse(sessionDataTimedata));
    if (activityCount == sessionNumIntervals)
      sessionData.incompleteSession = false;
    if (expPerformanceInterval.hasMatch(inputString)) {
      int intervalNumber = 1;
      for (var match in expPerformanceInterval.allMatches(inputString)) {
        String number = match.group(1);
        String duration = match.group(2);
        if (int.parse(number) == intervalNumber) {
          sessionData.setActivityCompleted(
              index: intervalNumber - 1,
              activityDuration: parseDuration(duration));
          //print('Interval complete ' + number + ' ' + duration);
        }
        intervalNumber++;
      }
      sessionData.incompletePerformance = false;
      for (bool completed in sessionData.activityCompleted) {
        if (!sessionData.incompletePerformance) {
          sessionData.incompletePerformance = !completed;
        }
      }
    }
    return sessionData;
  }
}
