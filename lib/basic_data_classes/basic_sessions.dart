import 'package:exercise/basic_data_classes/activity.dart';
import 'package:exercise/basic_data_classes/session.dart';
import 'package:time/time.dart';

class BasicSessions {
  static Session shortRun;
  static Session shortCircuits;
  static Session miniCircuits;
  static Session resistanceBandCircuits;
  BasicSessions() {
    shortRun =
        Session(sessionName: 'Short Run', sessionDescription: '30 minute run');
    shortRun.addActivity(Activity(
        activityName: 'Warm up',
        activityDescription: 'Walk',
        activityTime: 5.minutes,
        activityMp3File: 'assest/mp3/warmup.mp3'));
    shortRun.addActivity(Activity(
        activityName: 'Run',
        activityDescription: 'Run',
        activityTime: 30.minutes,
        activityMp3File: 'assets/mp3/run.mp3'));
    shortRun.addActivity(Activity(
        activityName: 'Warm down',
        activityDescription: 'Walk',
        activityTime: 5.minutes,
        activityMp3File: 'assets/mp3/warmdown.mp3'));

    shortCircuits = Session(
        sessionName: 'Circuit Training 1',
        sessionDescription: 'Daily Circuits');
    shortCircuits.addActivity(Activity(
        activityName: 'Squats',
        activityDescription: '',
        activityTime: 1.minutes,
        activityMp3File: 'assets/mp3/squats.mp3'));
    shortCircuits.addActivity(Activity(
        activityName: 'Lunges',
        activityDescription: '',
        activityTime: 1.minutes,
        activityMp3File: 'assets/mp3/lunges.mp3'));
    shortCircuits.addActivity(Activity(
        activityName: 'Push-ups',
        activityDescription: '',
        activityTime: 1.minutes,
        activityMp3File: 'assets/mp3/pushups.mp3'));
    shortCircuits.addActivity(Activity(
        activityName: 'Dead Lift',
        activityDescription: 'One Leg Dead Lifts',
        activityTime: 1.minutes,
        activityMp3File: 'assets/mp3/deadlift.mp3'));
    shortCircuits.addActivity(Activity(
        activityName: 'Knee Lifts',
        activityDescription: 'Wall Sit Knee Lifts',
        activityTime: 2.minutes,
        activityMp3File: 'assets/mp3/kneelifts.mp3'));
    shortCircuits.addActivity(Activity(
        activityName: 'Dips',
        activityDescription: '',
        activityTime: 1.minutes,
        activityMp3File: 'assets/mp3/dips.mp3'));
    shortCircuits.addActivity(Activity(
        activityName: 'Burpees',
        activityDescription: '',
        activityTime: 1.minutes,
        activityMp3File: 'assets/mp3/burpees.mp3'));
    shortCircuits.addActivity(Activity(
        activityName: 'Push Ups',
        activityDescription: '',
        activityTime: 1.minutes,
        activityMp3File: 'assets/mp3/pushups.mp3'));
    shortCircuits.addActivity(Activity(
        activityName: 'Bridge',
        activityDescription: '',
        activityTime: 1.minutes,
        activityMp3File: 'assets/mp3/bridge.mp3'));
    miniCircuits = Session(
        sessionName: 'Mini Circuits (Test)',
        sessionDescription: 'Very Short Daily Circuits');
    miniCircuits.addActivity(Activity(
        activityName: 'Squats',
        activityDescription: '',
        activityTime: 10.seconds,
        activityMp3File: 'assets/mp3/squats.mp3'));
    miniCircuits.addActivity(Activity(
        activityName: 'Lunges',
        activityDescription: '',
        activityTime: 10.seconds,
        activityMp3File: 'assets/mp3/lunges.mp3'));
    miniCircuits.addActivity(Activity(
        activityName: 'Push-ups',
        activityDescription: '',
        activityTime: 10.seconds,
        activityMp3File: 'assets/mp3/pushups.mp3'));
    miniCircuits.addActivity(Activity(
        activityName: 'Dead Lift',
        activityDescription: 'One Leg Dead Lifts',
        activityTime: 10.seconds,
        activityMp3File: 'assets/mp3/deadlift.mp3'));
    miniCircuits.addActivity(Activity(
        activityName: 'Knee Lifts',
        activityDescription: 'Wall Sit Knee Lifts',
        activityTime: 20.seconds,
        activityMp3File: 'assets/mp3/kneelifts.mp3'));
    miniCircuits.addActivity(Activity(
        activityName: 'Dips',
        activityDescription: '',
        activityTime: 10.seconds,
        activityMp3File: 'assets/mp3/dips.mp3'));
    miniCircuits.addActivity(Activity(
        activityName: 'Burpees',
        activityDescription: '',
        activityTime: 10.seconds,
        activityMp3File: 'assets/mp3/burpees.mp3'));
    miniCircuits.addActivity(Activity(
        activityName: 'Push Ups',
        activityDescription: '',
        activityTime: 10.seconds,
        activityMp3File: 'assets/mp3/pushups.mp3'));
    miniCircuits.addActivity(Activity(
        activityName: 'Bridge',
        activityDescription: '',
        activityTime: 10.seconds,
        activityMp3File: 'assets/mp3/bridge.mp3'));
    resistanceBandCircuits = Session(
        sessionName: 'Resistance band Circuits',
        sessionDescription: 'Daily Circuits using a Resistance Band');
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Bicep Curl',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Frontal Lateral',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Shoulder Press',
      activityDescription: 'Right Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Shoulder Press',
      activityDescription: 'Left Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Tricep Extension',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rows',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'V-Pec Up',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Lunge Lateral Raise',
      activityDescription: 'Right Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Lunge Lateral Raise',
      activityDescription: 'Left Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));

    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Dead Lift',
      activityDescription: '',
      activityTime: 60.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Abductor Rows',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: 'Move to the Mat',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Leg Raise',
      activityDescription: 'Right Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Leg Raise',
      activityDescription: 'Left Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 20.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Leg Press',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 20.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Ab Rolls',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: 'Up off the Mat',
      activityTime: 30.seconds,
    ));

    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Windmill',
      activityDescription: '',
      activityTime: 60.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Side Obliques',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 20.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Bicep Curl',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Frontal Lateral',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Shoulder Press',
      activityDescription: 'Right Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Shoulder Press',
      activityDescription: 'Left Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Tricep Extension',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rows',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'V-Pec Up',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Lunge Lateral Raise',
      activityDescription: 'Right Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Lunge Lateral Raise',
      activityDescription: 'Left Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Dead Lift',
      activityDescription: '',
      activityTime: 60.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Abductor Rows',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: 'Move to the Mat',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Leg Raise',
      activityDescription: 'Right Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Leg Raise',
      activityDescription: 'Left Side',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 20.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Leg Press',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 20.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Ab Rolls',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: 'Up off the Mat',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Windmill',
      activityDescription: '',
      activityTime: 60.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 10.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Side Obliques',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 20.seconds,
    ));

    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Knee Highs',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Lunge Switch',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Rest',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Crouch Explode',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
    resistanceBandCircuits.addActivity(Activity(
      activityName: 'Stretch',
      activityDescription: '',
      activityTime: 30.seconds,
    ));
  }
}
