//import 'package:time/time.dart';
import 'package:xml/xml.dart';
import 'package:duration/duration.dart';

class Activity {
  String activityName;
  String activityDescription;
  Duration activityTime;
  String activityMp3File;

  static List<Activity> readActivities(String xmlString) {
    List<Activity> returnList;
    var root = XmlDocument.parse(xmlString).getElement('activities');
    returnList = root
        .findElements('activity')
        .map<Activity>((e) => Activity.readActivity(e))
        .toList();
    return returnList;
  }

  static String writeActivities(List<Activity> activityList) {
    String returnString = '';
    returnString = "<activities>\n";
    for (Activity a in activityList) {
      returnString += a.toXML() + '\n';
    }
    returnString += "<\/activities>\n";
    return returnString;
  }

  Activity(
      {this.activityName,
      this.activityDescription,
      this.activityTime,
      this.activityMp3File});
  Activity._(String name, String description, String duration, String mp3File) {
    this.activityName = name;
    this.activityDescription = description;
    this.activityTime = parseDuration(duration);
    this.activityMp3File = mp3File;
  }
  factory Activity.fromElement(XmlElement activityElement) {
    return Activity._(
      activityElement.getAttribute('name'),
      activityElement.getAttribute('description'),
      activityElement.getAttribute('duration'),
      activityElement.getAttribute('mp3file'),
    );
  }

  static Activity readActivity(XmlElement xmlElement) {
    return Activity(
        activityName: xmlElement.getAttribute('name'),
        activityDescription: xmlElement.getAttribute('description'),
        activityTime: parseDuration(xmlElement.getAttribute('duration')),
        activityMp3File: xmlElement.getAttribute('mp3file'));
  }

  String activityTimeString() {
    return prettyDuration(activityTime, abbreviated: true);
  }

  String toXML() {
    String returnString = '';
    returnString += '<activity name="' +
        activityName +
        '" description="' +
        activityDescription +
        '" duration="' +
        prettyDuration(activityTime, abbreviated: true) +
        '" mp3file="' +
        //activityMp3File +
        '" />';
    return returnString;
  }
}
