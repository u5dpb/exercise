import 'dart:io';

import 'package:exercise/basic_data_classes/activity.dart';
import 'package:path_provider/path_provider.dart';
//import 'package:provider/provider.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:exercise/basic_data_classes/session.dart';
import 'package:time/time.dart';
import 'package:duration/duration.dart';
import 'package:xml/xml.dart';

// TODO throw some exceptions if xml information is incomplete

class SessionData extends Session {
  List<bool> _activityCompleted = [];
  List<Duration> _activityTime = [];

  DateTime _dateTime;

  DateTime getDateTime() {
    return _dateTime;
  }

  SessionData({@required Session sessionInstance})
      : super(
            sessionName: sessionInstance.sessionName,
            sessionDescription: sessionInstance.sessionDescription) {
    for (Activity activity in sessionInstance.activities) {
      super.addActivity(activity);
    }
    this._activityCompleted = List.filled(super.activities.length, false);
    this._activityTime = List.filled(super.activities.length, 0.seconds);
  }

  void setDateTime(DateTime dt) => _dateTime = dt;

  void setActivityCompleted(
      {@required int index, @required Duration activityDuration}) {
    this._activityCompleted[index] = true;
    this._activityTime[index] =
        this.activities[index].activityTime; //activityDuration;
  }

  String recordActivityCompletedXML({@required index}) {
    return "<performance_interval number=\"" +
        (index + 1).toString() +
        "\" >\n<duration time=\"" +
        prettyDuration(activityTime[index], abbreviated: true) +
        "\" />\n</performance_interval>\n";
  }

  UnmodifiableListView<bool> get activityCompleted {
    return UnmodifiableListView(_activityCompleted);
  }

  UnmodifiableListView<Duration> get activityTime {
    return UnmodifiableListView(_activityTime);
  }

  Duration sessionTotalTime() {
    Duration totalTime = 0.seconds;
    for (Duration time in _activityTime) {
      totalTime += time;
    }
    return totalTime;
  }

  String getDate() =>
      _dateTime.day.toString().padLeft(2, '0') +
      '/' +
      _dateTime.month.toString().padLeft(2, '0') +
      '/' +
      _dateTime.year.toString();

  String sessionSummary() {
    String summaryString = super.sessionName;

    summaryString +=
        ' Total Time: ' + sessionTotalTime().inSeconds.toString() + ' seconds';
    return summaryString;
  }

  static String writeSessionDatas(List<SessionData> sessionDatas) {
    String returnString = '<session_datas>\n';
    for (var sessionData in sessionDatas) returnString += sessionData.toXML();
    returnString += '</session_datas>\n';
    return returnString;
  }

  String headerXML() {
    return "<session_data timedata=\"" +
        _dateTime.toString() +
        "\">\n" +
        super.toXML();
  }

  String footerXML() {
    return "</session_data>\n";
  }

  String toXML() {
    String returnString =
        "<session_data timedata=\"" + _dateTime.toString() + "\">\n";
    returnString += super.toXML();
    int intervalNum = 1;
    for (Duration duration in _activityTime) {
      if (_activityCompleted[intervalNum - 1]) {
        returnString += "<performance_interval number=\"" +
            intervalNum.toString() +
            "\" >\n";
        returnString += "<duration time=\"" +
            prettyDuration(duration, abbreviated: true) +
            "\" />\n";
        returnString += "</performance_interval>\n";
      }
      intervalNum++;
    }
    returnString += "</session_data>\n";
    return returnString;
  }

  static List<SessionData> readSessionDatas(String xmlString) {
    List<SessionData> sessionDataList = [];
    for (var sessionDataXML in XmlDocument.parse(xmlString)
        .getElement('session_datas')
        .findElements('session_data')) {
      sessionDataList.add(SessionData.readSessionData(sessionDataXML));
    }
    return sessionDataList;
  }

  static SessionData readSessionData(XmlElement xmlElement) {
    var sessionDataMap = {
      xmlElement.getAttribute('timedata'): [
        xmlElement.getElement('session'),
        xmlElement.findAllElements('performance_interval').toList()
      ]
    };

    SessionData sessionDataInstance = SessionData(
        sessionInstance: Session.readSession(sessionDataMap.values.first[0]));
    sessionDataInstance
        .setDateTime(DateTime.parse(sessionDataMap.keys.first.toString()));
    for (var interval in sessionDataMap.values.first[1]) {
      sessionDataInstance.setActivityCompleted(
          index: int.parse(interval.getAttribute('number')) - 1,
          activityDuration: parseDuration(
              interval.getElement('duration').getAttribute('time')));
    }
    return sessionDataInstance;
  }

  static Future<List<SessionData>> readSessionDataDir(
      {Function getApplicationDirectory = getApplicationDocumentsDirectory,
      Function directoryList,
      Function readFileAsString,
      Function readSessionDatas}) async {
    if (directoryList == null) {
      directoryList = (dirName) {
        return Directory(dirName).list();
      };
    }
    if (readFileAsString == null) {
      readFileAsString = (fileSystemEntitiy) async {
        return File(fileSystemEntitiy.path).readAsString();
      };
    }
    if (readSessionDatas == null) {
      readSessionDatas = (dataString) {
        return SessionData.readSessionDatas(dataString);
      };
    }
    // TODO add an "onFail" function for when there's a problem loading
    // TODO add an "onIncomplete" function for when the file is incomplete
    List<SessionData> returnData = [];
    final directory = await getApplicationDirectory();
    final dirName = '${directory.path}/data';
    Stream<FileSystemEntity> dirs = directoryList(dirName);
    await for (FileSystemEntity file in dirs) {
      // for each file in the data directory
      // load the data
      String dataString = await readFileAsString(file);
      //print(fileData);
      returnData += readSessionDatas(dataString);
    }
    return returnData;
  }
}
