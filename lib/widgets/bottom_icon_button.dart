import 'package:flutter/material.dart';
import 'package:exercise/constant.dart';

class BottomIconButton extends StatelessWidget {
  final Function onPressed;
  final IconData buttonIcon;
  final Key key;

  BottomIconButton({this.onPressed, this.buttonIcon, this.key});

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: const ShapeDecoration(
        color: Colors.white,
        shape: CircleBorder(),
      ),
      child: IconButton(
        iconSize: 30,
        icon: Icon(buttonIcon),
        color: kPrimaryColor,
        onPressed: onPressed,
      ),
    );
  }
}
