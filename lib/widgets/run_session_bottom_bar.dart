import 'package:exercise/screens/main_screen.dart';
import 'package:exercise/widgets/bottom_icon_button.dart';
import 'package:flutter/material.dart';
import 'package:exercise/constant.dart';

class RunSessionBottomBar extends StatelessWidget {
  const RunSessionBottomBar({
    Key key,
    @required bool sessionInitial,
    @required bool sessionFinished,
  })  : _sessionInitial = sessionInitial,
        _sessionFinished = sessionFinished,
        super(key: key);

  final bool _sessionInitial;
  final bool _sessionFinished;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: kPrimaryColor,
      child: Padding(
        key: Key('runSessionBottomBarPadding'),
        padding: EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            if (_sessionInitial)
              BottomIconButton(
                key: Key('backButton'),
                buttonIcon: Icons.arrow_back,
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            if (_sessionFinished)
              BottomIconButton(
                key: Key('homeButton'),
                buttonIcon: Icons.home,
                onPressed: () {
                  Navigator.pushReplacementNamed(context, MainScreen.id);
                },
              ),
          ],
        ),
      ),
    );
  }
}
