import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/widgets/list_tiles/list_card.dart';
import 'package:exercise/widgets/list_tiles/list_tile_functions.dart';
import 'package:flutter/material.dart';

class SessionTile extends StatelessWidget {
  final Session session;
  final Function onTap;
  final Key key;

  SessionTile({@required this.session, this.onTap, this.key});

  @override
  Widget build(BuildContext context) {
    return ListCard(
        tile: ListTile(
      title: TileText(session.sessionName),
      subtitle: TileSmallText(session.sessionDescription),
      contentPadding: EdgeInsets.all(10),
      onTap: onTap,
    ));
  }
}
