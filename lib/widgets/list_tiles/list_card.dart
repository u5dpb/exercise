import 'package:flutter/material.dart';

import '../../constant.dart';

class ListCard extends StatelessWidget {
  final ListTile tile;
  ListCard({@required this.tile});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Card(
          color: kLightPrimaryColor,
          margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0), side: BorderSide.none),
          child: tile),
    );
  }
}
