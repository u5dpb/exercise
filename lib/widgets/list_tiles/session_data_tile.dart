import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/widgets/list_tiles/list_card.dart';
import 'package:exercise/widgets/list_tiles/list_tile_functions.dart';
import 'package:flutter/material.dart';

class SessionDataTile extends StatelessWidget {
  final SessionData sessionData;
  final Function onTap;
  final Key key;

  SessionDataTile({@required this.sessionData, this.onTap, this.key});

  @override
  Widget build(BuildContext context) {
    return ListCard(
        tile: ListTile(
      title: TileSmallText(
        dateString(sessionData.getDateTime()) + '  ' + sessionData.sessionName,
      ),
      subtitle: Padding(
        padding: EdgeInsets.only(top: 5),
        child: TileSmallText(timeString(sessionData.getDateTime()) +
            '   Total time: ' +
            sessionData.sessionTotalTime().inMinutes.toString() +
            ' minutes'),
      ),
      contentPadding: EdgeInsets.all(10),
      onTap: onTap,
    ));
  }
}
