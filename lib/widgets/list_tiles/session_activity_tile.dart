import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/widgets/list_tiles/activity_list_tile.dart';
import 'package:flutter/material.dart';

class SessionActivityTile extends StatelessWidget {
  final int index;
  final Session currentSession;
  final Key key;

  SessionActivityTile({this.index, this.currentSession})
      : key = Key("SessionActivityTile_" + index.toString());

  @override
  Widget build(BuildContext context) {
    return ActivityListTile(
      activity: currentSession.activities[index],
      showDescription: true,
      endTime: currentSession.activityEndTime[index],
      duration: currentSession.activities[index].activityTime,
    );
  }
}
