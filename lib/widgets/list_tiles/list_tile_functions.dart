import 'package:flutter/material.dart';

class TileText extends StatelessWidget {
  final String text;
  const TileText(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text, style: TextStyle(fontSize: 30));
  }
}

class TileSmallText extends StatelessWidget {
  final String text;
  const TileSmallText(this.text, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text, style: TextStyle(fontSize: 20));
  }
}

String dateString(DateTime dateTime) {
  return dateTime.day.toString().padLeft(2, '0') +
      '/' +
      dateTime.month.toString().padLeft(2, '0') +
      '/' +
      dateTime.year.toString();
}

String timeString(DateTime dateTime) {
  return dateTime.hour.toString().padLeft(2, '0') +
      ':' +
      dateTime.minute.toString().padLeft(2, '0');
}
