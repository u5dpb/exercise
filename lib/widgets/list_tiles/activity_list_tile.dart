import 'package:duration/duration.dart';
import 'package:exercise/basic_data_classes/activity.dart';
import 'package:exercise/widgets/list_tiles/list_card.dart';
import 'package:exercise/widgets/list_tiles/list_tile_functions.dart';
import 'package:flutter/material.dart';

class ActivityListTile extends StatelessWidget {
  final Activity activity;
  final Duration duration;
  final bool showDescription;
  final Duration endTime;
  final Key key;

  ActivityListTile(
      {@required this.activity,
      this.duration,
      this.showDescription = false,
      this.endTime,
      this.key});

  @override
  Widget build(BuildContext context) {
    return ListCard(
      tile: ListTile(
        title: TileSmallText(activity.activityName),
        subtitle: Padding(
          padding: EdgeInsets.only(top: 5),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            if (showDescription && activity.activityDescription.length > 0)
              TileSmallText(activity.activityDescription),
            if (duration != null)
              TileSmallText('Duration: ' + prettyDuration(duration)),
          ]),
        ),
        trailing: endTime != null
            ? TileSmallText(
                'Finish: ' + prettyDuration(endTime, abbreviated: true))
            : null,
        contentPadding: EdgeInsets.all(10),
        onTap: () {},
      ),
    );
  }
}
