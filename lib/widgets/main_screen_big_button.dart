import 'package:auto_size_text/auto_size_text.dart';
import 'package:exercise/constant.dart';
import 'package:flutter/material.dart';

class MainScreenBigButton extends StatelessWidget {
  final String buttonRoute;
  final String text;
  const MainScreenBigButton({
    Key key,
    this.buttonRoute,
    @required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: kLightPrimaryColor,
      child: Card(
        color: kDividerColor,
        margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20.0), side: BorderSide.none),
        child: TextButton(
          //key: Key('mainScreenChooseSession'),
          style: TextButton.styleFrom(textStyle: kMainScreenText),
          onPressed: () {
            Navigator.pushNamed(
              context,
              buttonRoute,
            );
          },
          child: Padding(
            padding: EdgeInsets.all(30),
            child: AutoSizeText(
              text,
              maxLines: 1,
              style: kMainScreenText,
            ),
          ),
        ),
      ),
    );
  }
}
