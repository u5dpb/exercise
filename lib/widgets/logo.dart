import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  const Logo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(50),
      child: Image(
        key: Key('mainScreenLogo'),
        image: AssetImage("assets/logo_2_full.png"),
      ),
    );
  }
}
