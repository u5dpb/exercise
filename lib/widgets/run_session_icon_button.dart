import 'package:flutter/material.dart';
import 'package:exercise/constant.dart';

class RunSessionIconButton extends StatelessWidget {
  final Function onPressed;
  final IconData buttonIcon;
  final Key key;

  RunSessionIconButton({this.onPressed, this.buttonIcon, this.key});

  @override
  Widget build(BuildContext context) {
    return Ink(
      decoration: const ShapeDecoration(
        color: kPrimaryColor,
        shape: CircleBorder(),
      ),
      child: IconButton(
        iconSize: 70,
        icon: Icon(
          buttonIcon,
          color: Colors.white,
        ),
        color: kPrimaryColor,
        onPressed: onPressed,
      ),
    );
  }
}
