import 'package:exercise/constant.dart';
import 'package:exercise/widgets/bottom_icon_button.dart';
import 'package:flutter/material.dart';

class CommonBottomAppBar extends StatelessWidget {
  const CommonBottomAppBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: kPrimaryColor,
      child: Padding(
        padding: EdgeInsets.all(5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            BottomIconButton(
              key: Key('backButton'),
              buttonIcon: Icons.arrow_back,
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
    );
  }
}
