import 'package:flutter/material.dart';
import 'package:exercise/constant.dart';

// TODO refactor as a class

class TitleAppBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => const Size.fromHeight(kTitleAppBarHeight);

  final String title;

  TitleAppBar({this.title});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: kPrimaryColor,
      automaticallyImplyLeading: false,
      title: Text(
        title,
        style: kTitleText,
      ),
    );
  }
}
