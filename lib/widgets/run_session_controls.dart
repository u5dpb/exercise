import 'package:exercise/widgets/run_session_icon_button.dart';
import 'package:flutter/material.dart';

class RunSessionControls extends StatelessWidget {
  final Function onPlay;
  final Function onPause;
  final Function onStop;
  final int seconds;

  RunSessionControls({this.onPlay, this.onPause, this.onStop, this.seconds});

  @override
  Widget build(BuildContext context) {
    return Material(
      key: Key('runSessionControls_Material'),
      color: Colors.white,
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RunSessionIconButton(
                  key: Key('pauseButton'),
                  onPressed: onPause,
                  buttonIcon: Icons.pause_rounded),
              RunSessionIconButton(
                  key: Key('playButton'),
                  onPressed: onPlay,
                  buttonIcon: Icons.play_arrow_rounded),
              RunSessionIconButton(
                  key: Key('stopButton'),
                  onPressed: onStop,
                  buttonIcon: Icons.stop_rounded),
            ],
          ),
          SizedBox(height: 10.0),
          Center(child: TimeText(seconds: seconds)),
        ],
      ),
    );
  }
}

class TimeText extends StatelessWidget {
  const TimeText({@required this.seconds});

  final int seconds;

  @override
  Widget build(BuildContext context) {
    int _hours = (seconds / 3600).floor();
    int _minutes = ((seconds - (3600 * _hours)) / 60).floor();
    int _seconds = (seconds - (60 * _minutes) - (3600 * _hours));
    return Text(
        _hours.toString().padLeft(2, '0') +
            ':' +
            _minutes.toString().padLeft(2, '0') +
            ':' +
            _seconds.toString().padLeft(2, '0'),
        key: Key("keyTimeText"),
        style: TextStyle(fontSize: 30));
  }
}
