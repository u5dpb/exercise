import 'package:exercise/basic_data_classes/session_data.dart';
import 'package:exercise/widgets/list_tiles/activity_list_tile.dart';
import 'package:flutter/material.dart';

class SessionDataListView extends StatelessWidget {
  final SessionData sessionData;
  const SessionDataListView({
    Key key,
    @required this.sessionData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: sessionData.activities.length,
      itemBuilder: (context, index) {
        return ActivityListTile(
            key: Key('showSessionDataActivity_' + index.toString()),
            activity: sessionData.activities[index],
            duration: sessionData.activityTime[index]);
      },
    );
  }
}
