import 'package:exercise/model/app_data.dart';
import 'package:exercise/screens/show_session_data_screen.dart';
import 'package:exercise/widgets/list_tiles/session_data_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SessionDataSummaryListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<AppData>(
      builder: (context, sessionList, child) {
        return ListView.builder(
            itemCount: sessionList.sessionDataList.length,
            itemBuilder: (context, index) {
              return SessionDataTile(
                key: Key('sessionDataList_' + index.toString()),
                sessionData: sessionList.sessionDataList[index],
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return ShowSessionDataScreen(
                            sessionData: sessionList.sessionDataList[index]);
                      },
                    ),
                  );
                },
              );
            });
      },
    );
  }
}
