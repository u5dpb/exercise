import 'package:exercise/basic_data_classes/session.dart';
import 'package:exercise/model/app_data.dart';
import 'package:exercise/widgets/list_tiles/session_activity_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PartialActivityList extends StatelessWidget {
  const PartialActivityList(
      {@required this.session, @required this.fromActivity});

  final Session session;
  final int fromActivity;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Consumer<AppData>(
        builder: (context, sessionList, child) {
          return ListView.builder(
            itemCount: session.activities.length - fromActivity - 1,
            itemBuilder: (context, index) {
              return SessionActivityTile(
                  index: index + fromActivity + 1, currentSession: session);
            },
          );
        },
      ),
    );
  }
}
