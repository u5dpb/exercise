import 'package:exercise/model/app_data.dart';
import 'package:exercise/model/run_session_timer.dart';
import 'package:exercise/model/speak.dart';
import 'package:exercise/screens/run_session_screen.dart';
import 'package:exercise/widgets/list_tiles/session_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChooseSessionListView extends StatelessWidget {
  const ChooseSessionListView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<AppData>(
      builder: (context, sessionList, child) {
        return ListView.builder(
            itemCount: sessionList.sessionList.length,
            itemBuilder: (context, index) {
              return SessionTile(
                  key: Key('chooseSession_' + index.toString()),
                  session: sessionList.sessionList[index],
                  onTap: () {
                    Provider.of<RunSessionTimer>(context, listen: false)
                        .currentSession = sessionList.sessionList[index];
                    // SchedulerBinding.instance.addPostFrameCallback((_) {
                    //   Provider.of<RunSessionTimer>(context, listen: false)
                    //       .initialise(restoreSession: false);
                    // });
                    // Provider.of<RunSessionTimer>(context, listen: false)
                    //     .initialise(restoreSession: false);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return RunSession(
                            speechDriver: Speak(),
                            restoreSession: false,
                          );
                        },
                      ),
                    );
                  });
            });
      },
    );
  }
}
